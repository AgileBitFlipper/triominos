#!/usr/bin/env bash
docker volume create --name nexus-data
docker run -d -p 8085:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3