# SpotBugs Errors and Warnings

The following table provides the errors and warnings identified on 28 Apr 2022.

| Level | Priority | Description | Type |
|-------|------|-----------------------------------------------------------------------|----------------------|
|[ERROR]|Medium|com.thirdsonsoftware.AnalyzeResults.averageNumberOfTilesInAGame() makes inefficient use of keySet iterator instead of entrySet iterator [com.thirdsonsoftware.AnalyzeResults] At AnalyzeResults.java:[line 142] |WMI_WRONG_MAP_ITERATOR|
|[ERROR]|Medium|com.thirdsonsoftware.Choice.getTile() may expose internal representation by returning Choice.tile [com.thirdsonsoftware.Choice] At Choice.java:[line 80] |EI_EXPOSE_REP|
|[ERROR]|Medium|new com.thirdsonsoftware.Choice(Tile, int, int, Orientation, int) may expose internal representation by storing an externally mutable object into Choice.tile [com.thirdsonsoftware.Choice] At Choice.java:[line 66] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Choice.setTile(Tile) may expose internal representation by storing an externally mutable object into Choice.tile [com.thirdsonsoftware.Choice] At Choice.java:[line 89] |EI_EXPOSE_REP2|
|[ERROR]|Medium|new com.thirdsonsoftware.Event(EventType, Player, Tile, int, int) may expose internal representation by storing an externally mutable object into Event.player [com.thirdsonsoftware.Event] At Event.java:[line 92] |EI_EXPOSE_REP2|
|[ERROR]|Medium|new com.thirdsonsoftware.Event(EventType, Player, Tile, int, int) may expose internal representation by storing an externally mutable object into Event.tile [com.thirdsonsoftware.Event] At Event.java:[line 93] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.EventManager.getEvents() may expose internal representation by returning EventManager.events [com.thirdsonsoftware.EventManager] At EventManager.java:[line 89] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Play.getPlayer() may expose internal representation by returning Play.player [com.thirdsonsoftware.Play] At Play.java:[line 87] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Play.getRound() may expose internal representation by returning Play.round [com.thirdsonsoftware.Play] At Play.java:[line 285] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Play.getTile() may expose internal representation by returning Play.tile [com.thirdsonsoftware.Play] At Play.java:[line 105] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Play.setPlayer(Player) may expose internal representation by storing an externally mutable object into Play.player [com.thirdsonsoftware.Play] At Play.java:[line 96] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Play.setRound(Round) may expose internal representation by storing an externally mutable object into Play.round [com.thirdsonsoftware.Play] At Play.java:[line 294] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Play.setTile(Tile) may expose internal representation by storing an externally mutable object into Play.tile [com.thirdsonsoftware.Play] At Play.java:[line 114] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Player.getStartingTile() may expose internal representation by returning Player.startingTile [com.thirdsonsoftware.Player] At Player.java:[line 120] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Player.getTray() may expose internal representation by returning Player.tray [com.thirdsonsoftware.Player] At Player.java:[line 128] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Player.setStartingTile(Tile) may expose internal representation by storing an externally mutable object into Player.startingTile [com.thirdsonsoftware.Player] At Player.java:[line 124] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Round.getBoard() may expose internal representation by returning Round.board [com.thirdsonsoftware.Round] At Round.java:[line 375] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Round.getPlayers() may expose internal representation by returning Round.players [com.thirdsonsoftware.Round] At Round.java:[line 384] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Round.getTiles() may expose internal representation by returning Round.tiles [com.thirdsonsoftware.Round] At Round.java:[line 320] |EI_EXPOSE_REP|
|[ERROR]|Medium|new com.thirdsonsoftware.Round(int, ArrayList) may expose internal representation by storing an externally mutable object into Round.players [com.thirdsonsoftware.Round] At Round.java:[line 75] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Tile.getPlayer() may expose internal representation by returning Tile.playedBy [com.thirdsonsoftware.Tile] At Tile.java:[line 308] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Tile.setPlayer(Player) may expose internal representation by storing an externally mutable object into Tile.playedBy [com.thirdsonsoftware.Tile] At Tile.java:[line 299] |EI_EXPOSE_REP2|
|[ERROR]|Medium|com.thirdsonsoftware.Triominos.getGame() may expose internal representation by returning Triominos.game [com.thirdsonsoftware.Triominos] At Triominos.java:[line 117] |EI_EXPOSE_REP|
|[ERROR]|Medium|com.thirdsonsoftware.Triominos.setGame(Game) may expose internal representation by storing an externally mutable object into Triominos.game [com.thirdsonsoftware.Triominos] At Triominos.java:[line 121] |EI_EXPOSE_REP2|
|[ERROR]|Medium|Unread field: com.thirdsonsoftware.Triominos.triominosFilename [com.thirdsonsoftware.Triominos] At Triominos.java:[line 29] |URF_UNREAD_FIELD|
