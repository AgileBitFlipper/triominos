/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware

class PlayerTest extends GroovyTestCase {

    static int TEST_SCORE = 25
    static int UNEXPECTED_SCORE = 111
    static String UNEXPECTED_NAME = "CoolCatLewis"
    static String TEST_NAME = "PlayerX"
    static Tile one
    static Tile two
    static Tile fiveFivesTile = new Tile(5,5,5)
    static Board board = new Board()
    ArrayList<Tile> availableFaces = new ArrayList<Tile>()
    ArrayList<Tile> playedTiles = new ArrayList<Tile>()

    Player player = null

    void setUp() {
        super.setUp()
        one = new Tile(1,1,1)
        two = new Tile(1,2,3)
        player = new Player(UNEXPECTED_NAME)
        player.score = UNEXPECTED_SCORE
        player.tray.add(one)
        player.tray.add(new Tile(2,2,2))
        player.tray.add(new Tile(3,3,3))
        player.tray.add(new Tile(4,4,4))
        player.tray.add(fiveFivesTile)

        availableFaces.add(two)
        availableFaces.add(new Tile(1,2,1))

        playedTiles.add(two)
        playedTiles.add(new Tile(1,2,1))
    }

    void tearDown() {
    }

    void testGetScore() {
        assertEquals( UNEXPECTED_SCORE, player.getScore() )
    }

    void testSetScore() {
        player.setScore(TEST_SCORE)
        assertEquals(TEST_SCORE, player.getScore())
    }

    void testGetName() {
        assertEquals(UNEXPECTED_NAME,player.getName())
    }

    void testSetName() {
        player.setName(TEST_NAME)
        assertEquals(TEST_NAME, player.getName())
    }

    void testIsStarts() {
        assertEquals(false, player.getStarts())
    }

    void testSetStarts() {
        player.setStarts(true)
        assertTrue(player.getStarts())
    }

    void testDrawTile() {
        ArrayList<Tile> pool = new ArrayList<Tile>()
        pool.add(two)
        player.drawTile(pool,1)

    }

    static String TWO_TILES = new String("    Tiles: --  0--  - --  0-- \n"
                                    + "           \\2   3/   - \\1   1/  \n"
                                    + "            \\   /     -  \\   /   \n"
                                    + "             \\1/       -   \\1/    \n"
                                    + "              v         -    v     \n")

    void testDisplayComparisonOfTwoTiles() {
        assertEquals(TWO_TILES, player.displayComparisonOfTwoTiles(two,one))
    }

    static String ONE_TILE_2ND_NULL = new String("    Tiles: --  0--  - \n"
                                               + "           \\2   3/   - \n"
                                               + "            \\   /     -  none \n"
                                               + "             \\1/       - \n"
                                               + "              v         - \n")


    void testDisplayComparisonOfTwoTilesWithFirstNull() {
        assertEquals(ONE_TILE_1ST_NULL, player.displayComparisonOfTwoTiles(null,one))
    }
 
    static String ONE_TILE_1ST_NULL = new String("    Tiles:       - --  0-- \n"
                                               + "                 - \\1   1/  \n"
                                               + "            none -  \\   /   \n"
                                               + "                 -   \\1/    \n"
                                               + "                 -    v     \n")

    void testDisplayComparisonOfTwoTilesWithSecondNull() {
        assertEquals(ONE_TILE_2ND_NULL, player.displayComparisonOfTwoTiles(two,null))
    }

    static String TWO_TILES_ONE_ROTATED = new String("    Tiles:    ^        - --  0-- \n"
                                                   + "             /2\\      - \\1   1/  \n"
                                                   + "            /   \\    -  \\   /   \n"
                                                   + "           /1   3\\  -   \\1/    \n"
                                                   + "           --  0--  -    v     \n")

    void testDisplayComparisonOfTwoTilesOneRotated() {
        two.rotate(90)
        two.setOrientation(Orientation.UP)
        assertEquals(TWO_TILES_ONE_ROTATED, player.displayComparisonOfTwoTiles(two,one))
    }

    static String TWO_TILES_TOP_AND_BOTTOM = new String("--  0-- \n"
                                                      + "\\1   1/  \n"
                                                      + " \\   /   \n"
                                                      + "  \\1/    \n"
                                                      + "   v     \n"
                                                      + "--  0-- \n"
                                                      + "\\2   3/  \n"
                                                      + " \\   /   \n"
                                                      + "  \\1/    \n"
                                                      + "   v     \n")
    void testShowTwoTilesTopAndBottom() {
        assertEquals(TWO_TILES_TOP_AND_BOTTOM, player.showTwoTilesTopAndBottom(one, two))
    }

    static String TWO_TILES_LEFT_AND_RIGHT = new String("--  0-- --  0-- \n"
                                                      + "\\1   1/  \\2   3/  \n"
                                                      + " \\   /    \\   /   \n"
                                                      + "  \\1/      \\1/    \n"
                                                      + "   v        v     \n")

    void testShowTwoTilesLeftAndRight() {
        assertEquals(TWO_TILES_LEFT_AND_RIGHT, player.showTwoTilesLeftAndRight(one,two))
    }

    static String ONE_TILE = new String("--  0-- \n"
                                      + "\\1   1/  \n"
                                      + " \\   /   \n"
                                      + "  \\1/    \n"
                                      + "   v     \n")

    void testShowTile() {
        assertEquals(ONE_TILE, player.showTile(one))
    }

    void testHighestValueTile() {
        Tile highest = player.highestValueTile()
        assertEquals(fiveFivesTile, highest)
    }

    // void testAddTile() {
    //     String[] lines = new String[]{"","","","",""}
    //     one.setPlayer(player);
    //     assertEquals("", player.addTile(lines, one, true))
    // }

    void testGetLargestValuedTile() {
        assertEquals(fiveFivesTile, player.getLargestValuedTile())
    }

    void testGetLargestValuedTileWithLargestValueLast() {
        Player newPlayer = new Player(TEST_NAME)
        newPlayer.tray.add(fiveFivesTile)
        newPlayer.tray.add(new Tile(1,1,1))
        newPlayer.tray.add(new Tile(3,3,3))
        assertEquals(fiveFivesTile, player.getLargestValuedTile())
    }

    void testDisplayTiles() {
        assertEquals("CoolCatLewis (2):\n  [1-2-3, 1-2-1]\n", player.displayTiles(false, player.getName(), playedTiles))
    }

    static final int playerHashCode = 282768411;

    void testHashCode() {
        assertEquals(playerHashCode, player.hashCode())
    }

    void testEqualsWithTile() {
        assertFalse(player.equals(one))
    }

    void testEqualsWithDifferentPlayerName() {
        Player duplicatePlayer = new Player(TEST_NAME)
        assertFalse(player.equals(duplicatePlayer))
    }

    void testEqualsWithSamePlayerName() {
        Player duplicatePlayer = new Player(UNEXPECTED_NAME)
        assertTrue(player.equals(duplicatePlayer))
    }

    void testGetStartingTile() {
        player.determineFirstTile()
        assertEquals(fiveFivesTile, player.getStartingTile())
    }

    void testGetStartingTileWithNoTriples() {
        Tile twoThreeFour = new Tile(2,3,4)
        player.tray.clear()
        player.tray.add(twoThreeFour)
        player.tray.add(new Tile(1,2,3))
        player.tray.add(new Tile(0,0,1))
        player.determineFirstTile()
        assertEquals(twoThreeFour, player.getStartingTile())
    }

    void testToString() {
    }

    void testPlayATile() {
        player.setStarts(true)
        assertEquals(0,board.count())
        player.playATile(board,playedTiles,availableFaces)
        assertEquals(1,board.count())
    }

    void testPlayATileDebug() {
        player.setMode(Mode.DEBUG)
        player.setStarts(true)
        assertEquals(1,board.count())
        player.playATile(board,playedTiles,availableFaces)
        assertEquals(1,board.count())
    }

}
