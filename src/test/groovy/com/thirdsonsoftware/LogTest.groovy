package com.thirdsonsoftware

class LogTest extends GroovyTestCase {
    void testInfo() {
        Log.info("This is a log information message.")
        assertTrue(true)
    }

    void testError() {
        Log.error("This is an error message.")
        assertTrue(true)
    }

    void testDebug() {
        Log.debug("This is a debug message.")
        assertTrue(true)
    }

    void testWarning() {
        Log.warning("This is a warning message.")
        assertTrue(true)
    }
}
