package com.thirdsonsoftware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RoundTest {

    final static int TEST_ROUND_NUMBER = 37;

    private int roundNumber = TEST_ROUND_NUMBER;
    
    @Mock
    Player playerA = new Player("rA");

    @Mock 
    Player playerB = new Player("rB");

    private ArrayList<Player> players = new ArrayList<Player>();

    private ArrayList<Tile> playerATray = new ArrayList<Tile>();
    private ArrayList<Tile> playerBTray = new ArrayList<Tile>();

    private Tile tile555 = new Tile(5,5,5,56);
    private Tile tile000 = new Tile(0,0,0,0);

    private Round round;

    @Before
    public void setup() {
        players.clear();
        players.add(playerA);
        players.add(playerB);
        
        playerATray.clear();
        playerATray.add(tile555);
        playerBTray.clear();
        playerBTray.add(tile000);

        this.round = new Round(roundNumber, players);

        when(playerA.getScore()).thenReturn(300);
        when(playerA.getTray()).thenReturn(playerATray);
        when(playerA.determineFirstTile()).thenReturn(tile555);
        when(playerA.getName()).thenReturn("PlayerA");
        when(playerA.toString()).thenReturn(
            "  Name: PlayerA\n"
            + "    Starts: no\n"
            + "    Score: 300\n"
            + "    Hand (0):\n"
            + "  [<empty>]\n");

        when(playerB.getScore()).thenReturn(450);
        when(playerB.getTray()).thenReturn(playerBTray);
        when(playerB.determineFirstTile()).thenReturn(tile000);
        when(playerB.getName()).thenReturn("PlayerB");
        when(playerB.toString()).thenReturn(
            "  Name: PlayerB\n"
            + "    Starts: no\n"
            + "    Score: 450\n"
            + "    Hand (0):\n"
            + "  [<empty>]\n");
    }

    @Test
    public void shouldGetRoundNumber() {
        assertThat(round.getRoundNumber(), is(TEST_ROUND_NUMBER));
    }

    @Test
    public void shouldHasWon() {
        assertThat(round.hasWon(), is(new ArrayList<Player>(Arrays.asList(playerB))));
    }

    @Test
    public void shouldPlayRound() {
        assertThat(round.playRound(), is(playerB));
    }

    @Test
    public void shouldGetTiles() {
        assertThat(round.getTiles().toString(), 
            is("[0-0-0 (1), 0-0-1 (2), 0-0-2 (3), 0-0-3 (4), 0-0-4 (5), 0-0-5 (6), 0-1-1 (7),"
            + " 0-1-2 (8), 0-1-3 (9), 0-1-4 (10), 0-1-5 (11), 0-2-2 (12), 0-2-3 (13), 0-2-4 (14),"
            + " 0-2-5 (15), 0-3-3 (16), 0-3-4 (17), 0-3-5 (18), 0-4-4 (19), 0-4-5 (20), 0-5-5 (21),"
            + " 1-1-1 (22), 1-1-2 (23), 1-1-3 (24), 1-1-4 (25), 1-1-5 (26), 1-2-2 (27), 1-2-3 (28),"
            + " 1-2-4 (29), 1-2-5 (30), 1-3-3 (31), 1-3-4 (32), 1-3-5 (33), 1-4-4 (34), 1-4-5 (35),"
            + " 1-5-5 (36), 2-2-2 (37), 2-2-3 (38), 2-2-4 (39), 2-2-5 (40), 2-3-3 (41), 2-3-4 (42),"
            + " 2-3-5 (43), 2-4-4 (44), 2-4-5 (45), 2-5-5 (46), 3-3-3 (47), 3-3-4 (48), 3-3-5 (49),"
            + " 3-4-4 (50), 3-4-5 (51), 3-5-5 (52), 4-4-4 (53), 4-4-5 (54), 5-5-4 (55), 5-5-5 (56)]"));
    }

    @Test
    public void shouldGetBoard() {
        assertThat(round.getBoard().toString(), is("Board:\n  Played Piece Count:0\n  Boundaries:(112,0,112,0)\n"));
    }

    @Test
    public void shouldGetPlayers() {
        assertThat(round.getPlayers(), is(players));
    }

    @Test
    public void shouldGetNumDraws() {
        assertThat(round.getNumDraws(), is(9));
    }

    @Test
    public void shouldDisplayTiles() {
        assertThat(round.displayTiles(true, playerA.getName(), playerA.getTray()), 
            is("PlayerA (1):\n"
                + "     ^    \n"
                + "    /5\\   \n"
                + "   /   \\  \n"
                + "  /5   5\\ \n"
                + "  -- 56-- \n"));

        assertThat(round.displayTiles(false, playerA.getName(), playerA.getTray()), 
            is("PlayerA (1):\n  [5-5-5 (56)]\n"));

        playerATray.add(new Tile(1,1,1));
        assertThat(round.displayTiles(true, playerA.getName(), playerA.getTray()), 
            is("PlayerA (2):\n"
            + "     ^       ^    \n"
            + "    /5\\     /1\\   \n"
            + "   /   \\   /   \\  \n"
            + "  /5   5\\ /1   1\\ \n"
            + "  -- 56-- --  0-- \n"));
        assertThat(round.displayTiles(false, playerA.getName(), playerA.getTray()), 
            is("PlayerA (2):\n  [5-5-5 (56), 1-1-1 (0)]\n"));
    }

    @Test
    public void shouldToString() {
        assertThat(round.toString(),
            is("Round 37:\n"
                + " Players (2):\n"
                + "  Name: PlayerA\n"
                + "    Starts: no\n"
                + "    Score: 300\n"
                + "    Hand (0):\n"
                + "  [<empty>]\n"
                + "  Name: PlayerB\n"
                + "    Starts: no\n"
                + "    Score: 450\n"
                + "    Hand (0):\n"
                + "  [<empty>]\n"
                + " Tile Pool (56):\n"
                + "     ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^    \n"
                + "    /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /3\\     /3\\     /3\\     /3\\     /3\\     /3\\     /4\\     /4\\     /5\\     /5\\   \n"
                + "   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  \n"
                + "  /0   0\\ /1   0\\ /2   0\\ /3   0\\ /4   0\\ /5   0\\ /1   1\\ /2   1\\ /3   1\\ /4   1\\ /5   1\\ /2   2\\ /3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /1   1\\ /2   1\\ /3   1\\ /4   1\\ /5   1\\ /2   2\\ /3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /2   2\\ /3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /4   4\\ /5   4\\ /4   5\\ /5   5\\ \n"
                + "  --  1-- --  2-- --  3-- --  4-- --  5-- --  6-- --  7-- --  8-- --  9-- -- 10-- -- 11-- -- 12-- -- 13-- -- 14-- -- 15-- -- 16-- -- 17-- -- 18-- -- 19-- -- 20-- -- 21-- -- 22-- -- 23-- -- 24-- -- 25-- -- 26-- -- 27-- -- 28-- -- 29-- -- 30-- -- 31-- -- 32-- -- 33-- -- 34-- -- 35-- -- 36-- -- 37-- -- 38-- -- 39-- -- 40-- -- 41-- -- 42-- -- 43-- -- 44-- -- 45-- -- 46-- -- 47-- -- 48-- -- 49-- -- 50-- -- 51-- -- 52-- -- 53-- -- 54-- -- 55-- -- 56-- \n"
                + " Played Tiles (0):\n"
                + "  [<empty>]\n"
                + "Board:\n"
                + "  Played Piece Count:0\n"
                + "  Boundaries:(112,0,112,0)\n"
                + " Pieces on Board with Empty Faces (0):\n"
                + "  [<empty>]\n"));
    }

    @Test
    public void shouldWhoIsFirst() {
        assertThat(round.whoIsFirst(), is(playerB));
    }
}
