package com.thirdsonsoftware;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

// Run with the Silent class to silence UnnecessaryStubbing exception
//   so we can property test and cover all code.
@RunWith(MockitoJUnitRunner.Silent.class)
public class BoardTest {

    static final int COLS = 10;
    static final int ROWS = 10;
    static final int PIECE_COL = 6;
    static final int PIECE_ROW = 5;

    @Mock
    Choice choice555;
    
    @Mock
    Choice choice554;

    @Mock
    Choice choice553;

    @Mock
    Choice choice552;

    @Mock
    Choice choiceOffBoard;

    @Mock
    Choice choice111;

    @Mock
    Tile tile555;

    @Mock
    Tile tile554;

    @Mock
    Tile tile553;
        
    @Mock
    Tile tile552;

    @Mock
    Tile tile111;
        
    @Mock
    Tile tile444;

    @Mock
    Tile tile222;

    private int rows;
    private int cols;

    private Board board;

    /**
     * Execute this method before each test.
     */
    @Before
    public void setup() {
        rows = ROWS;
        cols = COLS;
        this.board = new Board(rows, cols);

        when(tile555.getCornerA()).thenReturn(5);
        when(tile555.getCornerB()).thenReturn(5);
        when(tile555.getCornerC()).thenReturn(5);
        when(tile555.getLeftCorner()).thenReturn(5);
        when(tile555.getMiddleCorner()).thenReturn(5);
        when(tile555.getRightCorner()).thenReturn(5);
        when(tile555.getLeftFace()).thenReturn(new Face(5,5));
        when(tile555.getRightFace()).thenReturn(new Face(5,5));
        when(tile555.getMiddleFace()).thenReturn(new Face(5,5));
        when(tile555.getOrientation()).thenReturn(Orientation.UP);

        when(tile553.getCornerA()).thenReturn(5);
        when(tile553.getCornerB()).thenReturn(5);
        when(tile553.getCornerC()).thenReturn(3);
        when(tile553.getLeftCorner()).thenReturn(3);
        when(tile553.getMiddleCorner()).thenReturn(5);
        when(tile553.getRightCorner()).thenReturn(5);
        when(tile553.getLeftFace()).thenReturn(new Face(3,5));
        when(tile553.getRightFace()).thenReturn(new Face(5,5));
        when(tile553.getMiddleFace()).thenReturn(new Face(5,3));
        when(tile553.getOrientation()).thenReturn(Orientation.UP);

        when(tile554.getCornerA()).thenReturn(5);
        when(tile554.getCornerB()).thenReturn(5);
        when(tile554.getCornerC()).thenReturn(4);
        when(tile554.getLeftCorner()).thenReturn(5);
        when(tile554.getMiddleCorner()).thenReturn(5);
        when(tile554.getRightCorner()).thenReturn(4);
        when(tile554.getLeftFace()).thenReturn(new Face(5,5));
        when(tile554.getRightFace()).thenReturn(new Face(4,5));
        when(tile554.getMiddleFace()).thenReturn(new Face(5,4));
        when(tile554.getOrientation()).thenReturn(Orientation.DOWN);

        when(tile552.getCornerA()).thenReturn(5);
        when(tile552.getCornerB()).thenReturn(5);
        when(tile552.getCornerC()).thenReturn(2);
        when(tile552.getLeftCorner()).thenReturn(5);
        when(tile552.getMiddleCorner()).thenReturn(5);
        when(tile552.getRightCorner()).thenReturn(2);
        when(tile552.getLeftFace()).thenReturn(new Face(5,5));
        when(tile552.getRightFace()).thenReturn(new Face(2,5));
        when(tile552.getMiddleFace()).thenReturn(new Face(5,2));
        when(tile552.getOrientation()).thenReturn(Orientation.DOWN);

        when(tile111.getCornerA()).thenReturn(1);
        when(tile111.getCornerB()).thenReturn(1);
        when(tile111.getCornerC()).thenReturn(1);
        when(tile111.getLeftCorner()).thenReturn(1);
        when(tile111.getMiddleCorner()).thenReturn(1);
        when(tile111.getRightCorner()).thenReturn(1);
        when(tile111.getLeftFace()).thenReturn(new Face(1,1));
        when(tile111.getRightFace()).thenReturn(new Face(1,1));
        when(tile111.getMiddleFace()).thenReturn(new Face(1,1));
        when(tile111.getOrientation()).thenReturn(Orientation.UP);

        when(tile444.getCornerA()).thenReturn(4);
        when(tile444.getCornerB()).thenReturn(4);
        when(tile444.getCornerC()).thenReturn(4);
        when(tile444.getLeftCorner()).thenReturn(4);
        when(tile444.getMiddleCorner()).thenReturn(4);
        when(tile444.getRightCorner()).thenReturn(4);
        when(tile444.getLeftFace()).thenReturn(new Face(4,4));
        when(tile444.getRightFace()).thenReturn(new Face(4,4));
        when(tile444.getMiddleFace()).thenReturn(new Face(4,4));
        when(tile444.getOrientation()).thenReturn(Orientation.UP);

        when(tile222.getCornerA()).thenReturn(2);
        when(tile222.getCornerB()).thenReturn(2);
        when(tile222.getCornerC()).thenReturn(2);
        when(tile222.getLeftCorner()).thenReturn(2);
        when(tile222.getMiddleCorner()).thenReturn(2);
        when(tile222.getRightCorner()).thenReturn(2);
        when(tile222.getLeftFace()).thenReturn(new Face(2,2));
        when(tile222.getRightFace()).thenReturn(new Face(2,2));
        when(tile222.getMiddleFace()).thenReturn(new Face(2,2));
        when(tile222.getOrientation()).thenReturn(Orientation.UP);

        when(choiceOffBoard.getCol()).thenReturn(0);

        // Tile 555 at 5,6 UP Rotated 120
        when(choice555.getCol()).thenReturn(PIECE_COL);
        when(choice555.getRow()).thenReturn(PIECE_ROW);
        when(choice555.getOrientation()).thenReturn(Orientation.UP);
        when(choice555.getRotation()).thenReturn(120);
        when(choice555.getTile()).thenReturn(tile555);

        // Tile 554 at 5,7 DOWN
        when(choice554.getCol()).thenReturn(PIECE_COL + 1);
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        when(choice554.getTile()).thenReturn(tile554);
        when(choice554.getRotation()).thenReturn(0);
        when(choice554.getOrientation()).thenReturn(Orientation.DOWN);

        // Tile 553 at 5,7 UP
        when(choice553.getCol()).thenReturn(PIECE_COL + 1);
        when(choice553.getRow()).thenReturn(PIECE_ROW);
        when(choice553.getTile()).thenReturn(tile553);
        when(choice553.getOrientation()).thenReturn(Orientation.UP);

        // Tile 552 at 5,5 DOWN
        when(choice552.getCol()).thenReturn(PIECE_COL - 1);
        when(choice552.getRow()).thenReturn(PIECE_ROW);
        when(choice552.getTile()).thenReturn(tile552);
        when(choice552.getRotation()).thenReturn(0);
        when(choice552.getOrientation()).thenReturn(Orientation.DOWN);

        // Tile 111 at 5,6 UP
        when(choice111.getCol()).thenReturn(PIECE_COL);
        when(choice111.getRow()).thenReturn(PIECE_ROW);
        when(choice111.getOrientation()).thenReturn(Orientation.UP);
        when(choice111.getRotation()).thenReturn(120);
        when(choice111.getTile()).thenReturn(tile111);
    }

    @After
    public void teardown() {
        board.clearBoard();
    }

    @Test
    public void testRightFaceFits() {
        assertThat(board.rightFaceFits(tile554, -1, PIECE_COL), is(false));
        assertThat(board.rightFaceFits(tile554, PIECE_ROW, -1), is(false));
        assertThat(board.rightFaceFits(tile554, board.getNumberOfRows(), PIECE_COL), is(false));
        assertThat(board.rightFaceFits(tile554, PIECE_ROW, board.getNumberOfCols()), is(false));

        // No tiles on board to match
        assertThat(board.rightFaceFits(tile554, PIECE_ROW, PIECE_COL-1), is(true));

        // Place a tile to match
        board.placeTile(choice555);

        // Fail to match faces
        assertThat(board.rightFaceFits(tile554, PIECE_ROW, PIECE_COL-1), is(false));

        // Fail to match orientation
        assertThat(board.rightFaceFits(tile553, PIECE_ROW, PIECE_COL-1), is(false));

        // Rotate to match faces
        when(tile554.getRightFace()).thenReturn(new Face(5,5));
        when(tile554.getRotation()).thenReturn(240);
        assertThat(board.rightFaceFits(tile554, PIECE_ROW, PIECE_COL-1), is(true));

    }

    //  Place Tile 5-5-4 @ 5,7 with all other slots occupied
    //     --------------------
    //          5    6    7    
    //     --------------------
    // | | +++++ -- 52-- = ++++
    // | | ++++/0\\0   0//0\++++
    // |4| +++/   \\   //   \+++
    // | | ++/2   5\\5//5   4\+
    // | | + --   -- v --   -- +
    // | | = -- 53-- ^ -- 55-- =
    // | | ==\2   5//5\\5   4/==
    // |5| ===\   //   \\   /===
    // | | ====\5//5   5\\5/====
    // | | =====v -- 56-- v=====

    @Test
    public void testLeftCornerFits() {
        // Validate the off the board
        assertThat(board.leftCornerFits(tile111, PIECE_ROW, -1), is(false));
        assertThat(board.leftCornerFits(tile111, -1, 0), is(false));
        assertThat(board.leftCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()), is(false));
        assertThat(board.leftCornerFits(tile111, board.getNumberOfRows(), 0), is(false));

        // Validate the on the edges
        assertThat(board.leftCornerFits(tile111, PIECE_ROW, 0), is(true));
        assertThat(board.leftCornerFits(tile111, 0, PIECE_COL), is(true));
        // No look up or left
        assertThat(board.leftCornerFits(tile554, 0, 0), is(true));
        // Look up not left
        assertThat(board.leftCornerFits(tile554, 1, 0), is(true));
        // Look up only
        assertThat(board.leftCornerFits(tile554, 0, PIECE_COL), is(true));
        assertThat(board.leftCornerFits(tile554, 1, PIECE_COL), is(true));
        assertThat(board.leftCornerFits(tile111, 1, PIECE_COL), is(true));
        assertThat(board.leftCornerFits(tile111, board.getNumberOfRows()-1, PIECE_COL), is(true));
        assertThat(board.leftCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()-1), is(true));

        // Validate looks with tiles
        board.placeTile(choice555);
        // Down tiles
        assertThat(board.leftCornerFits(tile554, PIECE_ROW-1, PIECE_COL+1), is(true));
        assertThat(board.leftCornerFits(tile554, PIECE_ROW, PIECE_COL+1), is(true));
        assertThat(board.leftCornerFits(tile554, PIECE_ROW+1, PIECE_COL+2), is(true));
        when(tile222.getOrientation()).thenReturn(Orientation.DOWN);
        assertThat(board.leftCornerFits(tile222, PIECE_ROW, PIECE_COL+1), is(false));
        assertThat(board.leftCornerFits(tile222, PIECE_ROW+1, PIECE_COL+2), is(false));

        // Up tiles
        assertThat(board.leftCornerFits(tile111, PIECE_ROW-1, PIECE_COL+1), is(false));
        assertThat(board.leftCornerFits(tile111, PIECE_ROW+1, PIECE_COL+2), is(true));

        // 5-5-2 @ 5,5 DOWN
        board.clearBoard();
        board.placeTile(choice552);
        assertThat(board.leftCornerFits(tile554, PIECE_ROW, PIECE_COL+1), is(false));
        assertThat(board.leftCornerFits(tile554, PIECE_ROW+1, PIECE_COL), is(true));
        assertThat(board.leftCornerFits(tile111, PIECE_ROW-1, PIECE_COL+1), is(false));
        when(tile111.getOrientation()).thenReturn(Orientation.DOWN);
        assertThat(board.leftCornerFits(tile111, PIECE_ROW+1, PIECE_COL), is(false));

    }

    //     -------------------- --------------------
    //         5    6    7           5    6    7    
    //     -------------------- --------------------
    // | | ++++^ -- 54-- ^+++++ +++++= -- 53-- =+++++
    // | | +++/5\\3   5//5\++++ ++++===\5   2/===++++
    // |4| ++/   \\   //   \+++ +++=====\   /=====+++
    // | | +/4   5\\5//4   5\++ ++=======\5/=======++
    // | |  -- 55-- v -- 55-- + +=========v=========+
    // | | +++++++++^+++++++++= = -- 53-- ^ -- 55-- =
    // | | =+++++++/5\+++++++== ==\2   5//5\\5   4/==
    // |5| ==+++++/   \+++++=== ===\   //   \\   /===
    // | | ===+++/5   5\+++==== ====\5//5   5\\5/====
    // | | ====+ -- 56-- +===== =====v -- 56-- v=====

    @Test
    public void testMiddleCornerFits() {
        // Validate the edges
        assertThat(board.middleCornerFits(tile111, PIECE_ROW, -1), is(false));
        assertThat(board.middleCornerFits(tile111, -1, 0), is(false));
        assertThat(board.middleCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()), is(false));
        assertThat(board.middleCornerFits(tile111, board.getNumberOfRows(), 0), is(false));

        // Validate the looks up, down, right, left
        assertThat(board.middleCornerFits(tile111, PIECE_ROW, 0), is(true));
        assertThat(board.middleCornerFits(tile111, 0, PIECE_COL), is(true));
        assertThat(board.middleCornerFits(tile111, board.getNumberOfRows()-1, PIECE_COL), is(true));
        assertThat(board.middleCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()-1), is(true));

        // Place DOWN tile on edges
        assertThat(board.middleCornerFits(tile554, PIECE_ROW, 0), is(true));
        assertThat(board.middleCornerFits(tile554, 0, PIECE_COL), is(true));
        assertThat(board.middleCornerFits(tile554, board.getNumberOfRows()-1, PIECE_COL), is(true));
        assertThat(board.middleCornerFits(tile554, PIECE_ROW, board.getNumberOfCols()-1), is(true));

        // Place 555 UP at 5,6.  Place others around it.
        board.placeTile(choice555);
        assertThat(board.middleCornerFits(tile553, PIECE_ROW+1, PIECE_COL-1), is(true));
        assertThat(board.middleCornerFits(tile553, PIECE_ROW+1, PIECE_COL+1), is(true));
        assertThat(board.middleCornerFits(tile111, PIECE_ROW+1, PIECE_COL-1), is(false));
        assertThat(board.middleCornerFits(tile111, PIECE_ROW+1, PIECE_COL+1), is(false));
        assertThat(board.middleCornerFits(tile554, PIECE_ROW-1, PIECE_COL), is(true));
        when(tile111.getOrientation()).thenReturn(Orientation.DOWN);
        assertThat(board.middleCornerFits(tile111, PIECE_ROW-1, PIECE_COL), is(false));

        // Place 554 DOWN at 5,7.
        board.clearBoard();
        board.placeTile(choice554);
        assertThat(board.middleCornerFits(tile444, PIECE_ROW+1, PIECE_COL+1), is(false));
        assertThat(board.middleCornerFits(tile552, PIECE_ROW-1, PIECE_COL), is(true));
        assertThat(board.middleCornerFits(tile552, PIECE_ROW-1, PIECE_COL+1), is(true));
        when(tile552.getMiddleCorner()).thenReturn(2);
        when(tile552.getRotation()).thenReturn(120);
        assertThat(board.middleCornerFits(tile552, PIECE_ROW-1, PIECE_COL), is(false));
        assertThat(board.middleCornerFits(tile552, PIECE_ROW-1, PIECE_COL+1), is(false));
        
        board.clearBoard();
        when(tile552.getLeftCorner()).thenReturn(2);
        when(tile552.getMiddleCorner()).thenReturn(5);
        when(tile552.getRightCorner()).thenReturn(5);
        when(tile552.getRotation()).thenReturn(240);
        board.placeTile(choice552);
        assertThat(board.middleCornerFits(tile554, PIECE_ROW-1, PIECE_COL), is(true));
        
    }

    // --------------  -----------
    //         7            6 
    // --------------  -----------
    // | |+++++^+++++  +++++^+++++
    // | |++++/5\++++  ++++/4\++++
    // |4|+++/   \+++  +++/   \+++
    // | |++/5   5\++  ++/5   5\++
    // | |+ -- 56-- +  + -- 54-- +
    // | |= -- 55-- =  = -- 56-- =
    // | |==\5   5/==  ==\5   5/==
    // |5|===\   /===  ===\   /===
    // | |====\4/====  ====\5/====
    // | |=====v=====  =====v=====

    @Test
    public void testMiddleFaceFits() {

        // Off board testing
        assertThat( board.middleFaceFits(tile555, PIECE_ROW, -1), is(false));
        assertThat( board.middleFaceFits(tile555, PIECE_ROW, board.getNumberOfCols()), is(false));
        assertThat( board.middleFaceFits(tile555, -1, PIECE_COL), is(false));
        assertThat( board.middleFaceFits(tile555, board.getNumberOfRows(), PIECE_COL), is(false));

        // Just at border
        assertThat( board.middleFaceFits(tile554, 0, PIECE_COL), is(true));
        assertThat( board.middleFaceFits(tile555, board.getNumberOfRows()-1, PIECE_COL), is(true));

        // One row inside borders
        assertThat( board.middleFaceFits(tile555, 1, PIECE_COL), is(true));
        assertThat( board.middleFaceFits(tile554, board.getNumberOfRows()-2, PIECE_COL), is(true));

        // Perfect fit placing tile that is UP
        board.placeTile(choice555);
        assertThat(board.middleFaceFits(tile554, PIECE_ROW-1, PIECE_COL), is(true));
        // Bad orientation UP
        assertThat(board.middleFaceFits(tile553, PIECE_ROW-1, PIECE_COL), is(false));

        // Perfect fit placing tile that is DOWN
        board.clearBoard();
        when(choice554.getRotation()).thenReturn(120);
        when(tile554.getMiddleFace()).thenReturn(new Face(5,5));
        board.placeTile(choice554);
        assertThat(board.middleFaceFits(tile555, PIECE_ROW-1, PIECE_COL+1), is(true));
        // Bad orientation DOWN
        assertThat(board.middleFaceFits(tile552, PIECE_ROW+1, PIECE_COL+1), is(false));
        board.clearBoard();
    }

    @Test
    public void testLeftFaceFits() {

        // Off board testing
        assertThat( board.leftFaceFits(tile555, PIECE_ROW, -1), is(false));
        assertThat( board.leftFaceFits(tile555, PIECE_ROW, board.getNumberOfCols()), is(false));
        assertThat( board.leftFaceFits(tile555, -1, PIECE_COL), is(false));
        assertThat( board.leftFaceFits(tile555, board.getNumberOfRows(), PIECE_COL), is(false));

        // On edge testing
        assertThat(board.leftFaceFits(tile555, PIECE_ROW, 0), is(true));
        assertThat(board.leftFaceFits(tile555, PIECE_ROW, board.getNumberOfCols()-1), is(true));
        assertThat(board.leftFaceFits(tile555, 0, PIECE_COL), is(true));
        assertThat(board.leftFaceFits(tile555, board.getNumberOfRows()-1, PIECE_COL), is(true));

        // In the middle
        assertThat(board.leftFaceFits(tile555, PIECE_ROW, PIECE_COL), is(true));

        board.placeTile(choice554);
        assertThat(board.leftFaceFits(tile555, PIECE_ROW, PIECE_COL+2), is(false));
        assertThat(board.leftFaceFits(tile111, PIECE_ROW, PIECE_COL+2), is(false));

        board.clearBoard();
        board.placeTile(choice555);
        assertThat(board.leftFaceFits(tile554, PIECE_ROW, PIECE_COL+1), is(true));
        assertThat(board.leftFaceFits(tile553, PIECE_ROW, PIECE_COL+1), is(false));

        board.clearBoard();
    }

    @Test
    public void testFindBoardMinMax() {
        board.findBoardMinMax();
        assertThat(board.getLeftBorder(), is(COLS));
        assertThat(board.getRightBorder(), is(0));
    }

    @Test
    public void testFindBoardMinMaxFull() {
        board.findBoardMinMax(true);
        assertThat(board.getLeftBorder(), is(0));
        assertThat(board.getTopBorder(), is(0));
        assertThat(board.getBottomBorder(), is(ROWS));
        assertThat(board.getRightBorder(), is(COLS));
    }

    @Test
    public void testSetNumberOfRows() {
        board.setNumberOfRows(100);
        assertThat(board.getNumberOfRows(), is(100));
        board.setNumberOfRows(ROWS);
    }

    @Test
    public void testSetNumberOfColumns() {
        board.setNumberOfCols(100);
        assertThat(board.getNumberOfCols(), is(100));
        board.setNumberOfCols(COLS);
    }

    // ---PASS-----  -----FAIL---------
    //        6             5    6    7   
    // ------------  ------------------
    // | |++++^++++  | |= --  -- ^+++++
    // | |+++/0\+++  | |=\0   0//5\++++
    // |5|++/ B \++  |5|==\ B // B \+++
    // | |+/0 P 0\+  | |===\0//5 P 5\++
    // | | --  1--   | |====v --   -- +
    //
    @Test
    public void testRightCornerFits() {

        // Validate our assumptions about the orientation of the slot on the board first.
        assertThat(board.getOrientationForPositionOnBoard(PIECE_ROW, PIECE_COL), is(Orientation.UP));

        // Validate the edges of the board (top, left, right, bottom)
        assertThat(board.rightCornerFits(tile111, PIECE_ROW, -1), is(false));
        assertThat(board.rightCornerFits(tile111, -1, 0), is(false));
        assertThat(board.rightCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()), is(false));
        assertThat(board.rightCornerFits(tile111, board.getNumberOfRows(), 0), is(false));

        // Validate the looks up, down, right, left on the edge of the board
        assertThat(board.rightCornerFits(tile111, PIECE_ROW, 0), is(true));
        assertThat(board.rightCornerFits(tile111, 0, PIECE_COL), is(true));
        assertThat(board.rightCornerFits(tile111, board.getNumberOfRows()-1, PIECE_COL), is(true));
        assertThat(board.rightCornerFits(tile111, PIECE_ROW, board.getNumberOfCols()-1), is(true));

        // Downward facing tile at top.
        assertThat(board.rightCornerFits(tile552, 0, PIECE_COL), is(true));
        assertThat(board.rightCornerFits(tile552, board.getNumberOfRows()-1, PIECE_COL), is(true));

        // Validate FAR looks up, down, right, left by moving one space into board edge
        assertThat(board.rightCornerFits(tile554, PIECE_ROW, 1), is(true));
        assertThat(board.rightCornerFits(tile554, 1, PIECE_COL), is(true));
        assertThat(board.rightCornerFits(tile554, board.getNumberOfRows()-1, PIECE_COL), is(true));
        assertThat(board.rightCornerFits(tile554, PIECE_ROW, board.getNumberOfCols()-1), is(true));

        // Place tile 555 at 5,6 and try to place 000 at 5,5 should fail.
        board.placeTile(choice555);
        assertThat(board.rightCornerFits(tile111, PIECE_ROW, PIECE_COL-1), is(false));
    }

    //    ------FAIL-------
    //             6    7   
    //    -----------------
    //    | | --  --  ^++++
    //    | |=\5   4//5\+++ 
    //    |5|==\ B // B \++
    //    | |===\5//5 P 5\+
    //    | |====v --   -- 
    @Test
    public void shouldRightCornerFitAfterNoRotation() {
        // Place 555 at 55,56.
        assertThat(board.placeTile(choice555), is(true));

        // Attempt to place 554 at 55,54 and rotate until it fits.
        assertThat(board.rightCornerFits(tile554, PIECE_ROW, PIECE_COL-1), is(false));
    }

    //    ------FAIL-------
    //             6    7   
    //    -----------------
    //    | | --  --  ^++++
    //    | |=\5   5//5\+++ 
    //    |5|==\ B // B \++
    //    | |===\4//5 P 5\+
    //    | |====v --   -- 
    @Test
    public void shouldRightCornerFitAfterRotation120() {
        // Place 555 at 55,56.
        assertThat(board.placeTile(choice555), is(true));

        // Attempt to place 554 at 55,54 and rotate 120.
        when(tile554.getRotation()).thenReturn(120);
        assertThat(board.rightCornerFits(tile554, PIECE_ROW, PIECE_COL-1), is(false));
    }

    //    ------PASS-------
    //             6    7   
    //    -----------------
    //    | | --  --  ^++++
    //    | |=\4   5//5\+++ 
    //    |5|==\ B // B \++
    //    | |===\5//5 P 5\+
    //    | |====v --   -- 
    @Test
    public void shouldRightCornerFitAfterRotation240() {
        // Place 555 at 55,56.
        assertThat(board.placeTile(choice555), is(true));

        // Attempt to place 554 at 55,54 and rotate 240 to fit.
        when(tile554.getRotation()).thenReturn(240);
        assertThat(board.rightCornerFits(tile554, PIECE_ROW, PIECE_COL-1), is(false));
    }

    //    ------PASS--------- ------FAIL-----------
    //        4    5    6          4    5    6    
    //    ------------------- ---------------------
    // | |++++=+++++++++^++++ +++++= -- 55-- =+++++
    // | |+++===+++++++/5\+++ ++++===\5   5/===++++
    // |5|++=====+++++/   \++ +++=====\   /=====+++
    // | |+=======+++/5   5\+ ++=======\2/=======++
    // | |=========+ -- 56--  +=========v=========+
    // | | -- 37-- =+++++++++ = -- 55-- =+++++++++=
    // | |=\2   2/===+++++++= ==\5   5/===+++++++==
    // |6|==\   /=====+++++== ===\   /=====+++++===
    // | |===\2/=======+++=== ====\4/=======+++====
    // | |====v=========+==== =====v=========+=====
    @Test
    public void shouldRightCornerFitUpAndRight() {
        board.placeTile(choice552);
        assertThat(board.rightCornerFits(tile554, PIECE_ROW+1, PIECE_COL-2), is(false));
        assertThat(board.rightCornerFits(tile222, PIECE_ROW+1, PIECE_COL-2), is(true));
        board.clearBoard();
        int x = board.getNumberOfCols()-1;
        when(choice552.getCol()).thenReturn(x);
        board.placeTile(choice552);
        assertThat(choice552.getOrientation(), is(board.getOrientationForPositionOnBoard(PIECE_ROW,x)));
        assertThat(board.rightCornerFits(tile554, PIECE_ROW+1, x-2), is(false));
        assertThat(board.rightCornerFits(tile222, PIECE_ROW+1, x-2), is(true));
    }

    //    ------PASS--------- ------FAIL-----------
    //        4    5    6          4    5    6    
    //    ------------------- ---------------------
    // | |++++=+++++++++^++++ +++++=+++++++++^+++++
    // | |+++===+++++++/5\+++ ++++===+++++++/5\++++
    // |5|++=====+++++/   \++ +++=====+++++/   \+++
    // | |+=======+++/5   5\+ ++=======+++/5   5\++
    // | |=========+ -- 56--  +=========+ -- 56-- +
    // | | -- 55-- =+++++++++ = -- 55-- =+++++++++=
    // | |=\5   4/===+++++++= ==\5   5/===+++++++==
    // |6|==\   /=====+++++== ===\   /=====+++++===
    // | |===\5/=======+++=== ====\4/=======+++====
    // | |====v=========+==== =====v=========+=====
    @Test
    public void shouldRightCornerFitUpAndFarRight() {
        board.placeTile(choice555);
        assertThat(board.rightCornerFits(tile554, PIECE_ROW+1, PIECE_COL-2), is(false));
        when(tile554.getRotation()).thenReturn(120);
        when(tile554.getRightCorner()).thenReturn(5);
        assertThat(board.rightCornerFits(tile554, PIECE_ROW+1, PIECE_COL-2), is(true));
    }

    //    ------PASS---------- ------FAIL----------
    //        5    6    7          5    6    7     
    //    -------------------- --------------------
    // | |++++^+++++++++=+++++ ++++^+++++++++=+++++
    // | |+++/5\+++++++===++++ +++/1\+++++++===++++
    // |4|++/   \+++++=====+++ ++/   \+++++=====+++
    // | |+/5   5\+++=======++ +/1   1\+++=======++
    // | | -- 56-- +=========+  -- 22-- +=========+
    // | |+++++++++= -- 55-- = +++++++++= -- 55-- =
    // | |=+++++++===\5   4/== =+++++++===\5   4/==
    // |5|==+++++=====\   /=== ==+++++=====\   /===
    // | |===+++=======\5/==== ===+++=======\5/====
    // | |====+=========v===== ====+=========v=====
    @Test
    public void shouldRightCornerFitBelowFarRight() {
        board.placeTile(choice554);
        assertThat(board.rightCornerFits(tile555, PIECE_ROW-1, PIECE_COL-1), is(true));
        assertThat(board.rightCornerFits(tile111, PIECE_ROW-1, PIECE_COL-1), is(false));
    }

        //    ------FAIL------- ------PASS-------
    //        5    6    7       5    6    7  
    //    ----------------- -----------------
    // | |++++^+++++++++=++ ++++^+++++++++=++
    // | |+++/5\+++++++===+ +++/5\+++++++===+
    // |4|++/   \+++++===== ++/   \+++++=====
    // | |+/3   5\+++====== +/5   3\+++======
    // | | -- 54-- +=======  -- 54-- +=======
    // | |+++++++++^+++++++ +++++++++^+++++++
    // | |=+++++++/5\++++++ =+++++++/5\++++++
    // |5|==+++++/   \+++++ ==+++++/   \+++++
    // | |===+++/5   5\+++= ===+++/5   5\+++=
    // | |====+ -- 56-- +== ====+ -- 56-- +==
    @Test
    public void shouldRightCornerFitBelowRight() {
        board.placeTile(choice555);
        // Fit with no rotation
        assertThat(board.rightCornerFits(tile553, PIECE_ROW-1, PIECE_COL-1), is(true));

        // Not fit with rotation of 240
        when(tile553.getRotation()).thenReturn(240);
        when(tile553.getLeftCorner()).thenReturn(5);
        when(tile553.getMiddleCorner()).thenReturn(5);
        when(tile553.getRightCorner()).thenReturn(3);
        assertThat(board.rightCornerFits(tile553, PIECE_ROW-1, PIECE_COL-1), is(false));
    }

    @Test
    public void shouldGetNumberOfRows() {
        assertThat(board.getNumberOfRows(), is(10));
    }

    @Test
    public void shouldGetNumberOfCols() {
        assertThat(board.getNumberOfCols(), is(10));
    }

    @Test
    public void shouldGetTopBorder() {
        assertThat(board.getTopBorder(), is(Board.DEFAULT_ROWS));
    }

    @Test
    public void shouldGetBottomBorder() {
        assertThat(board.getBottomBorder(), is(0));
    }

    @Test
    public void shouldGetLeftBorder() {
        assertThat(board.getLeftBorder(), is(Board.DEFAULT_COLS));
    }

    @Test
    public void shouldGetRightBorder() {
        assertThat(board.getRightBorder(), is(0));
    }

    @Test
    public void shouldGetOrientationForPositionOnBoard() {
        Orientation actualValue = board.getOrientationForPositionOnBoard(PIECE_ROW, PIECE_COL);
        assertThat(actualValue, is(Orientation.UP));
    }

    @Test
    public void shouldCount() {
        assertThat("There should be no pieces on the board.", board.count(), is(0));
        board.placeTile(choice555);
        assertThat(board.count(), is(1));
    }

    @Test
    public void shouldPieceAtLocation() {
        board.placeTile(choice555);
        assertThat(board.pieceAtLocation(PIECE_ROW, PIECE_COL), is(tile555));
    }

    @Test
    public void shouldPieceFits() {
        assertThat(board.pieceFits(choice555), is(true));
        board.placeTile(choice555);
        assertThat(board.pieceFits(choice111), is(false));
    }

    @Test
    public void shouldPlaceTile() {
        assertThat(board.placeTile(choice555), is(true));
    }

    @Test
    public void shouldNotPlaceTileOffLeftBoard() {
        when(choiceOffBoard.getRow()).thenReturn(-1);
        assertThat(board.placeTile(choiceOffBoard), is(false));
        when(choiceOffBoard.getRow()).thenReturn(0);
        when(choiceOffBoard.getCol()).thenReturn(-1);
        assertThat(board.placeTile(choiceOffBoard), is(false));
        when(choiceOffBoard.getCol()).thenReturn(board.getNumberOfCols());
        assertThat(board.placeTile(choiceOffBoard), is(false));
        when(choiceOffBoard.getRow()).thenReturn(board.getNumberOfRows());
        when(choiceOffBoard.getCol()).thenReturn(0);
        assertThat(board.placeTile(choiceOffBoard), is(false));
    }

    @Test
    public void shouldPieceNotFit() {
        board.placeTile(choice555);
        assertThat(board.placeTile(choice553), is(false));
    }

    @Test
    public void shouldGetRound() {
        assertThat(board.getRound(), is(0));
        board.setRound(2);
        assertThat(board.getRound(), is(2));
    }

    @Test
    public void shouldToString() {
        assertThat(board.toString(), is("Board:\n  Played Piece Count:0\n  Boundaries:(10,0,10,0)\n"));
    }

    @Test
    public void testSetTopBorder() {
        assertThat(board.getTopBorder(), is(Board.DEFAULT_ROWS));
        board.setTopBorder(5);
        assertThat(board.getTopBorder(), is(5));
        board.setTopBorder(-5);
        assertThat(board.getBottomBorder(), is(0));
        board.setTopBorder(200);
        assertThat(board.getBottomBorder(), is(0));
    }

    @Test
    public void testSetBottomBorder() {
        assertThat(board.getBottomBorder(), is(0));
        board.setBottomBorder(5);
        assertThat(board.getBottomBorder(), is(5));
        board.setBottomBorder(-5);
        assertThat(board.getBottomBorder(), is(0));
        board.setBottomBorder(200);
        assertThat(board.getBottomBorder(), is(COLS - 1));
    }

    @Test
    public void testSetLeftBorder() {
        assertThat(board.getLeftBorder(), is(Board.DEFAULT_COLS));
        board.setLeftBorder(5);
        assertThat(board.getLeftBorder(), is(5));
        board.setLeftBorder(-5);
        assertThat(board.getLeftBorder(), is(5));
        board.setLeftBorder(200);
        assertThat(board.getLeftBorder(), is(COLS - 1));
    }

    @Test
    public void testSetRightBorder() {
        assertThat(board.getRightBorder(), is(0));
        board.setRightBorder(5);
        assertThat(board.getRightBorder(), is(5));
        board.setRightBorder(-5);
        assertThat(board.getRightBorder(), is(0));
        board.setRightBorder(200);
        assertThat(board.getRightBorder(), is(9));
    }

    @Test
    public void testCalculateScore() {

        // Bad tile
        when(choice555.getTile()).thenReturn(null);
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getTile()).thenReturn(tile555);

        // Off the board and no left look
        when(choice555.getCol()).thenReturn(-1);
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getCol()).thenReturn(0);        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getCol()).thenReturn(board.getNumberOfCols());        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getCol()).thenReturn(board.getNumberOfCols()-1);        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getCol()).thenReturn(PIECE_COL);

        // Off the board and no top look
        when(choice555.getRow()).thenReturn(-1);
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getRow()).thenReturn(0);        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getRow()).thenReturn(board.getNumberOfRows());        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getRow()).thenReturn(board.getNumberOfRows()-1);        
        assertThat(board.calculateScore(choice555), is(0));
        when(choice555.getRow()).thenReturn(PIECE_ROW);

        // Working the points.
        when(choice555.getRow()).thenReturn(3);
        when(choice555.getCol()).thenReturn(3);
        when(tile555.getValue()).thenReturn(9);
        when(choice555.getTile()).thenReturn(tile555);
        assertThat(board.calculateScore(choice555), is(9));

        // Orientation down
        // Tile is 554
        // Rotation is 0
        when(choice554.getTile()).thenReturn(tile554);
        when(choice554.getRotation()).thenReturn(0);
        when(choice554.getOrientation()).thenReturn(Orientation.DOWN);
        when(choice554.getCol()).thenReturn(PIECE_COL + 1);
        // 1. Row is 0
        when(choice554.getRow()).thenReturn(0);
        assertThat(board.calculateScore(choice554), is(0));
        // 2. Row is >0
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        assertThat(board.calculateScore(choice554), is(0));
        // 3. Piece at (row-1,col) == null
        board.playedTiles[PIECE_ROW-1][PIECE_COL+1] = null;
        assertThat(board.calculateScore(choice554), is(0));
        // 4. Piece at (row-1,col) != null
        board.playedTiles[PIECE_ROW-1][PIECE_COL+1] = tile111;
        assertThat(board.calculateScore(choice554), is(0));
        // 5. col = 0
        when(choice554.getCol()).thenReturn(0);
        assertThat(board.calculateScore(choice554), is(0));
        // 6. col > 0, row > numberOfRows-1
        when(choice554.getCol()).thenReturn(ROWS);
        assertThat(board.calculateScore(choice554), is(0));
        // 7. col > 0, row < numberOfRows-1, piece[row+1][col-1] == null
        when(choice554.getCol()).thenReturn(PIECE_COL + 1);
        board.playedTiles[PIECE_ROW+1][PIECE_COL-1] = null;
        assertThat(board.calculateScore(choice554), is(0));
        // 8. col > 0, row < numberOfRows-1, piece[row+1][col-1] != null
        board.playedTiles[PIECE_ROW+1][PIECE_COL-1] = tile111;
        assertThat(board.calculateScore(choice554), is(0));
        // 9. col = 0, row >= numberOfRows-1
        when(choice554.getCol()).thenReturn(0);
        when(choice554.getRow()).thenReturn(ROWS);
        assertThat(board.calculateScore(choice554), is(0));
        // 10. col = 0, row >= numberOfRows-1, piece[row+1][col] != null
        when(choice554.getCol()).thenReturn(0);
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        board.playedTiles[PIECE_ROW+1][PIECE_COL] = tile111;
        assertThat(board.calculateScore(choice554), is(0));
        // 11. col = 0, row >= numberOfRows-1, piece[row+1][col] == null
        when(choice554.getCol()).thenReturn(0);
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        board.playedTiles[PIECE_ROW+1][PIECE_COL+1] = null;
        assertThat(board.calculateScore(choice554), is(0));
        // 12. col = 0, row >= numberOfRows-1, piece[row+1][col] != null
        when(choice554.getCol()).thenReturn(0);
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        board.playedTiles[PIECE_ROW+1][PIECE_COL+1] = tile111;
        assertThat(board.calculateScore(choice554), is(0));

        // last
        when(choice554.getCol()).thenReturn(PIECE_COL + 1);
        when(choice554.getRow()).thenReturn(PIECE_ROW);
        assertThat(board.calculateScore(choice554), is(0));
    }

    @Test
    public void testHexagonScoring() {
        
        // Use real choices and real tiles
        Choice choice334 = new Choice(new Tile(3,3,4,48),3,5, Orientation.DOWN,120);
        choice334.getTile().setOrientation(Orientation.DOWN);
        Choice choice344 = new Choice(new Tile(3,4,4,50),3,6, Orientation.UP,0);
        choice344.getTile().setOrientation(Orientation.UP);        
        Choice choice444 = new Choice(new Tile(4,4,4,53),4,6, Orientation.DOWN,0);
        choice444.getTile().setOrientation(Orientation.DOWN);
        Choice choice244 = new Choice(new Tile(2,4,4,50),4,5, Orientation.UP,240);
        choice244.getTile().setOrientation(Orientation.UP);
        Choice choice224 = new Choice(new Tile(2,2,4,39),4,4, Orientation.DOWN,0);
        choice224.getTile().setOrientation(Orientation.DOWN);
        Choice choice234 = new Choice(new Tile(2,3,4,42),3,4, Orientation.UP,240);
        choice234.getTile().setOrientation(Orientation.UP);

        // Let's roll around the hexagon testing each one placed.
        board.placeTile(choice334);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        assertThat(board.calculateScore(choice234), is(59));
        board.placeTile(choice234);
        Log.info(board.display(false));

        board.clearBoard();
        board.placeTile(choice334);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice234);
        assertThat(board.calculateScore(choice224), is(58));
        board.placeTile(choice224);
        Log.info(board.display(false));

        board.clearBoard();
        board.placeTile(choice334);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice224);
        board.placeTile(choice234);
        assertThat(board.calculateScore(choice244), is(60));
        board.placeTile(choice244);
        Log.info(board.display(false));

        board.clearBoard();
        board.placeTile(choice334);
        board.placeTile(choice344);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        assertThat(board.calculateScore(choice444), is(62));
        board.placeTile(choice444);
        Log.info(board.display(false));

        board.clearBoard();
        board.placeTile(choice334);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        assertThat(board.calculateScore(choice344), is(61));
        board.placeTile(choice334);
        Log.info(board.display(false));

        // Let's test the bridges (50 + 10)
        board.clearBoard();
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        assertThat(board.calculateScore(choice334), is(60));
        board.placeTile(choice334);
        Log.info(board.display(false));
    }

    @Test
    public void testBridgeScoring() {
        
        // Use real choices and real tiles
        Choice choice334 = new Choice(new Tile(3,3,4,48),3,5, Orientation.DOWN,120);
        choice334.getTile().setOrientation(Orientation.DOWN);
        Choice choice344 = new Choice(new Tile(3,4,4,50),3,6, Orientation.UP,0);
        choice344.getTile().setOrientation(Orientation.UP);        
        Choice choice444 = new Choice(new Tile(4,4,4,53),4,6, Orientation.DOWN,0);
        choice444.getTile().setOrientation(Orientation.DOWN);
        Choice choice244 = new Choice(new Tile(2,4,4,50),4,5, Orientation.UP,240);
        choice244.getTile().setOrientation(Orientation.UP);
        Choice choice224 = new Choice(new Tile(2,2,4,39),4,4, Orientation.DOWN,0);
        choice224.getTile().setOrientation(Orientation.DOWN);
        Choice choice233 = new Choice(new Tile(2,3,3,42),3,3, Orientation.DOWN,0);
        choice233.getTile().setOrientation(Orientation.DOWN);
        Choice choice234 = new Choice(new Tile(2,3,4,42),3,4, Orientation.UP,240);
        choice234.getTile().setOrientation(Orientation.UP);
       

        // Let's test the bridges (40 + 10)
        board.clearBoard();
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        choice334.setRow(2);
        choice334.setRotation(120);
        choice334.setOrientation(Orientation.UP);
        choice334.getTile().setOrientation(Orientation.UP);
        assertThat(board.calculateScore(choice334), is(50));
        board.placeTile(choice334);
        Log.info(board.display(false));

        // Let's test the bridges (40 + 8)
        board.clearBoard();
        choice334 = new Choice(new Tile(3,3,4,48),3,5, Orientation.DOWN,120);
        choice334.getTile().setOrientation(Orientation.DOWN);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice334);
        assertThat(board.calculateScore(choice233), is(48));
        board.placeTile(choice233);
        Log.info(board.display(false));

        // Let's test the bridges (40 + 9)
        board.clearBoard();
        Choice choice225 = new Choice(new Tile(2,2,5,40),4,3, Orientation.UP,0);
        choice225.getTile().setOrientation(Orientation.UP); 
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice234);
        board.placeTile(choice334);
        assertThat(board.calculateScore(choice225), is(49));
        board.placeTile(choice225);
        Log.info(board.display(false));

        // Let's test the bridges (40 + 7)
        board.clearBoard();
        Choice choice124 = new Choice(new Tile(1,2,4,29),5,5, Orientation.DOWN,0);
        choice124.getTile().setOrientation(Orientation.DOWN);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice224);
        board.placeTile(choice234);
        board.placeTile(choice334);
        assertThat(board.calculateScore(choice124), is(47));
        board.placeTile(choice124);
        Log.info(board.display(false));

        // Let's test the bridges (40 + 7)
        board.clearBoard();
        Choice choice144 = new Choice(new Tile(1,4,4,34),4,7, Orientation.UP,120);
        choice144.getTile().setOrientation(Orientation.UP);
        board.placeTile(choice344);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        board.placeTile(choice334);
        assertThat(board.calculateScore(choice144), is(49));
        board.placeTile(choice144);
        Log.info(board.display(false));
    
        // Let's test the bridges (40 + 7)
        board.clearBoard();
        choice344.setCol(7);
        choice344.setRow(3);
        choice344.setRotation(120);
        choice344.setOrientation(Orientation.DOWN);
        choice344.getTile().setOrientation(Orientation.DOWN);
        board.placeTile(choice344);
        board.placeTile(choice444);
        board.placeTile(choice244);
        board.placeTile(choice224);
        board.placeTile(choice234);
        board.placeTile(choice334);
        assertThat(board.calculateScore(choice344), is(51));
        board.placeTile(choice344);
        Log.info(board.display(false));
    }

    @Test
    public void testDisplayBoard() {
        Tile t554 = new Tile(5,5,4);
        t554.setOrientation(Orientation.UP);
        t554.setRotation(240);
        board.playedTiles[5][6] = t554;
        
        Tile t555 = new Tile(5,5,5);
        board.playedTiles[5][5] = t555;

        String info = board.display(false);
        assertThat(info, is(
            "------------------------\n"
            + "           5    6 \n"
            + "------------------------\n"
            + "|    |= --  0-- ^+++++\n"
            + "|    |==\\5   5//5\\++++\n"
            + "|  5 |===\\   //   \\+++\n"
            + "|    |====\\5//5   4\\++\n"
            + "|    |=====v --  0-- +\n"));

        board.playedTiles[5][6] = null;
        t554.setRotation(120);
        board.playedTiles[4][5] = t554;

        info = board.display(false);
        assertThat(info, is(
            "---------------\n"
            + "           5 \n"
            + "---------------\n"
            + "|    |+++++^+++++\n"
            + "|    |++++/4\\++++\n"
            + "|  4 |+++/   \\+++\n"
            + "|    |++/5   5\\++\n"
            + "|    |+ --  0-- +\n"
            + "|    |= --  0-- =\n"
            + "|    |==\\5   5/==\n"
            + "|  5 |===\\   /===\n"
            + "|    |====\\5/====\n"
            + "|    |=====v=====\n"));
            Log.info(info);
    }

}
