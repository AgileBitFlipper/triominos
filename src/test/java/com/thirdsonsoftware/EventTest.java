package com.thirdsonsoftware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EventTest {

    int ROW = 0;
    int COL = 0;

    @Mock
    Player player;

    @Mock
    Tile tile;

    EventManager evtMgr = EventManager.getInstance();

    private Event event;

    @Before
    public void setup() {
        this.event = new Event(EventType.START_A_GAME, player, tile, ROW, COL);
    }

    @Test
    public void testEvent() {
        event.type = EventType.END_A_GAME ;
        assertThat(event.type, is(EventType.END_A_GAME));
    }

    @Test
    public void testCreateEvent() {
        assertThat(event.type, is(EventType.START_A_GAME));
        assertThat(event.col, is(COL));
        assertThat(event.row, is(ROW));
        assertThat(event.tile, is(tile));
        assertThat(event.player, is(player));
    }

    @Test
    public void shouldBeStartingMove() {
        event.startingMove = true;
        assertThat(event.startingMove, is(true));
        assertThat(event.toString(), containsString("Starts:true"));

        event.startingMove = false;
        assertThat(event.startingMove, is(false));
        assertThat(event.toString(), containsString("Starts:false"));
    }

    @Test
    public void shouldBeEndOfRound() {
        event.endOfRound = true;
        assertThat(event.endOfRound, is(true));
        assertThat(event.toString(), containsString("End of Round:true"));

        event.endOfRound = false;
        assertThat(event.endOfRound, is(false));
        assertThat(event.toString(), containsString("End of Round:false"));
    }

    @Test
    public void shouldBeEndOfGame() {
        event.endOfGame = true;
        assertThat(event.endOfGame, is(true));
        assertThat(event.toString(), containsString("End of Game:true"));

        event.endOfGame = false;
        assertThat(event.endOfGame, is(false));
        assertThat(event.toString(), containsString("End of Game:false"));
    }

    @Test
    public void shouldLogEvent() {
        int count = evtMgr.getEvents().size();
        Event.logEvent(event);
        assertThat(evtMgr.getEvents().size(), is(count+1));
    }

    @Test
    public void shouldLogEventType() {
        int count = evtMgr.getEvents().size();
        EventType evtType = EventType.START_A_GAME;
        Event.logEvent(evtType);
        assertThat(evtMgr.getEvents().size(), is(count+1));
    }

    @Test
    public void shouldLogEventTypeAndValue() {
        int count = evtMgr.getEvents().size();
        int value = 30;
        EventType evtType = EventType.START_A_GAME;
        Event.logEvent(evtType, value);
        int size = evtMgr.getEvents().size();
        assertThat(size, is(count+1));
        assertThat(evtMgr.getEvents().get(size-1).type, is(EventType.START_A_GAME));

        count = size;
        evtType = EventType.END_A_GAME;
        Event.logEvent(evtType, value);
        size = evtMgr.getEvents().size();
        assertThat(size, is(count+1));
        assertThat(evtMgr.getEvents().get(size-1).type, is(EventType.END_A_GAME));

        count = size;
        evtType = EventType.START_A_ROUND;
        Event.logEvent(evtType, value);
        size = evtMgr.getEvents().size();
        assertThat(size, is(count+1));
        assertThat(evtMgr.getEvents().get(size-1).type, is(EventType.START_A_ROUND));

        count = size;
        evtType = EventType.END_A_ROUND;
        Event.logEvent(evtType, value);
        size = evtMgr.getEvents().size();
        assertThat(size, is(count+1));
        assertThat(evtMgr.getEvents().get(size-1).type, is(EventType.END_A_ROUND));
    }

    @Test
    public void shouldLogEventByTypeRoundAndTile() {
        int count = evtMgr.getEvents().size();
        int value = 30;
        Tile tile = new Tile(0, 1, 2);
        EventType evtType = EventType.START_A_GAME;
        Event.logEvent(evtType, value, tile);
        int size = evtMgr.getEvents().size();
        assertThat(size, is(count+1));
        assertThat(evtMgr.getEvents().get(size-1).type, is(EventType.START_A_GAME));
        assertThat(evtMgr.getEvents().get(size-1).round, is(30));
        assertThat(evtMgr.getEvents().get(size-1).tile.getCornerA(), is(0));
        assertThat(evtMgr.getEvents().get(size-1).tile.getCornerB(), is(1));
        assertThat(evtMgr.getEvents().get(size-1).tile.getCornerC(), is(2));
    }

    @Test
    public void shouldLogEventStartARoundType() {
        int size = evtMgr.getEvents().size();
        EventType evtType = EventType.START_A_ROUND;
        Event.logEvent(evtType);
        assertThat(evtMgr.getEvents().size(), is(size+1));
        assertThat(evtMgr.getEvents().get(size).type, is(EventType.START_A_ROUND));
    }

    @Test
    public void shouldLogEventEndARoundType() {
        int size = evtMgr.getEvents().size();
        EventType evtType = EventType.END_A_ROUND;
        Event.logEvent(evtType);
        assertThat(evtMgr.getEvents().size(), is(size+1));
        assertThat(evtMgr.getEvents().get(size).type, is(EventType.END_A_ROUND));
    }

    @Test
    public void shouldToString() {
        // The first part of an event is a date/time string, so match the end.
        assertThat(event.toString(), endsWith(
            "Type:START_A_GAME,Game:1,Round:0,Name:null,Tile:tile,Position:(0,0),,Score:0,"
            + "Starts:false,Start Bonus:0,Completed Bridge:false,Completed Hexagon:false,End of Round:false,End of Game:false"));
    }

    @Mock
    private JsonGenerator gen;

    @Test
    public void testSerializeEvent() {
        CustomEventSerializer serializer = new CustomEventSerializer();
        serializer.serialize(new Event(EventType.END_A_GAME), gen, null);

        try {
            verify(gen, times(1)).writeStartObject();
            verify(gen, times(1)).writeEndObject();
            verify(gen, times(1)).writeStringField(eq("eventDateTime"), anyString());
            verify(gen, times(1)).writeStringField(eq("type"), eq(EventType.END_A_GAME.name()));
            verify(gen, times(1)).writeFieldName("Player");
            verify(gen, times(2)).writeObject(any());
            verify(gen, times(1)).writeFieldName("Tile");
            verify(gen, times(1)).writeNumberField(eq("row"), anyInt());
            verify(gen, times(1)).writeNumberField(eq("col"), anyInt());
            verify(gen, times(1)).writeNumberField(eq("score"), anyInt());
            verify(gen, times(1)).writeNumberField(eq("startBonus"), anyInt());
            verify(gen, times(1)).writeBooleanField(eq("startingMove"), anyBoolean());
            verify(gen, times(1)).writeBooleanField(eq("completedAHexagon"), anyBoolean());
            verify(gen, times(1)).writeBooleanField(eq("completedABridge"), anyBoolean());
            verify(gen, times(1)).writeBooleanField(eq("endOfRound"), anyBoolean());
            verify(gen, times(1)).writeBooleanField(eq("endOfGame"), anyBoolean());
            verify(gen, times(1)).writeNumberField(eq("round"), anyInt());
            verify(gen, times(1)).writeNumberField(eq("game"), anyInt());
        } catch (IOException ioe) {
            fail("Shouldn't fail serializing a tile.");
        }
    }
    
    @Test
    public void serializeEventWithException() throws IOException {

        Mockito.doThrow(new IOException()).when(gen).writeNumberField(eq("row"),anyInt());

        CustomEventSerializer serializer = new CustomEventSerializer();
        serializer.serialize(new Event(EventType.END_A_GAME), gen, null);
        assertTrue(true, "The IOException should have been handled correctly.");
    }
}
