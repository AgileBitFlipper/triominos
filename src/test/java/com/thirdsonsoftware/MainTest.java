package com.thirdsonsoftware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.not;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MainTest {

    @Mock
    Triominos triominos;

    private Main main;

    @Before
    public void setup() {
        this.main = new Main();
        doNothing().when(triominos).start();
        doNothing().when(triominos).analyze();
    }

    @Test
    public void shouldMain() {
        String[] args = {"-p", "4"};
        assertThat(main, not(nullValue()));

        main.main(args);
        assertThat(Main.triominos, not(nullValue()));
    }

    @Test
    public void shouldAnalyze() {
        String[] args = { "-a"};
        assertThat(main, not(nullValue()));

        main.main(args);
        assertThat(Main.triominos, not(nullValue()));
    }
}
