package com.thirdsonsoftware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChoiceTest {

    static final int ROW = 5; 
    static final int COL = 5;
    static final int ROW_1 = 6;
    static final int ROT = 120;
    static final int DEFAULT_ROW = 56;
    static final int DEFAULT_COL = 56;

    @Mock
    private Tile tile;

    @Mock
    private Tile tile555;

    private Orientation orientation;
    private Choice choice;

    @Before
    public void setup() {
        orientation = Orientation.UP;
        this.choice = new Choice(tile, ROW, COL, orientation, ROT);
    }

    @Test
    public void shouldGetTile() {
        assertThat(choice.getTile(), is(tile));
    }

    @Test
    public void shouldIsTestForFitOnly() {
        boolean actualValue = choice.isTestForFitOnly();
        assertThat(actualValue, is(false));
        choice.setTestForFitOnly(true);
        actualValue = choice.isTestForFitOnly();
        assertThat(actualValue, is(true));
    }

    @Test
    public void shouldGetRow() {
        assertThat(choice.getRow(), is(ROW));
    }

    @Test
    public void shouldGetCol() {
        assertThat(choice.getCol(), is(COL));
    }

    @Test
    public void shouldGetOrientation() {
        assertThat(choice.getOrientation(), is(orientation));
    }

    @Test
    public void shouldGetRotation() {
        assertThat(choice.getRotation(), is(ROT));
    }

    @Test
    public void shouldGetScore() {
        assertThat(choice.getScore(), is(0));
    }

    @Test
    public void testGetTile() {
        assertThat(choice.getTile(), is(tile));
    }

    @Test
    public void testSetTile() {
        assertThat(choice.getTile(), is(tile));
        choice.setTile(tile555);
        assertThat(choice.getTile(), is(tile555));
    }

    @Test
    public void testGetRow() {
        assertThat(choice.getRow(), is(ROW));
    }

    @Test
    public void testSetRow() {
        assertThat(choice.getRow(), is(ROW));
        choice.setRow(0);
        assertThat(choice.getRow(), is(0));
    }

    @Test
    public void testGetCol() {
        assertThat(choice.getCol(), is(COL));
    }

    @Test
    public void testSetCol() {
        assertThat(choice.getCol(), is(COL));
        choice.setCol(0);
        assertThat(choice.getCol(), is(0));
    }

    @Test
    public void testGetScore() {
        assertThat(choice.getScore(), is(0));
    }

    @Test
    public void testGetTileOrientation() {
        assertThat(choice.getOrientation(), is(Orientation.UP));
    }

    @Test
    public void testSetTileOrientation() {
        assertThat(choice.getOrientation(), is(Orientation.UP));
        choice.setOrientation(Orientation.DOWN);
        assertThat(choice.getOrientation(), is(Orientation.DOWN));
    }

    @Test
    public void testSetRotation() {
        assertThat(choice.getRotation(), is(120));
    }

    @Test
    public void testSetTileRotation() {
        assertThat(choice.getRotation(), is(120));
        choice.setRotation(240);
        assertThat(choice.getRotation(), is(240));
    }
    @Test
    public void testSetScore() {
        assertThat(choice.getScore(), is(0));
        choice.setScore(50);
        assertThat(choice.getScore(), is(50));
    }

    @Test
    public void testEquals() {
        // same object
        assertThat(choice.equals(choice), is(true));
        // null
        assertThat(choice.equals(null), is(false));
        // not same type of object
        Object obj = Integer.valueOf(3);
        assertThat(choice.equals(obj), is(false));
        // not same tile
        Choice differentChoice = new Choice(tile, ROW, COL, orientation, ROT);
        assertThat(choice.equals(differentChoice), is(true));
        differentChoice.setRotation(240);
        assertThat(choice.equals(differentChoice), is(false));
        differentChoice.setOrientation(Orientation.DOWN);
        assertThat(choice.equals(differentChoice), is(false));
        differentChoice.setScore(321);
        assertThat(choice.equals(differentChoice), is(false));
        differentChoice.setRow(ROW - 1);
        assertThat(choice.equals(differentChoice), is(false));
        differentChoice.setCol(COL - 1);
        assertThat(choice.equals(differentChoice), is(false));
        differentChoice.setTile(null);
        assertThat(choice.equals(differentChoice), is(false));
    }

    @Test
    public void testHashCode() {
        Tile hashTile1 = new Tile(0,0,0);
        Choice hashChoice1 = new Choice(hashTile1, 5, 5, Orientation.UP, 120);
        Tile hashTile2 = new Tile(0,0,0);
        Choice hashChoice2 = new Choice(hashTile2, 5, 5, Orientation.UP, 120);

        assertThat(hashChoice1.hashCode(), is(hashChoice2.hashCode()));
    }

    @Test
    public void testHashCodeNoMatch() {
        Tile hashTile1 = new Tile(0,0,0);
        Choice hashChoice1 = new Choice(hashTile1, 5, 5, Orientation.UP, 120);
        Tile hashTile3 = new Tile(0,0,1);
        Choice hashChoice3 = new Choice(hashTile3, 5, 5, Orientation.UP, 120);
        assertThat(hashChoice1.hashCode(), is(not(hashChoice3.hashCode())));
    }
}
