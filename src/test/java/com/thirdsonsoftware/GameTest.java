package com.thirdsonsoftware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    public static final int DEFAULT_NUMBER_OF_PLAYERS = 3;
    public static final int DEFAULT_NUMBER_OF_ROUNDS = 1;
    public static final String PLAYER_A_NAME = "Player A";
    public static final String PLAYER_B_NAME = "Player B";
    public static final String PLAYER_C_NAME = "Player C";
    public static final String EMPTY_TILE_LIST = "[]";
    public static final String EMPTY_ROUND_LIST = "<[]>";
    public static final String UNSORTED_PLAYER_ONE_TRAY = "[0-0-0, 0-0-3, 0-1-1, 0-1-4, 0-2-3, 0-3-3, 0-4-4]";
    public static final String UNSORTED_PLAYER_TWO_TRAY = "[0-0-1, 0-0-4, 0-1-2, 0-1-5, 0-2-4, 0-3-4, 0-4-5]";
    public static final String UNSORTED_PLAYER_THREE_TRAY = "[0-0-2, 0-0-5, 0-1-3, 0-2-2, 0-2-5, 0-3-5, 0-5-5]";
    public static final String EXPECTED_UNSORTED_TILE_ARRAY_LIST = "[0-0-0, 0-0-1, 0-0-2, 0-0-3, 0-0-4, 0-0-5, 0-1-1, 0-1-2,"
            + " 0-1-3, 0-1-4, 0-1-5, 0-2-2, 0-2-3, 0-2-4, 0-2-5, 0-3-3, 0-3-4,"
            + " 0-3-5, 0-4-4, 0-4-5, 0-5-5, 1-1-1, 1-1-2, 1-1-3, 1-1-4, 1-1-5,"
            + " 1-2-2, 1-2-3, 1-2-4, 1-2-5, 1-3-3, 1-3-4, 1-3-5, 1-4-4, 1-4-5,"
            + " 1-5-5, 2-2-2, 2-2-3, 2-2-4, 2-2-5, 2-3-3, 2-3-4, 2-3-5, 2-4-4,"
            + " 2-4-5, 2-5-5, 3-3-3, 3-3-4, 3-3-5, 3-4-4, 3-4-5, 3-5-5, 4-4-4," + " 4-4-5, 5-5-4, 5-5-5]";
    public static final String EXPECTED_UNSORTED_TILE_IMAGE_ARRAY_LIST = "     ^       ^       ^       ^       ^       ^       "
            + "^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       "
            + "^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       "
            + "^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       ^       "
            + "^       ^       ^       ^       ^       ^       ^       ^    \n"
            + "    /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\   "
            + "  /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /0\\     /1\\     /1\\     /1\\   "
            + "  /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\     /1\\   "
            + "  /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /2\\     /3\\     /3\\   "
            + "  /3\\     /3\\     /3\\     /3\\     /4\\     /4\\     /5\\     /5\\   \n"
            + "   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  "
            + " /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  "
            + " /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  "
            + " /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  "
            + " /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\   /   \\  \n"
            + "  /0   0\\ /1   0\\ /2   0\\ /3   0\\ /4   0\\ /5   0\\ /1   1\\ /2   1\\ /3   1\\ /4   1\\ /5   1\\ /2   2\\ "
            + "/3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /1   1\\ /2   1\\ /3   1\\ "
            + "/4   1\\ /5   1\\ /2   2\\ /3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ "
            + "/2   2\\ /3   2\\ /4   2\\ /5   2\\ /3   3\\ /4   3\\ /5   3\\ /4   4\\ /5   4\\ /5   5\\ /3   3\\ /4   3\\ "
            + "/5   3\\ /4   4\\ /5   4\\ /5   5\\ /4   4\\ /5   4\\ /4   5\\ /5   5\\ \n"
            + "  --  1-- --  2-- --  3-- --  4-- --  5-- --  6-- --  7-- --  8-- --  9-- -- 10-- -- 11-- -- 12-- -- 13-- "
            + "-- 14-- -- 15-- -- 16-- -- 17-- -- 18-- -- 19-- -- 20-- -- 21-- -- 22-- -- 23-- -- 24-- -- 25-- -- 26-- "
            + "-- 27-- -- 28-- -- 29-- -- 30-- -- 31-- -- 32-- -- 33-- -- 34-- -- 35-- -- 36-- -- 37-- -- 38-- -- 39-- "
            + "-- 40-- -- 41-- -- 42-- -- 43-- -- 44-- -- 45-- -- 46-- -- 47-- -- 48-- -- 49-- -- 50-- -- 51-- -- 52-- "
            + "-- 53-- -- 54-- -- 55-- -- 56-- ";

    List<Tile> aListOfTiles;
    List<Round> aListOfRounds;

    public static final Tile TILE123 = new Tile(1, 2, 3);
    public static final Tile TILE131 = new Tile(1, 3, 1);

    Game game;

    @Before
    public void setup() {
        game = new Game(DEFAULT_NUMBER_OF_PLAYERS);
        aListOfTiles = new ArrayList<Tile>();
        aListOfTiles.add(TILE123);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void shouldPlay() {
        game.play();
        assertTrue(true);
    }

    @Test
    public void shouldGetNumPlayers() {
        int actualValue = game.getNumPlayers();
        assertThat(actualValue, is(DEFAULT_NUMBER_OF_PLAYERS));
    }

    @Test
    public void shouldToString() {
        String strExpectedGame = "\n\nGame Results:\n  Rounds:\n";
        String strActualGame = game.toString();
        assertThat(strExpectedGame, is(strActualGame));
    }

    @Test
    public void shouldGetRounds() {
        aListOfRounds = new ArrayList<Round>();
        ArrayList<Round> actualValue = game.getRounds();
        assertThat(actualValue, is(aListOfRounds));
    }

    @Test
    public void testNumberOfGetPlayers() {
        assertThat(DEFAULT_NUMBER_OF_PLAYERS, is(game.getNumPlayers()));
    }

    @Test
    public void testSetPlayers() {
        assertThat(DEFAULT_NUMBER_OF_PLAYERS, is(game.getNumPlayers()));
        game.setNumPlayers(2);
        assertThat(2, is(game.getNumPlayers()));

        game.setNumPlayers(1);
        assertThat(2, is(game.getNumPlayers()));

        game.setNumPlayers(10);
        assertThat(2, is(game.getNumPlayers()));
    }

    @Test
    public void testGetPlayers() {
        assertThat(game.getPlayers(), not(nullValue()));
        assertThat(game.getPlayer(0).toString(), 
            is(equalToCompressingWhiteSpace(
                "  Name: Player A\n    Starts: no\n    Score: 0\n"
                + "    Hand (0):\n  [<empty>]\n")));
        assertThat(game.getPlayer(-1), is(nullValue()));
        assertThat(game.getPlayer(10), is(nullValue()));
    }
}