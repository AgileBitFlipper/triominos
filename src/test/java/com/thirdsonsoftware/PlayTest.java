package com.thirdsonsoftware;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class PlayTest {

	private Play play;

	@Mock
	private Player player ;

	@Mock
	private Tile tile;

	@Mock
	private Round round;

	static final String TESTER_NAME = "Tester";
	static final int    TESTER_ID = 13;
	static final int    TESTER_COL = 14;
	static final int    TESTER_ROW = 15;
	static final int    TESTER_SCORE = 1616;
	static final int    TESTER_BONUS = 1717;
	static final int 	TESTER_ROUND = 18;

	@Before
	public void setup() {
		LocalDate now = LocalDate.now();
		this.play = new Play();
		this.play.setWhenPlayed(now);
		this.play.setPlayer(player);
		this.play.setTile(tile);
		this.play.setCol(TESTER_COL);
		this.play.setRow(TESTER_ROW);
		this.play.setScore(TESTER_SCORE);
		this.play.setStartBonus(TESTER_BONUS);
		this.play.setStartingMove(true);
		this.play.setCompletedABridge(true);
		this.play.setCompletedAHexagon(true);
		this.play.setEndOfGame(true);
		this.play.setEndOfRound(true);
		this.play.setRound(round);

		when(player.getName()).thenReturn(TESTER_NAME);
		when(tile.getId()).thenReturn(TESTER_ID);
		when(round.getRoundNumber()).thenReturn(TESTER_ROUND);
	}

	@Test
	public void shouldGetWhenPlayed() {
		LocalDate actualValue = play.getWhenPlayed();

		assertThat(actualValue, is(LocalDate.now()));
		assertThat(actualValue, is(not(nullValue())));
	}

	@Test
	public void shouldGetPlayer() {
		Player actualValue = play.getPlayer();
		assertThat(actualValue, is(not(nullValue())));
		assertThat(actualValue, is(player));
		assertThat(actualValue.getName(), is(TESTER_NAME));
	}

	@Test
	public void shouldGetTile() {
		Tile actualValue = play.getTile();
		assertThat(actualValue, is(not(nullValue())));
		assertThat(actualValue, is(tile));
		assertThat(actualValue.getId(), is(TESTER_ID));
	}

	@Test
	public void shouldGetRow() {
		int actualValue = play.getRow();
		assertThat(actualValue, is(TESTER_ROW));
	}

	@Test
	public void shouldGetCol() {
		int actualValue = play.getCol();
		assertThat(actualValue, is(TESTER_COL));
	}

	@Test
	public void shouldGetScore() {
		int actualValue = play.getScore();
		assertThat(actualValue, is(TESTER_SCORE));
	}

	@Test
	public void shouldGetStartBonus() {
		int actualValue = play.getStartBonus();
		assertThat(actualValue, is(TESTER_BONUS));
	}

	@Test
	public void shouldIsStartingMove() {
		boolean actualValue = play.isStartingMove();
		assertTrue(actualValue);
	}

	@Test
	public void shouldIsCompletedAHexagon() {
		boolean actualValue = play.isCompletedAHexagon();
		assertTrue(actualValue);
	}

	@Test
	public void shouldIsCompletedABridge() {
		boolean actualValue = play.isCompletedABridge();
		assertTrue(actualValue);
	}

	@Test
	public void shouldIsEndOfRound() {
		boolean actualValue = play.isEndOfRound();
		assertTrue(actualValue);
	}

	@Test
	public void shouldIsEndOfGame() {
		boolean actualValue = play.isEndOfGame();
		assertTrue(actualValue);
	}

	@Test
	public void shouldGetRound() {
		Round actualValue = play.getRound();
		assertThat(actualValue, is(not(nullValue())));
		assertThat(actualValue.getRoundNumber(), is(TESTER_ROUND));
	}
}
