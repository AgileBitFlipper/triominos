package com.thirdsonsoftware;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TileTest {

    @Mock
    Player player;

    Face leftFace_234 = new Face(2, 3);
    Face rightFace_234 = new Face(4, 2);
    Face middleFace_234 = new Face(3, 4);

    private Tile tile234;
    private Tile tile555;
    private Tile tile000;

    private static final int CORNER_LEFT = 2;
    private static final int CORNER_RIGHT = 3;
    private static final int CORNER_MIDDLE = 4;
    private static final Orientation DEFAULT_ORIENTATION = Orientation.DOWN;
    private static final int DEFAULT_ROTATION = 0;
    private static final int DEFAULT_ROW = 0;
    private static final int DEFAULT_COL = 0;
    private static final int TILE_234_ID = 42;
    private static final int TILE_555_ID = 56;
    private static final int TILE_000_ID = 1;
    private static final boolean DEFAULT_PLACED = false;
    private static final boolean DEFAULT_IN_TRAY = false;
    private static final boolean DEFAULT_USE_COLORS = true;
    private static final String PLAYER_A = "Player A";

    @Before
    public void setup() {
        this.tile234 = new Tile(2, 3, 4, TILE_234_ID);
        this.tile555 = new Tile(5, 5, 5, TILE_555_ID);
        this.tile000 = new Tile(0, 0, 0, TILE_000_ID);

        tile234.setPlayer(player);
        tile555.setPlayer(player);
        tile000.setPlayer(player);

        Tile.setUseColor(DEFAULT_USE_COLORS);

        when(player.getName()).thenReturn(PLAYER_A);
    }

    @After
    public void teardown() {
        tile234.setPlaced(DEFAULT_PLACED);
        tile234.setRotation(DEFAULT_ROTATION);
        tile234.setCol(DEFAULT_COL);
        tile234.setRow(DEFAULT_ROW);
        tile234.setOrientation(DEFAULT_ORIENTATION);
        Tile.setUseColor(DEFAULT_USE_COLORS);
    }

    @Test
    public void shouldToString() {
        Tile.setUseColor(false);
        assertThat(tile234.toString(), is("2-3-4 (42)"));
        assertThat(tile555.toString(), is("5-5-5 (56)"));
        assertThat(tile000.toString(), is("0-0-0 (1)"));
    }

    @Test
    public void shouldGetId() {
        assertThat(tile234.getId(), is(TILE_234_ID));
        assertThat(tile555.getId(), is(TILE_555_ID));
        assertThat(tile000.getId(), is(TILE_000_ID));
    }

    @Test
    public void shouldGetCornerA() {
        assertThat(tile234.getCornerA(), is(2));
        assertThat(tile555.getCornerA(), is(5));
        assertThat(tile000.getCornerA(), is(0));
    }

    @Test
    public void shouldGetCornerB() {
        assertThat(tile234.getCornerB(), is(3));
        assertThat(tile555.getCornerB(), is(5));
        assertThat(tile000.getCornerB(), is(0));
    }

    @Test
    public void shouldGetCornerC() {
        assertThat(tile234.getCornerC(), is(4));
        assertThat(tile555.getCornerC(), is(5));
        assertThat(tile000.getCornerC(), is(0));
    }

    @Test
    public void shouldGetValue() {
        assertThat(tile234.getValue(), is(9));
        assertThat(tile555.getValue(), is(15));
        assertThat(tile000.getValue(), is(0));
    }

    @Test
    public void shouldGetRotation() {
        assertThat(tile234.getRotation(), is(DEFAULT_ROTATION));
        tile234.setRotation(240);
        assertThat(tile234.getRotation(), is(240));
    }

    @Test
    public void shouldGetOrientation() {
        assertThat(tile234.getOrientation(), is(DEFAULT_ORIENTATION));
        tile234.setOrientation(Orientation.UP);
        assertThat(tile234.getOrientation(), is(Orientation.UP));
    }

    @Test
    public void shouldGetRow() {
        assertThat(tile234.getRow(), is(0));
        tile234.setRow(56);
        assertThat(tile234.getRow(), is(56));
    }

    @Test
    public void shouldGetCol() {
        assertThat(tile234.getCol(), is(DEFAULT_COL));
        tile234.setCol(56);
        assertThat(tile234.getCol(), is(56));
    }

    @Test
    public void shouldIsTriplet() {
        assertThat(tile234.isTriplet(), is(false));
        assertThat(tile555.isTriplet(), is(true));
        assertThat(tile000.isTriplet(), is(true));
    }

    @Test
    public void shouldRotate() {
        assertThat(tile234.getRotation(), is(DEFAULT_ROTATION));
        tile234.rotate(120);
        assertThat(tile234.getRotation(), is(DEFAULT_ROTATION + 120));
    }

    @Test
    public void shouldGetPlaced() {
        assertThat(tile234.getPlaced(), is(DEFAULT_PLACED));
        tile234.setPlaced(true);
        assertThat(tile234.getPlaced(), is(true));
    }

    @Test
    public void shouldGetInTray() {
        assertThat(tile234.getInTray(), is(DEFAULT_IN_TRAY));
        tile234.setInTray(true);
        ;
        assertThat(tile234.getInTray(), is(true));
    }

    @Test
    public void shouldGetLeftFace() {
        assertThat(tile234.getLeftFace(), is(leftFace_234));
    }

    @Test
    public void shouldGetRightFace() {
        assertThat(tile234.getRightFace(), is(rightFace_234));
    }

    @Test
    public void shouldGetMiddleFace() {
        assertThat(tile234.getMiddleFace(), is(middleFace_234));
    }

    @Test
    public void shouldGetPlayer() {
        assertThat(tile234.getPlayer(), is(player));
    }

    private String[] tilePlacedInRow = { 
        "-- 42-- ", 
        "\\3   4/  ", 
        " \\ A /   ", 
        "  \\2/    ", 
        "   v     " };

    @Test
    public void shouldDraw() {
        Boolean solo = true;
        String[] tileRow = { "", "", "", "", "" };

        Tile.setUseColor(false);
        tile234.draw(solo, tileRow);
        assertThat(tileRow, is(tilePlacedInRow));
    }

    private String[] tilePlacedInTray = { 
        "-- 42-- ", 
        "\\3 T 4/  ", 
        " \\ A /   ", 
        "  \\2/    ", 
        "   v     " };
        
    @Test
    public void shouldDrawInTray() {        
        tile234.setInTray(true);
        String[] secondTileRow = { "", "", "", "", "" };
        Tile.setUseColor(false);
        tile234.draw(true, secondTileRow);
        assertThat(secondTileRow, is(tilePlacedInTray));
    }

    private String[] tilePlacedNoPlayer = { 
        "-- 42-- ", 
        "\\3 T 4/  ", 
        " \\   /   ", 
        "  \\2/    ", 
        "   v     " };

    @Test
    public void shouldDrawWithNoPlayer() {        
        tile234.setInTray(true);
        String[] secondTileRow = { "", "", "", "", "" };
        Tile.setUseColor(false);
        when(player.getName()).thenReturn(null);
        tile234.draw(true, secondTileRow);
        assertThat(secondTileRow, is(tilePlacedNoPlayer));
    }

    @Test
    public void shouldGetLeftCorner() {
        assertThat(tile234.getLeftCorner(), is(CORNER_RIGHT));
    }

    @Test
    public void shouldGetRightCorner() {
        assertThat(tile234.getRightCorner(), is(CORNER_MIDDLE));
    }

    @Test
    public void shouldGetMiddleCorner() {
        assertThat(tile234.getMiddleCorner(), is(CORNER_LEFT));
    }

    @Test
    public void shouldCompareTo() {
        Object o = new Tile(CORNER_LEFT, CORNER_RIGHT, CORNER_MIDDLE, TILE_234_ID);
        assertThat(tile234.compareTo(o), is(0));
        assertThat(tile234.compareTo(new Tile(3, 3, 4)), is(-1));
        assertThat(tile234.compareTo(new Tile(2, 4, 4)), is(-1));
        assertThat(tile234.compareTo(new Tile(2, 3, 5)), is(-1));
        assertThat(tile234.compareTo(new Tile(1, 3, 4)), is(1));
        assertThat(tile234.compareTo(new Tile(2, 2, 4)), is(1));
        assertThat(tile234.compareTo(new Tile(2, 3, 3)), is(1));
    }

    @Test
    public void shouldHashCode() {
        assertThat(tile234.hashCode(), is(-1311083541));
    }

    @Test
    public void shouldEquals() {
        Object o = new Tile(CORNER_LEFT, CORNER_MIDDLE, CORNER_RIGHT, TILE_234_ID);
        assertThat(tile234.equals(o), is(false));
        o = Integer.valueOf(3);
        assertThat(tile234.equals(o), is(false));
    }

    @Mock
    private JsonGenerator gen;

    @Test
    public void serializeTile() {
        CustomTileSerializer serializer = new CustomTileSerializer();
        serializer.serialize(new Tile(CORNER_LEFT, CORNER_MIDDLE, CORNER_RIGHT, TILE_234_ID), gen, null);

        try {
            verify(gen, times(1)).writeStartObject();
            verify(gen, times(1)).writeEndObject();
            verify(gen, times(8)).writeNumberField(anyString(),anyInt());
            verify(gen, times(1)).writeBooleanField("placed",false);
            verify(gen, times(1)).writeBooleanField("tray",false);
            verify(gen, times(1)).writeBooleanField("bUseColors",false);
        } catch (IOException ioe) {
            fail("Shouldn't fail serializing a tile.");
        }
    }
    
    @Test
    public void serializeTileWithException() throws IOException {

        Mockito.doThrow(new IOException()).when(gen).writeNumberField(anyString(),anyInt());

        CustomTileSerializer serializer = new CustomTileSerializer();
        serializer.serialize(tile234, gen, null);
        assertTrue(true, "The IOException should have been handled correctly.");
    }
}
