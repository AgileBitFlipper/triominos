package com.thirdsonsoftware;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.lang.reflect.Method;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LogTest {

	@Before
	public void setup() {
	}

	@Test
	public void shouldGetCallerClass() throws ClassNotFoundException {
		Class actualValue = Log.getCallerClass(0);
		assertThat(actualValue, is(Log.class));
	}

	@Test
	public void testInfo() {
        Log.info("This is a log information message.");
		assertTrue(true);
    }

	@Test
    public void testError() {
        Log.error("This is an error message.");
		assertTrue(true);
   }

	@Test
	public void testDebug() {
        Log.debug("This is a debug message.");
		assertTrue(true);
    }

	@Test
	public void testDebugMode() {
		Log.debugMode = true;
        Log.debug("This is a debug message.");
		assertTrue(true);
    }

	@Test
    public void testWarning() {
        Log.warning("This is a warning message.");
		assertTrue(true);
    }

}
