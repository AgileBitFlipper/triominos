package com.thirdsonsoftware;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TriominosTest {

    private static int DEFAULT_GAME_COUNT = 1;
    private static int NUMBER_OF_PLAYERS_TO_TEST_WITH = 4;
            
    private String[] args = {"-p", "2", "-g", "1"};

    @Mock(serializable = true)
    private Game game;

    private Triominos triominos;

    @Before
    public void setup() {
        this.triominos = new Triominos(args);
        triominos.setGame(game);
        triominos.setNumberOfPlayers(NUMBER_OF_PLAYERS_TO_TEST_WITH);
        when(game.getNumPlayers()).thenReturn(NUMBER_OF_PLAYERS_TO_TEST_WITH);
    }

    @After
    public void teardown() {
    }

    @Test
    public void shouldIsAnalyzeResults() {
        assertThat(triominos.isAnalyzeResults(), is(false));
        triominos.setAnalyzeResults(true);
        assertThat(triominos.isAnalyzeResults(), is(true));
    }

    @Test
    public void shouldGetGame() {
        assertThat(triominos.getGame(), is(game));
    }

    @Test
    public void shouldGetGameCount() {
        assertThat(triominos.getGameCount(), is(DEFAULT_GAME_COUNT));
    }

    @Test
    public void shouldGetNumberOfPlayers() {
        assertThat(triominos.getNumberOfPlayers(), is(NUMBER_OF_PLAYERS_TO_TEST_WITH));
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "-p 2 -g 1 -d",
        "-x",
        "thisIsAnArgument"
    })
    public void shouldTestArguments(String[] args) {
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(false));
    }

    @Test
    public void shouldHandleParseException() {
        String[] args = {"-p", "j"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(false));
    }

    // @Test
    // public void shouldSetArguments() {
    //     String[] args = {"thisIsAnArgument"};
    //     triominos.setArguments(args);
    //     assertThat(triominos.getArguments(), not(nullValue()));
    //     assertThat(triominos.getArguments()[0], is("thisIsAnArgument"));
    // }

    @Test
    public void shouldProcessCommandLine() {
        String[] args = {"thisIsAnArgument"};
        triominos.setArguments(args);
        triominos.processCommandLine();
        assertThat(triominos.processCommandLine(), is(true));        
        assertThat(triominos.isAnalyzeResults(), is(false));
    }

    @Test
    public void shouldSetDebugLogMode() {
        String[] args = {"-d"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(true));
        assertThat(Log.isDebugMode(), is(true));
    }

    @Test
    public void shouldSetNumberOfGames() {
        String[] args = {"-g", "100"};
        triominos.setArguments(args);
        assertThat(triominos.getGameCount(), is(Triominos.DEFAULT_GAME_COUNT));
        assertThat(triominos.processCommandLine(), is(true));        
        assertThat(triominos.getGameCount(), is(100));
    }

    @Test
    public void shouldSetNumberOfPlayers() {
        String[] args = {"-p", "3"};
        triominos.setArguments(args);
        assertThat(triominos.getNumberOfPlayers(), is(NUMBER_OF_PLAYERS_TO_TEST_WITH));
        assertThat(triominos.processCommandLine(), is(true));
        assertThat(triominos.getNumberOfPlayers(), is(3));
    }

    @Test
    public void shouldLoadAGameArgument() {
        triominos.saveGame();
        String[] args = {"-l"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(true));
    }
    @Test
    public void shouldNotLoadAGameArgument() {
        File file = new File(triominos.getSavedFilename());
        file.delete();
        String[] args = {"-l"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(false));
    }

    @Test
    public void shouldSetOverMaxNumberOfPlayers() {
        String[] args = {"-p", "50"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(false));
        assertThat(triominos.getNumberOfPlayers(), not(50));

        args = new String[]{"-p", "3"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(true));
        assertThat(triominos.getNumberOfPlayers(), is(3));
    }
        
    @Test
    public void shouldGetSavedFilename() {
        assertThat(triominos.getSavedFilename(), endsWith(Triominos.TRIOMINOS_GAME_FILENAME));
    }

    @Test
    public void shouldSaveGame() {
        String filename = triominos.getSavedFilename();
        assertThat(filename, not(emptyOrNullString()));
        
        File savedFile = new File(filename);
        assertThat(savedFile, not(nullValue()));

        savedFile.delete();
        assertThat(savedFile.exists(), is(false));

        assertThat(triominos.getGame(), not(nullValue()));
        assertThat(triominos.getGame().getNumPlayers(), is(NUMBER_OF_PLAYERS_TO_TEST_WITH));

        triominos.saveGame();
        assertThat(savedFile.exists(), is(true));
    }

    @Test
    public void shouldFnfExceptOnLoad() {
        triominos.setSavedFilename("bad");
        String filename = triominos.getSavedFilename();
        assertThat(filename, not(emptyOrNullString()));
        
        File savedFile = new File(filename);
        assertThat(savedFile, not(nullValue()));
        if (savedFile.exists()) {
            savedFile.delete();
        }

        triominos.loadGame();
    }

    @Test
    public void shouldLoadGame() {
        when(game.getNumPlayers()).thenReturn(NUMBER_OF_PLAYERS_TO_TEST_WITH);
        triominos.saveGame();

        String filename = triominos.getSavedFilename();
        assertThat(filename, not(emptyOrNullString()));
        
        File savedFile = new File(filename);
        assertThat(savedFile, not(nullValue()));
        assertThat(savedFile.exists(), is(true));

        triominos.loadGame();
        assertThat(triominos.getGame().getNumPlayers(), is(NUMBER_OF_PLAYERS_TO_TEST_WITH));
    }


    @Test
    public void testBadSaveGame() {
        File file = new File(Triominos.TRIOMINOS_GAME_FILENAME);
        file.delete();
        triominos.setSavedFilename("");
        triominos.saveGame();
        assertFalse(file.exists());
    }

    @Test
    public void testBadArgument() {
        String[] args = {"-youDontKnowMe"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(false));
    }

    @Test
    public void testHelp() {
        String[] args = {"-h"};
        triominos.setArguments(args);
        assertThat(triominos.processCommandLine(), is(true));
    }

    class FakeGame implements Serializable {
        double num ;
        public FakeGame(double d){
            num = d;
        }
    }

    @Test
    public void testLoadGameFailure() {
        String[] args = {"-l"};
        triominos.setArguments(args);
        try ( FileOutputStream fos = new FileOutputStream(triominos.getSavedFilename());
              ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            oos.writeObject(new FakeGame(32.0));
            oos.close();
        } catch (final FileNotFoundException fnfe){
            assertThat("We should have an FileNotFoundException", true);
        } catch (final IOException ioe) {
            assertThat("We should have an IOException", true);
        }
        assertThat(triominos.processCommandLine(), is(false));
    }
}
