package com.thirdsonsoftware;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FaceTest {

    private int l = 2;
    private int r = 3;

    private Face face;

    @Before
    public void setup() {
        this.face = new Face(l, r);
    }

    @Test
    public void shouldToString() {
        assertThat(face.toString(), is("2-3"));
    }

    @Test
    public void shouldMatch() {
        Object o = new Face(3, 2);
        assertThat(face.match(o), is(true));
    }

    @Test
    public void shouldHashCode() {
        assertThat(face.hashCode(), is(1026));
    }

    @Test
    public void shouldEquals() {
        assertThat(face.equals(null), is(false));
        Object obj = Integer.valueOf(3);
        assertThat(face.equals(obj), is(false));
        obj = new Face(3, 2);
        assertThat(obj.equals(obj), is(true));
        obj = new Face(2, 3);
        assertThat(face.equals(obj), is(true));
        obj = new Face(3, 4);
        assertThat(face.equals(obj), is(false));
        obj = new Face(2, 5);
        assertThat(face.equals(obj), is(false));
    }

    @Test
    public void shouldCompareTo() {
        Object o = new Face(2, 3);
        assertThat(face.compareTo(o), is(0));
        o = new Face(3, 2);
        assertThat(face.compareTo(o), is(-1));
        o = new Face(2, 4);
        assertThat(face.compareTo(o), is(-1));
        o = new Face(1, 1);
        assertThat(face.compareTo(o), is(1));
        o = new Face(2, 1);
        assertThat(face.compareTo(o), is(1));
    }
}
