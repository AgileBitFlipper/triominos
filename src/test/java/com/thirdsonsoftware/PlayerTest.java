package com.thirdsonsoftware;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.core.JsonGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {

    private Player player;

    static int NO_GAMES_WON = 0;
    static int TEST_SCORE = 25;
    static int UNEXPECTED_SCORE = 111;
    static String UNEXPECTED_NAME = "CoolCatLewis";
    static String TEST_NAME = "PlayerX";
    static Tile one;
    static Tile two;
    static Tile tileFiveFives = new Tile(5, 5, 5);
    static Tile tileZeroTriplet = new Tile(0, 0, 0);
    static Board board = new Board();
    ArrayList<Tile> tilesWithAvailableFaces = new ArrayList<Tile>();
    ArrayList<Tile> playedTiles = new ArrayList<Tile>();

    static final String ONE_TILE = new String("--  0-- \n"
                                            + "\\1   1/  \n"
                                            + " \\   /   \n"
                                            + "  \\1/    \n"
                                            + "   v     \n");

    /**
     * Perform this setup before each test is run.
     */
    @Before
    public void setup() {
        one = new Tile(1, 1, 1);
        two = new Tile(1, 2, 3);
        player = new Player(UNEXPECTED_NAME);
        player.setScore(UNEXPECTED_SCORE);
        player.tray.add(one);
        player.tray.add(new Tile(2, 2, 2));
        player.tray.add(new Tile(3, 3, 3));
        player.tray.add(new Tile(4, 4, 4));
        player.tray.add(tileFiveFives);

        tilesWithAvailableFaces.add(two);
        tilesWithAvailableFaces.add(new Tile(1, 2, 1));

        playedTiles.add(two);
        playedTiles.add(new Tile(1, 2, 1));
    }

    @Test
    public void shouldGetZeroTriplet() {
        assertThat(player.determineFirstTile(), is(tileFiveFives));
        player.tray.remove(tileFiveFives);
        player.tray.add(tileZeroTriplet);
        assertThat(player.determineFirstTile(), is(tileZeroTriplet));
    }

    @Test
    public void shouldGetWonAGameCount() {
        assertThat(player.getWonAGameCount(), is(NO_GAMES_WON));
    }

    @Test
    public void shouldGetMode() {
        assertThat(player.getMode(), is(Mode.RELEASE));
    }

    @Test
    public void shouldGetScore() {
        assertThat(player.getScore(), is(UNEXPECTED_SCORE));
    }

    @Test
    public void testSetScore() {
        player.setScore(TEST_SCORE);
        assertThat(player.getScore(), is(TEST_SCORE));
    }

    @Test
    public void testSetName() {
        player.setName(TEST_NAME);
        assertThat(player.getName(), is(TEST_NAME));
    }

    @Test
    public void shouldGetName() {
        assertThat(player.getName(), is(UNEXPECTED_NAME));
    }

    @Test
    public void shouldGetStarts() {
        assertThat(player.getStarts(), is(not(true)));
    }

    @Test
    public void shouldGetStartingTile() {
        player.determineFirstTile();
        assertThat(player.getStartingTile(), is(tileFiveFives));
    }

    @Test
    public void shouldGetTray() {
        List<Tile> actualValue = player.getTray();
        assertThat(actualValue.size(), is(5));
    }

    @Test
    public void shouldDrawTile() {
        ArrayList<Tile> tilePool = new ArrayList<Tile>();
        tilePool.add(two);
        boolean actualValue = player.drawTile(tilePool, 1);
        assertThat(actualValue, is(true));
    }

    @Test
    public void shouldHighestValueTile() {
        Tile highest = player.highestValueTile();
        assertThat(highest, is(tileFiveFives));
        player.tray.clear();
        assertThat(player.highestValueTile(), is(nullValue()));
    }

    @Test
    public void shouldDetermineFirstTile() {
        Tile actualValue = player.determineFirstTile();
        assertThat(actualValue, is(tileFiveFives));
    }

    @Test
    public void testSetStarts() {
        player.setStarts(true);
        assertThat(player.getStarts(), is(true));
    }

    @Test
    public void shouldPlayATile() {
        board.clearBoard();
        player.setStarts(true);
        assertThat(board.count(), is(0));
        player.playATile(board, playedTiles, tilesWithAvailableFaces);
        assertThat(board.count(), is(1));
    }

    @Test
    public void shouldGetLargestTriplet() {
        assertThat(player.getLargestTriplet(), is(tileFiveFives));
    }

    @Test
    public void shouldGetLargestValuedTile() {
        assertThat(player.getLargestValuedTile(), is(tileFiveFives));
        Tile newTile = new Tile(0, 0, 0);
        player.tray.add(newTile);
        assertThat(player.getLargestValuedTile(), is(tileFiveFives));
        player.tray.remove(newTile);
    }

    @Test
    public void shouldAddTile() {
        StringBuilder strBuilder = new StringBuilder(50);
        String[] rows = new String[5];
        Tile tile = new Tile(1, 1, 1);
        for (int i = 0; i < 5; i++) {
            rows[i] = "";
        }
        Player.addTile(rows, tile, true);
        for (String strRow : rows) {
            strBuilder.append(strRow).append("\n");
        }
        assertThat(strBuilder.toString(), is(ONE_TILE));
    }

    @Test
    public void shouldShowTile() {
        Tile tile = new Tile(1, 1, 1);

        String actualValue = Player.showTile(tile);

        assertThat(actualValue, is(ONE_TILE));
    }

    static String TWO_TILES_LEFT_AND_RIGHT = new String("--  0-- --  0-- \n"
                                                      + "\\1   1/  \\2   3/  \n"
                                                      + " \\   /    \\   /   \n"
                                                      + "  \\1/      \\1/    \n"
                                                      + "   v        v     \n");

    @Test
    public void shouldShowTwoTilesLeftAndRight() {
        assertThat(Player.showTwoTilesLeftAndRight(one, two), is(TWO_TILES_LEFT_AND_RIGHT));
    }


    static String TWO_TILES_TOP_AND_BOTTOM = new String("--  0-- \n"
                                                      + "\\1   1/  \n"
                                                      + " \\   /   \n"
                                                      + "  \\1/    \n"
                                                      + "   v     \n"
                                                      + "--  0-- \n"
                                                      + "\\2   3/  \n"
                                                      + " \\   /   \n"
                                                      + "  \\1/    \n"
                                                      + "   v     \n");

    @Test
    public void shouldShowTwoTilesTopAndBottom() {
        assertThat(Player.showTwoTilesTopAndBottom(one, two), is(TWO_TILES_TOP_AND_BOTTOM));
    }

    @Test
    public void shouldToString() {
        String actualValue = player.toString();
        assertThat(actualValue, not(nullValue()));
        assertThat(actualValue, not(""));
    }

    static final int playerHashCode = 282768411;

    @Test
    public void shouldHashCode() {
        assertEquals(playerHashCode, player.hashCode());
   }

    @Test
    public void shouldEquals() {
        assertThat(player.equals(one.getPlayer()), is(false));
    }

    @Test
    public void shouldEqualsWithDifferentPlayerName() {
        Player duplicatePlayer = new Player(TEST_NAME);
        assertThat(player.equals(duplicatePlayer), is(false));
    }

    @Test
    public void shouldEqualsWithSamePlayerName() {
        Player duplicatePlayer = new Player(UNEXPECTED_NAME);
        assertThat(player.equals(duplicatePlayer), is(true));
    }

    static String TWO_TILES = new String("    Tiles: --  0--  - --  0-- \n"
                                    + "           \\2   3/   - \\1   1/  \n"
                                    + "            \\   /     -  \\   /   \n"
                                    + "             \\1/       -   \\1/    \n"
                                    + "              v         -    v     \n");

    @Test
    public void testDisplayComparisonOfTwoTiles() {
        assertThat(player.displayComparisonOfTwoTiles(two, one), is(TWO_TILES));
    }

    static String ONE_TILE_2ND_NULL = new String("    Tiles: --  0--  - \n"
                                               + "           \\2   3/   - \n"
                                               + "            \\   /     -  none \n"
                                               + "             \\1/       - \n"
                                               + "              v         - \n");

    @Test
    public void testDisplayComparisonOfTwoTilesWithFirstNull() {
        assertThat(player.displayComparisonOfTwoTiles(null, one), is(ONE_TILE_1ST_NULL));
    }
 
    static String ONE_TILE_1ST_NULL = new String("    Tiles:       - --  0-- \n"
                                               + "                 - \\1   1/  \n"
                                               + "            none -  \\   /   \n"
                                               + "                 -   \\1/    \n"
                                               + "                 -    v     \n");

    @Test
    public void testDisplayComparisonOfTwoTilesWithSecondNull() {
        assertThat(player.displayComparisonOfTwoTiles(two, null), is(ONE_TILE_2ND_NULL));
    }

    static String TWO_TILES_ONE_ROTATED = new String("    Tiles:    ^        - --  0-- \n"
                                                   + "             /2\\      - \\1   1/  \n"
                                                   + "            /   \\    -  \\   /   \n"
                                                   + "           /1   3\\  -   \\1/    \n"
                                                   + "           --  0--  -    v     \n");

    @Test
    public void testDisplayComparisonOfTwoTilesOneRotated() {
        two.rotate(90);
        two.setOrientation(Orientation.UP);
        assertThat(player.displayComparisonOfTwoTiles(two, one), is(TWO_TILES_ONE_ROTATED));
    }

    @Test
    public void testShowTwoTilesTopAndBottom() {
        assertThat(Player.showTwoTilesTopAndBottom(one, two), is(TWO_TILES_TOP_AND_BOTTOM));
    }

    @Test
    public void testShowTwoTilesLeftAndRight() {
        assertThat(Player.showTwoTilesLeftAndRight(one, two), is(TWO_TILES_LEFT_AND_RIGHT));
    }

    @Test
    public void testShowTile() {
        assertThat(Player.showTile(one), is(ONE_TILE));
    }

    @Test
    public void testGetLargestTripletWithoutTriples() {
        player.tray.clear();
        player.tray.add(new Tile(0, 0, 1));
        player.tray.add(new Tile(0, 1, 2));
        player.tray.add(new Tile(1, 2, 3));
        assertThat(player.getLargestTriplet(), is(nullValue()));
    }

    @Test
    public void testGetLargestTripletWithoutThree() {
        Tile onesTile = new Tile(1, 1, 1);
        Tile twosTile = new Tile(2, 2, 2);
        Tile threesTile = new Tile(3, 3, 3);
        player.tray.clear();
        player.tray.add(onesTile);
        player.tray.add(threesTile);
        player.tray.add(twosTile);
        assertThat(player.getLargestTriplet(), is(threesTile));
    }

    @Test
    public void testDisplayChoicesWithNoChoices() {
        player.setMode(Mode.DEBUG);
        assertThat(player.playATile(board, playedTiles, tilesWithAvailableFaces), is(tileFiveFives));
        tilesWithAvailableFaces.clear();
        assertThat(player.playATile(board, playedTiles, tilesWithAvailableFaces), is(nullValue()));
        player.setMode(Mode.RELEASE);
    }

    @Test
    public void testTileHasAnEmptyFace() {
        board.clearBoard();
        board.playedTiles[55][56] = new Tile(1, 1, 1);
        board.playedTiles[56][57] = new Tile(1, 1, 2);
        one.setCol(0);
        one.setRow(0);
        assertThat(player.tileHasAnEmptyFace(board, one), is(false));
    }

    @Test
    public void testTileHasAnEmptyFaceMiddle() {
        board.clearBoard();
        board.playedTiles[55][56] = new Tile(1, 1, 1);
        board.playedTiles[56][57] = new Tile(1, 1, 2);
        one.setCol(56);
        one.setRow(56);
        assertThat(player.tileHasAnEmptyFace(board, one), is(true));
        one.setCol(111);
        one.setRow(111);
        assertThat(player.tileHasAnEmptyFace(board, one), is(true));
    }

    @Test
    public void testDisplayChoicesWithOneChoices() {
        ArrayList<Choice> choices = new ArrayList<Choice>(
            Arrays.asList(
                new Choice(new Tile(2, 4, 4), 57, 56, Orientation.DOWN, 120)));
        String display = player.displayChoices("Fred", choices);
        assertThat(display, is("Fred (1):\n  [2-4-4 (0)]\n"));
    }

    @Test
    public void testDisplayChoicesWithTwoChoices() {
        ArrayList<Choice> choices = new ArrayList<Choice>(
            Arrays.asList(
                new Choice(new Tile(2, 2, 2), 56, 56, Orientation.UP, 120),
                new Choice(new Tile(2, 4, 4), 57, 56, Orientation.DOWN, 120)));
        String display = player.displayChoices("Fred", choices);
        assertThat(display, is("Fred (2):\n  [2-2-2 (0), 2-4-4 (0)]\n"));
    }

    @Test
    public void testPlayATileWithTwoChoices() {
        board.clearBoard();
        player.tray.clear();
        playedTiles.clear();
        tilesWithAvailableFaces.clear();
        player.tray.add(one);
        player.playATile(board, playedTiles, tilesWithAvailableFaces);
        player.tray.add(new Tile(1, 1, 2));
        Tile tile113 = new Tile(1, 1, 3);
        player.tray.add(tile113);
        assertThat(player.playATile(board, playedTiles, tilesWithAvailableFaces), is(tile113));
    }

    @Test
    public void testPlayFirstTileWithNoTriples() {
        board.clearBoard();
        playedTiles.clear();
        tilesWithAvailableFaces.clear();
        Tile aZeroOneOneTile = new Tile(0, 1, 1);
        Tile aTwoThreeFiveTile = new Tile(2, 3, 5);
        Tile aOneTwoThreeTile = new Tile(1, 2, 3);
        player.tray.clear();
        player.tray.add(aZeroOneOneTile);
        player.tray.add(aTwoThreeFiveTile);
        player.tray.add(aOneTwoThreeTile);
        assertThat(player.playATile(board, playedTiles, tilesWithAvailableFaces), is(not(nullValue())));
    }

    @Test
    public void testGetTileFromTrayForLeftFace() {
        Tile tile224 = new Tile(2, 2, 4);
        Face left = new Face(2, 2);
        ArrayList<Choice> expected = new ArrayList<Choice>(
            Arrays.asList(new Choice(new Tile(2, 2, 2), 56, 56, Orientation.UP, 120)));
        ArrayList<Choice> choices = player.getTileFromTrayForLeftFace(tile224, left, 56, 56) ;
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));

        player.setMode(Mode.DEBUG);
        choices = player.getTileFromTrayForLeftFace(tile224, left, 56, 56) ;
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));
        player.setMode(Mode.RELEASE);
    }

    @Test
    public void testGetTileFromTrayForRightFace() {
        Tile tile224 = new Tile(2, 2, 4);
        Face left = new Face(2, 2);
        Choice choice = new Choice(new Tile(2, 2, 2), 56, 56, Orientation.UP, 120);
        ArrayList<Choice> expected = new ArrayList<Choice>(
            Arrays.asList(choice));
        choice.setRotation(0);
        ArrayList<Choice> choices = player.getTileFromTrayForRightFace(tile224, left, 56, 56) ;
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));

        player.setMode(Mode.DEBUG);
        choices = player.getTileFromTrayForRightFace(tile224, left, 56, 56) ;
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));
        player.setMode(Mode.RELEASE);
    }

    @Test
    public void shouldGetTileFromTrayForMiddleFace(){
        Tile tile224 = new Tile(2, 2, 4);
        Face left = new Face(2, 2);
        Choice choice = new Choice(new Tile(2, 2, 2), 56, 56, Orientation.UP, 240);
        ArrayList<Choice> expected = new ArrayList<Choice>(
            Arrays.asList(choice));
       
        ArrayList<Choice> choices = player.getTileFromTrayForMiddleFace(tile224, left, 56, 56);
        assertThat(choices, is(not(nullValue())));
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));

        player.setMode(Mode.DEBUG);
        choices = player.getTileFromTrayForMiddleFace(tile224, left, 56, 56);
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));

        choice.setOrientation(Orientation.UP);
        choice.setRow(57);
        choices = player.getTileFromTrayForMiddleFace(tile224, left, 57, 56);
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));

        tile224.setOrientation(Orientation.UP);
        choice.setOrientation(Orientation.DOWN);
        choice.setRotation(120);
        choice.setRow(55);
        choices = player.getTileFromTrayForMiddleFace(tile224, left, 55, 56);
        assertThat(choices.size(), is(expected.size()));
        assertThat(choices.get(0), is(expected.get(0)));
        player.setMode(Mode.RELEASE);
    }

    String displayTileAsString = "CoolCatLewis (2):\n  [5-5-5 (0), 0-0-0 (0)]\n";
    String displayTileAsTile = new String(
        "CoolCatLewis (2):\n"
        + "         ^       ^    \n"
        + "        /5\\     /0\\   \n"
        + "       /   \\   /   \\  \n"
        + "      /5   5\\ /0   0\\ \n"
        + "      --  0-- --  0-- \n"); 

    @Test
    public void testDisplayTilesAsString() {
        Boolean asTile = false;
        ArrayList<Tile> tiles = new ArrayList<Tile>(Arrays.asList(tileFiveFives, tileZeroTriplet));
        String list = Player.displayTiles(asTile, player.getName(), tiles);
        assertThat(list, is(displayTileAsString));
    }

    @Test
    public void testDisplayTilesAsTiles() {
        Boolean asTile = true;
        tileFiveFives.setPlaced(false);
        tileFiveFives.setPlayer(null);
        ArrayList<Tile> tiles = new ArrayList<Tile>(Arrays.asList(tileFiveFives, tileZeroTriplet));
        String list = Player.displayTiles(asTile, player.getName(), tiles);
        assertThat(list, is(displayTileAsTile));
    }

    @Mock
    private JsonGenerator gen;

    @Test
    public void testSerializePlayer() {
        CustomPlayerSerializer serializer = new CustomPlayerSerializer();
        serializer.serialize(new Player("TestName"), gen, null);

        try {
            verify(gen, times(1)).writeStartObject();
            verify(gen, times(1)).writeEndObject();
            verify(gen, times(2)).writeNumberField(anyString(),anyInt());
            verify(gen, times(1)).writeBooleanField("starts",false);
            verify(gen, times(1)).writeFieldName("startingTile");
            verify(gen, times(1)).writeObject(any());
        } catch (IOException ioe) {
            fail("Shouldn't fail serializing a tile.");
        }
    }
    
    @Test
    public void serializePlayerWithException() throws IOException {

        Mockito.doThrow(new IOException()).when(gen).writeNumberField(anyString(),anyInt());

        CustomPlayerSerializer serializer = new CustomPlayerSerializer();
        serializer.serialize(new Player("PlayerTest"), gen, null);
        assertTrue(true, "The IOException should have been handled correctly.");
    }
}
