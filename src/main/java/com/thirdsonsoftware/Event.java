/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

    static final long serialVersionUID = 43L;

    Date eventDateTime;

    // What type of event was this
    EventType type;

    // Played by
    Player player;

    // Tile played, including orientation and rotation
    Tile tile;

    // Location played
    int row;
    int col;

    // Score received for play
    int score;

    // Received a start bonus
    int startBonus;
    boolean startingMove;

    // Specialized moves
    boolean completedAHexagon;

    // Bridge?
    boolean completedABridge;

    // Final move
    boolean endOfRound;

    // Final round
    boolean endOfGame;

    // What was the current state of the round?
    int round;

    // Making the game static allows us to set it and forget it
    protected int game = 1;

    // Generic Event
    public Event(EventType evtType) {
        type = evtType;
        eventDateTime = new Date();
    }

    /**
     * Default constructor.
     * 
     * @param eventType type of event
     * @param player player
     * @param tile tile
     * @param row row
     * @param column column
     */
    public Event(EventType eventType, Player player, Tile tile, int row, int column) {
        eventDateTime = new Date();
        this.type = eventType;
        this.player = player;
        this.tile = tile;
        this.row = row;
        this.col = column;
    }

    private static final String TRUE = "true";
    private static final String FALSE = "false";

    /**
     * Reflect the event as a string.
     */
    public String toString() {
        StringBuilder strEvent = new StringBuilder(100);
        strEvent.append(eventDateTime).append(",");
        strEvent.append("Type:").append(type).append(",");
        strEvent.append("Game:").append(game).append(",");
        strEvent.append("Round:").append(round).append(",");
        strEvent.append("Name:").append((player != null) ? player.getName() : "").append(",");
        strEvent.append("Tile:").append((tile != null) ? tile : "").append(",");
        strEvent.append(String.format("Position:(%d,%d),", row, col)).append(",");
        strEvent.append("Score:").append(score).append(",");
        strEvent.append("Starts:").append(startingMove ? TRUE : FALSE).append(",");
        strEvent.append("Start Bonus:").append(startBonus).append(",");
        strEvent.append("Completed Bridge:").append(completedABridge).append(",");
        strEvent.append("Completed Hexagon:").append(completedAHexagon).append(",");
        strEvent.append("End of Round:").append(endOfRound ? TRUE : FALSE).append(",");
        strEvent.append("End of Game:").append(endOfGame ? TRUE : FALSE);
        return strEvent.toString();
    }

    static void logEvent(Event evt) {
        EventManager.getInstance().logEvent(evt);
    }

    static void logEvent(EventType type) {
        Event event = new Event(type);
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, int value) {
        Event event = new Event(type);
        if (type == EventType.START_A_GAME || type == EventType.END_A_GAME) {
            event.game = value;
        } else if (type == EventType.START_A_ROUND || type == EventType.END_A_ROUND) {
            event.round = value;
        }
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r) {
        Event event = new Event(type);
        event.round = r.getRoundNumber();
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, int r, Tile t) {
        Event event = new Event(type);
        event.round = r;
        event.tile = t;
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Round r, Player p) {
        Event event = new Event(type);
        event.round = r.getRoundNumber();
        event.tile = p.getStartingTile();
        event.player = p;
        EventManager.getInstance().logEvent(event);
    }

    static void logEvent(EventType type, Tile t, Player p, int round) {
        Event event = new Event(type);
        event.tile = t;
        event.player = p;
        event.round = round;
        EventManager.getInstance().logEvent(event);
    }

}
