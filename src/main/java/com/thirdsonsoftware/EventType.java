/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 5:07 PM
 */

package com.thirdsonsoftware;

public enum EventType {
    START_A_GAME,
    START_A_ROUND,
    SETUP_PLAYERS,
    GENERATE_TILES,
    SHUFFLE_TILES,
    SETUP_PLAYER_TRAY,
    HIGHEST_TILE_START,
    TRIPLE_ZERO_BONUS,
    TRIPLE_PLAY_BONUS,
    DRAW_A_TILE,
    PLACE_A_TILE,
    FAIL_CORNER_TEST,
    CREATE_A_HEXAGON,
    CREATE_A_BRIDGE,
    WIN_A_ROUND_BY_EMPTY_TRAY,
    WIN_A_ROUND_BY_FEWEST_TILES,
    WIN_A_GAME_BY_POINTS,
    END_A_ROUND,
    END_A_GAME,
    FAIL_FACE_TEST,
    PLAYER_STARTS_A_ROUND,
}
