/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.time.LocalDate;

public class Play {


    // When
    LocalDate whenPlayed;

    // Played by
    Player player;

    // Tile played, including orientation and rotation
    Tile tile;

    // Location played
    int row;
    int col;

    // Score received for play
    int score;

    // Received a start bonus
    int startBonus;
    boolean startingMove;

    // Specialized moves
    boolean completedAHexagon;

    // Bridge?
    boolean completedABridge;

    // Final move
    boolean endOfRound;

    // Final round
    boolean endOfGame;

    // What was the current state of the round?
    Round round;


    /**
     * Retrieve the date and time when played.
     * 
     * @return the whenPlayed
     */
    public LocalDate getWhenPlayed() {
        return whenPlayed;
    }

    /**
     * Set the data and time when played.
     * 
     * @param whenPlayed the whenPlayed to set
     */
    public void setWhenPlayed(LocalDate whenPlayed) {
        this.whenPlayed = whenPlayed;
    }

    /**
     * Retrieve the player.
     * 
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Set the player.
     * 
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Retrieve the tile.
     * 
     * @return the tile
     */
    public Tile getTile() {
        return tile;
    }

    /**
     * Set the tile.
     * 
     * @param tile the tile to set
     */
    public void setTile(Tile tile) {
        this.tile = tile;
    }

    /**
     * Retrieve the row.
     * 
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * Set the row.
     * 
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Get the column.
     * 
     * @return the col
     */
    public int getCol() {
        return col;
    }

    /**
     * Set the column.
     * 
     * @param col the col to set
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Retrieve the score.
     * 
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * Set the score.
     * 
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Retrieve the earned start bonus.
     * 
     * @return the startBonus
     */
    public int getStartBonus() {
        return startBonus;
    }

    /**
     * Set the start bonus.
     * 
     * @param startBonus the startBonus to set
     */
    public void setStartBonus(int startBonus) {
        this.startBonus = startBonus;
    }

    /**
     * Determine if this is the starting move.
     * 
     * @return the startingMove
     */
    public boolean isStartingMove() {
        return startingMove;
    }

    /**
     * Set the starting move.
     * 
     * @param startingMove the startingMove to set
     */
    public void setStartingMove(boolean startingMove) {
        this.startingMove = startingMove;
    }

    /**
     * Determine if this is a completed hexagon.
     * 
     * @return the completedAHexagon
     */
    public boolean isCompletedAHexagon() {
        return completedAHexagon;
    }

    /**
     * Set the completed hexagon flag.
     * 
     * @param completedAHexagon the completedAHexagon to set
     */
    public void setCompletedAHexagon(boolean completedAHexagon) {
        this.completedAHexagon = completedAHexagon;
    }

    /**
     * Determines if a bridge was completed.
     * 
     * @return the completedABridge
     */
    public boolean isCompletedABridge() {
        return completedABridge;
    }

    /**
     * Set if a bridge was completed.
     * 
     * @param completedABridge the completedABridge to set
     */
    public void setCompletedABridge(boolean completedABridge) {
        this.completedABridge = completedABridge;
    }

    /**
     * Is this play the end of a round.
     * 
     * @return the endOfRound
     */
    public boolean isEndOfRound() {
        return endOfRound;
    }

    /**
     * Set this play is end of a round.
     * 
     * @param endOfRound the endOfRound to set
     */
    public void setEndOfRound(boolean endOfRound) {
        this.endOfRound = endOfRound;
    }

    /**
     * Is this the end of the game.
     * 
     * @return the endOfGame
     */
    public boolean isEndOfGame() {
        return endOfGame;
    }

    /**
     * Set this as the end of the game.
     * 
     * @param endOfGame the endOfGame to set
     */
    public void setEndOfGame(boolean endOfGame) {
        this.endOfGame = endOfGame;
    }

    /**
     * What round is this.
     * 
     * @return the round
     */
    public Round getRound() {
        return round;
    }

    /**
     * Set the round.
     * 
     * @param round the round to set
     */
    public void setRound(Round round) {
        this.round = round;
    }
}
