/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

public class Log {

    static boolean debugMode = false;

    // Foreground colors
    public static final String RESET  = "\u001B[0m";
    public static final String BLACK  = "\u001B[30m";
    public static final String RED    = "\u001B[31m";
    public static final String GREEN  = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";
    public static final String BLUE   = "\u001B[34m";
    public static final String PURPLE = "\u001B[35m";
    public static final String CYAN   = "\u001B[36m";
    public static final String WHITE  = "\u001B[37m";

    // Background colors
    public static final String BLACK_BG  = "\u001B[40m";
    public static final String RED_BG    = "\u001B[41m";
    public static final String GREEN_BG  = "\u001B[42m";
    public static final String YELLOW_BG = "\u001B[43m";
    public static final String BLUE_BG   = "\u001B[44m";
    public static final String PURPLE_BG = "\u001B[45m";
    public static final String CYAN_BG   = "\u001B[46m";
    public static final String WHITE_BG  = "\u001B[47m";

    /**
     * Utility classes should not have a public constructor.
     */
    private Log() {
        throw new IllegalStateException("ThirdSonSoftware Log Utility class");
    }

    /**
     * Retrieve the calling class.
     * 
     * @param level log level
     * @return a class for the given level
     * @throws ClassNotFoundException unable to find the class
     */
    public static Class getCallerClass(int level) throws ClassNotFoundException {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String rawFullyQualifiedName = stElements[level + 1].toString().split("\\(")[0];
        return Class.forName(rawFullyQualifiedName.substring(0, rawFullyQualifiedName.lastIndexOf('.')));
    }

    /**
     * Retrieve our debug logging mode.
     * @return true if debug logging is enabled.
     */
    public static boolean isDebugMode() {
        return debugMode;
    }

    /**
     * Update our logging mode.
     * @param debugMode true for debug logging.
     */
    public static void setDebugMode(boolean debugMode) {
        Log.debugMode = debugMode;
    }

    /**
     * Provide the ability to log info messages.
     * 
     * @param message to log
     */
    public static void info(String message) {
        System.out.println(WHITE + message + RESET);
    }

    /**
     * Provide the ability to log error messages.
     * 
     * @param message to log
     */
    public static void error(String message) {
        System.out.println(RED + message + RESET);
    }

    /**
     * Provide the ability to log debug messages.
     * 
     * @param message to log
     */
    public static void debug(String message) {
        if (debugMode) {
            System.out.println(BLUE + message + RESET);
        }
    }

    /**
     * Provide the ability to log warning messages.
     * 
     * @param message to log
     */
    public static void warning(String message) {
        System.out.println(YELLOW + message + RESET);
    }

    // Todo: Add logging to a file support
}
