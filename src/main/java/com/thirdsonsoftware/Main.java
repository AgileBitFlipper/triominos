/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:57 PM
 */

package com.thirdsonsoftware;

import java.util.Arrays;

class Main {

    static Triominos triominos;

    public static void main(String[] args) {

        // Process the command line options
        triominos = new Triominos(args);

        // If we successfully processed them
        if (triominos.processCommandLine()) {

            // If the user wants to anaylze the game results, let's only do that
            if (triominos.isAnalyzeResults()) {

                triominos.analyze();

            // If the user just want's help, let's not do anything else.
            } else if (!Arrays.asList(args).contains("-h") 
                && !Arrays.asList(args).contains("-help")) {

                // Otherwise, let's play a game
                triominos.start();
            }
        }
    }
}
