package com.thirdsonsoftware;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Provide the basic functionality of the game of Triominos.
 */
public class Triominos {

    static final String TRIOMINOS_GAME_FILENAME = "triominos.ser";
    static final int DEFAULT_GAME_COUNT = 1;
    static final int DEFAULT_NUMBER_OF_PLAYERS = 2;
    static final int MAX_NUMBER_OF_PLAYERS = 5;

    int gameCount = DEFAULT_GAME_COUNT;
    int numberOfPlayers = DEFAULT_NUMBER_OF_PLAYERS;

    String triominosFilename = TRIOMINOS_GAME_FILENAME;

    boolean analyzeResults;

    Game game;

    String[] arguments;

    /**
     * Let's start the game of triominos with the arguments provided.
     * @param args - what options do we want to play with?
     */
    Triominos(final String[] args) {
        Log.info("Creating a new instance of Triominos!");
        arguments = args;
    }

    /**
     * Retrieve the arguments from the command line.
     * @return String array of options
     */
    public String[] getArguments() {
        return arguments.clone();
    }

    /**
     * Set the arguments for the game.
     * @param arguments - array of string options
     */
    public void setArguments(String[] arguments) {
        this.arguments = arguments.clone();
    }

    /**
     * Analyze previous game results.
     */
    public void analyze() {
        Log.info("Starting analysis of Triominos games!");

        // Let's dump all the Event data to a text file
        EventManager.getInstance().dumpAllEventData();

        // Let's convert any object files not converted to JSON
        EventManager.getInstance().convertToJson();

        // Let's kick off the analysis phase
        AnalyzeResults.getInstance().analyzeResults();
    }

    /**
     * Let's start our games.
     */
    public void start() {
        Log.info("Starting Triominos!");

        // Let's loop through the number of games we were asked to play
        for (int gameIndex = 1; gameIndex <= getGameCount(); gameIndex++) {

            // Clear out any events before we get started with the next game
            EventManager.getInstance().clearEvents();

            Event.logEvent(EventType.START_A_GAME, gameIndex);

            setGame(new Game(getNumberOfPlayers()));

            getGame().play();

            Log.info(getGame().toString());

            Event.logEvent(EventType.END_A_GAME, gameIndex);

            final String eventLog = EventManager.getInstance().logEvents();
            Log.info(String.format(" Event log for Game %d is %s.", gameIndex, eventLog));
        }

        EventManager.getInstance().dumpAllEventData();

    }

    public boolean isAnalyzeResults() {
        return analyzeResults;
    }

    public void setAnalyzeResults(final boolean doAnalysis) {
        analyzeResults = doAnalysis;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(final Game aGame) {
        game = aGame;
    }

    public int getGameCount() {
        return gameCount;
    }

    public void setGameCount(final int count) {
        gameCount = count;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(final int numPlayers) {
        numberOfPlayers = numPlayers;
    }

    /**
     * Process the command line and setup the options.
     * @return true if successfully processed.
     */
    public boolean processCommandLine() {

        boolean returnValue = true;
        final Options options = new Options();

        options.addOption("d", "debug", false, "Debug Logging");
        options.addOption("g", "games", true, "Number Of Games To Play");
        options.addOption("p", "players", true, "Number of Players in the Game");
        options.addOption("a", "analyze", false, "Analyze the events recorded");
        options.addOption("l", "load", false, "Load a saved game");
        options.addOption("h", "help", false, "Display help text");

        final CommandLineParser parser = new DefaultParser();
        try {

            // Parse the command line, throw a Parse Exception when you don't recognize one
            final CommandLine cmd = parser.parse(options, getArguments(), false);

            // Show any help support we can
            if (cmd.hasOption("h")) {
                new HelpFormatter().printHelp("triominos", "", options, "The --load option assumes the saved game exists.", true);
                return true;
            }

            // Determine if we are in debug mode
            if (cmd.hasOption("d")) {
                Log.info("   Enabling debug logging.");
                Log.setDebugMode(true);
            } else {
                Log.setDebugMode(false);
            }

            // Setting the number of games to play
            if (cmd.hasOption("g")) {
                final String strGameCount = cmd.getOptionValue("g");
                Log.info(String.format("   Performing %d games.", getGameCount()));
                setGameCount(Integer.parseInt(strGameCount));
            } else {
                setGameCount(DEFAULT_GAME_COUNT);
            }

            // Setting the number of players in the game
            if (cmd.hasOption("p")) {
                final String strNumberOfPlayers = cmd.getOptionValue("p");
                final int numberOfPlayersAskedFor = Integer.parseInt(strNumberOfPlayers);
                if (numberOfPlayersAskedFor > MAX_NUMBER_OF_PLAYERS) {
                    Log.error("   Players are limited to 5 maximum.");
                    return false;
                }
                Log.info("   Setting the number of players to " + getNumberOfPlayers());
                setNumberOfPlayers(numberOfPlayersAskedFor);
            } else {
                setNumberOfPlayers(DEFAULT_NUMBER_OF_PLAYERS);
            }

            // Analyze the existing game results.
            if (cmd.hasOption("a")) {
                Log.info("\nAnalyzing results...");
                setAnalyzeResults(true);
            } else {
                setAnalyzeResults(false);
            }

            // Load a saved game (there can be only one)
            if (cmd.hasOption("l")) {
                Log.info("\nLoading a saved game...");
                game = loadGame();
                if (null == game) {
                    return false;
                }
            }
        } catch (final NumberFormatException nfe) {
            Log.error("  Parse exception: " + nfe.getMessage());
            new HelpFormatter().printHelp("triominos", "", options, "The --load option assumes the saved game exists.", true);
            returnValue = false;
        } catch (final ParseException pe) {
            Log.error("  Parse exception: " + pe.getMessage());
            new HelpFormatter().printHelp("triominos", "", options, "The --load option assumes the saved game exists.", true);
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Construct the saved game filename.
     * @return String name of saved game file.
     */
    public String getSavedFilename() {
        final File savedGameFile = new File(triominosFilename);
        return savedGameFile.getAbsolutePath();
    }

    /**
     * Set the name we will use to save the game to.
     * @param filename name of file to save the game to.
     */
    public void setSavedFilename(String filename) {
        triominosFilename = filename;
    }

    /**
     * Save the game to a file.
     */
    public void saveGame() {
        Log.info("Saving the current game state...");
        try (
            FileOutputStream fos = new FileOutputStream(getSavedFilename());
            ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            oos.writeObject(game);
        } catch (final FileNotFoundException e) {
            Log.error("  File Not Found Exception in saveGame(): " + e.getMessage());
        } catch (final IOException e) {
            Log.error("  IO Exception in saveGame(): " + e.getMessage());
        }
    }

    /**
     * Load a game from a file.
     * 
     * @return (Game) the game after loading from a saved state
     */
    public Game loadGame() {
        Game loadedGame = null;
        Log.info("Loading game from saved state...");
        try (FileInputStream fis = new FileInputStream(getSavedFilename());
            ObjectInputStream ois = new ObjectInputStream(fis);) {
            loadedGame = (Game) ois.readObject();
        } catch (final FileNotFoundException e) {
            Log.error("  File Not Found Exception in saveGame(): =" + e.getMessage());
        } catch (final IOException e) {
            Log.error("  IO Exception in saveGame(): " + e.getMessage());
        } catch (final ClassNotFoundException e) {
            Log.error("  Class Not Found Exception in saveGame(): " + e.getMessage());
        }
        return loadedGame;
    }
}
