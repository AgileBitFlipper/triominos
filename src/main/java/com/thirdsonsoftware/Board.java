/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.Serializable;

/**
 * This is a smaller example of a game board. The game board, by default, is 112
 * by 112 allowing for free flow in all directions from the center starting
 * point of 56,56. All references to coordinates are Y-axis first, followed by
 * X-axis (row,col) format. This is due to the nature of our choice of frame of
 * reference.
 * 
 * <p>Game Board
 * ===================== Game Board ======================...=========
 * |                                                      ...1 1 1 1 | 
 * |                          1 1 1 1 1 1 1 1 1 2 2 2 2 2 ...0 0 1 1 | 
 * |      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 ...8 9 0 1 |
 * =======================================================...========= 
 * |   | /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /... /\  /\ | 
 * | 0 |/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/ .../  \/  \|
 * |   |--------------------------------------------------...--------| 
 * |   |\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\ ...\  /\  /|
 * | 1 | \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \... \/  \/ |
 * |   |--------------------------------------------------...--------|
 * |   | /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /... /\  /\ |
 * | 2 |/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/ .../  \/  \|
 * |   |--------------------------------------------------...--------|
 * |   |\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\ ...\  /\  /|
 * | 3 | \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \... \/  \/ |
 * ...................................................................
 * ...................................................................
 * ...................................................................
 * |   |--------------------------------------------------...--------|
 * |   |\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\  /\ ...\  /\  /|
 * |111| \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \/  \... \/  \/ |
 * =======================================================...=========
 * Row + Col == Even means tile points up 
 * Row + Col == Odd means tile points down
 *
 */
public class Board implements Serializable {

    static final long serialVersionUID = 42L;

    protected boolean useColor = false;

    private String colorCyan = Log.CYAN;

    private String colorPurple = Log.PURPLE;

    private String colorReset = Log.RESET;

    protected static final int BRIDGE_BONUS = 40;
    protected static final int HEXAGON_BONUS = 50;

    protected static final int DEFAULT_ROWS = 112;
    protected static final int DEFAULT_COLS = 112;

    private int round;

    private int numberOfRows;
    private int numberOfColumns;

    private int topBorder = DEFAULT_ROWS;
    private int bottomBorder;
    private int leftBorder = DEFAULT_COLS;
    private int rightBorder;

    final Tile[][] playedTiles;

    /**
     * Build the game board with the default size.
     */
    public Board() {
        this(DEFAULT_ROWS, DEFAULT_COLS);
    }

    /**
     * Build the game board with the specified rows and columns.
     * 
     * @param rows number of rows in the board
     * @param cols number of columns in the board
     */
    public Board(int rows, int cols) {
        playedTiles = new Tile[rows][cols];
        numberOfRows = rows;
        numberOfColumns = cols;
        setUseColor(false);
    }

    /**
     * Tell the board to use color or not. Some terminals don't support ANSI colors,
     * so this is a way to turn it off if needed. In the case of testing, color is
     * turned off.
     * 
     * @param useColor true if the board should be in color; false otherwise.
     */
    protected void setUseColor(boolean useColor) {
        this.useColor = useColor;
        Tile.setUseColor(useColor);
        if (useColor) {
            colorCyan = Log.CYAN;
            colorPurple = Log.PURPLE;
            colorReset = Log.RESET;
        } else {
            colorCyan = colorPurple = colorReset = "";
        }
    }

    /**
     * Overload for findBoardMinMax(boolean).
     */
    protected void findBoardMinMax() {
        findBoardMinMax(false);
    }

    /**
     * Determines the true size of the board based on the provided input choice. If
     * 'full' is set to true, the method is used to set the border values to the
     * existing values specified in the rows and columns found . If the value of
     * 'full' is set to false, it will look for the min and max values and display
     * the smallest version of the board that shows all tiles.
     * 
     * @param full - (boolean) If true, set border values to the current rows and
     *             cols values. If false, set borders based on the placement of the
     *             tiles.
     */
    protected void findBoardMinMax(boolean full) {

        // If we weren't asked to display the full board, let's find ou
        // where the left, top, right, and bottom borders are.
        if (!full) {

            // Max out the minimum values, and minimize out the maximum values.
            topBorder = numberOfRows;
            leftBorder = numberOfColumns;
            bottomBorder = rightBorder = 0;

            // Now, spin through the board and find the rows and cols
            // closest to the edges
            for (int row = 0; row < numberOfRows; row++) {

                for (int col = 0; col < numberOfColumns; col++) {

                    // if the spot on the board has a tile, set the
                    // values as appropriate.
                    if (playedTiles[row][col] != null) {

                        // Set the top
                        if (row < topBorder) {
                            topBorder = row;
                        }

                        // Set the bottom
                        if (row > bottomBorder) {
                            bottomBorder = row;
                        }

                        // Set the left
                        if (col < leftBorder) {
                            leftBorder = col;
                        }

                        // set the right
                        if (col > rightBorder) {
                            rightBorder = col;
                        }
                    }
                }
            }

        } else {

            // The user asked for a full board display
            topBorder = leftBorder = 0;
            bottomBorder = numberOfRows;
            rightBorder = numberOfColumns;
        }
    }

    /**
     * Gets the number of rows.
     * 
     * @return (int) number of rows in the board
     */
    public int getNumberOfRows() {
        return numberOfRows;
    }

    /**
     * Sets the number of rows.
     * 
     * @param numberOfRows (int) Sets the number of rows in the board
     */
    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }

    /**
     * Gets the number of columns.
     * 
     * @return (int) number of columns in the board
     */
    public int getNumberOfCols() {
        return numberOfColumns;
    }

    /**
     * Sets the number of columns on the board.
     * 
     * @param cols (int) set the number of columns in the board
     */
    public void setNumberOfCols(int cols) {
        this.numberOfColumns = cols;
    }

    /**
     * Gets the top border edge.
     * 
     * @return (int) current value of the topBorder (the top edge of the piece
     *         places closest to the top border for display purposes)
     */
    public int getTopBorder() {
        return topBorder;
    }

    /**
     * Sets the top border edge.
     * 
     * @param topBorder (int) Sets the topBoarder value
     */
    public void setTopBorder(int topBorder) {
        if (topBorder < 0) {
            setTopBorder(0);
        } else if (topBorder >= getNumberOfRows()) {
            setTopBorder(getNumberOfRows() - 1);
        } else {
            this.topBorder = topBorder;
        }
    }

    /**
     * Gets the bottom border edge.
     * 
     * @return (int) current value of the bottomBorder (the bottom edge of the piece
     *         places closest to the bottom border for display purposes)
     */
    public int getBottomBorder() {
        return bottomBorder;
    }

    /**
     * Sets the bottom border edge.
     * 
     * @param bottomBorder (int) sets the value of the bottom border
     */
    public void setBottomBorder(int bottomBorder) {
        if (bottomBorder < 0) {
            setBottomBorder(0);
        } else if (bottomBorder >= getNumberOfRows()) {
            setBottomBorder(getNumberOfRows() - 1);
        } else {
            this.bottomBorder = bottomBorder;
        }
    }

    /**
     * Gets the left border edge.
     * 
     * @return (int) the left edge of all pieces played on the board
     */
    public int getLeftBorder() {
        return leftBorder;
    }

    /**
     * Sets the left border edge.
     * 
     * @param leftBorder (int) the left edge of all pieces that have been played on
     *                   the board.
     */
    public void setLeftBorder(int leftBorder) {
        if (leftBorder < 0) {
            setBottomBorder(0);
        } else if (leftBorder >= getNumberOfCols()) {
            setLeftBorder(getNumberOfCols() - 1);
        } else {
            this.leftBorder = leftBorder;
        }
    }

    /**
     * Gets the right border edge.
     * 
     * @return (int) the right edge of all pieces played on the board
     */
    public int getRightBorder() {
        return rightBorder;
    }

    /**
     * Sets the right border edge.
     * 
     * @param rightBorder (int) sets the right edge of all pieces played on the
     *                    board
     */
    public void setRightBorder(int rightBorder) {
        if (rightBorder < 0) {
            setRightBorder(0);
        } else if (rightBorder >= getNumberOfCols()) {
            setRightBorder(getNumberOfCols() - 1);
        } else {
            this.rightBorder = rightBorder;
        }
    }

    /**
     * Simply determines the Orientation of a tile when placed on the board based on
     * the row and the column that it is placed. If (row + col) is an even value,
     * the tile is Oriented upwards (Orientation.UP), if the value is odd, the tile
     * is Oriented downwards (Oriented.DOWN).
     * 
     * @param row - the row for the tile
     * @param col - the column for the tile
     * @return Orientation.UP or Orientation.DOWN
     */
    public Orientation getOrientationForPositionOnBoard(int row, int col) {
        return ((((row + col) % 2) == 0) ? Orientation.DOWN : Orientation.UP);
    }

    /**
     * Clears the current board of all pieces. This is a destructive move; no pieces
     * are moved to any tray or pile.
     */
    protected void clearBoard() {
        for (int x = 0; x < getNumberOfRows(); x++) {
            for (int y = 0; y < getNumberOfCols(); y++) {
                playedTiles[x][y] = null;
            }
        }
    }

    /**
     * Returns the number of tiles that have been played on the board.
     * 
     * @return count of tiles on the board (int)
     */
    public int count() {
        int count = 0;
        for (Tile[] row : playedTiles) {
            for (Tile tile : row) {
                if (tile != null) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Returns the piece that has been played at the location given, or
     *   null if no piece is present.
     * 
     * @param row - The row of the played tile we are requesting
     * @param col - The column of the played tile we are requesting
     * @return - A Tile if one has been played there, null otherwise
     */
    public Tile pieceAtLocation(int row, int col) {
        return playedTiles[row][col];
    }

    /**
     * Verifies that the specified space on the board is unoccupied, and that all of
     * the adjacent faces and corners will allow it to be played there.
     * 
     * @param choice the choice given
     * @return true if it fits, and false otherwise
     */
    public boolean pieceFits(Choice choice) {

        boolean itFits = false;
        Tile t = choice.getTile();
        int row = choice.getRow();
        int col = choice.getCol();

        // We need to adjust the tile to the choice setting first...for now
        // Later, let's remove the use of the tile all together...
        t.setOrientation(choice.getOrientation());
        t.setRotation(choice.getRotation());

        // Is the slot empty?
        if (playedTiles[row][col] == null) {
            itFits = leftFaceFits(t, row, col) && rightFaceFits(t, row, col) && middleFaceFits(t, row, col)
                    && leftCornerFits(t, row, col) && middleCornerFits(t, row, col) && rightCornerFits(t, row, col);

            if (itFits) {
                int score = calculateScore(choice);
                Log.debug(String.format("  Score for playing tile %s @ (%d,%d) is %s.", t, row, col, score));
                choice.setScore(score);
            }
        }
        return itFits;
    }

    /**
     * Determines if the left corner of this Tile fits on the board.
     * 
     * @param t   - tile to be placed on the board
     * @param row - row in which to place it
     * @param col - column in which to place it
     * @return true if the tile's left corner matches all other tiles on the board
     *         or blank spaces
     */
    protected Boolean leftCornerFits(Tile t, int row, int col) {

        // This is a sample board, and we need to determine
        // if a corner can fit or not. In this case, we are
        // looking at a bridge (row, column) @ (57,58). Piece (29),
        // '1-2-4' was placed as a bridge when it shouldn't have.
        // The left corner needs to match with (row,col) @ (57,57),
        // (57,56),(58,56),(58,57). We can assume that the face
        // test will handle any other match issues.
        // --------------------------------------------------------------
        //           52   53   54   55   56   57   58   59   60   61
        // --------------------------------------------------------------
        // |    |=+++++++++= -- 21-- ^ -- 1-- ^ -- 19-- ^ -- 44-- ^
        // |    |==+++++++===\5 P 0//0\\0 P 0//0\\0 P 4//4\\4 P 2//2\
        // | 56 |===+++++=====\ B // B \\ A // A \\ B // A \\ B // A \
        // |    |====+++=======\5//5 P 0\\0//0 P 4\\4//4 P 4\\4//4 P 3\
        // |    |=====+=========v -- 6-- v -- 5-- v -- 53-- v -- 42--
        // |    |+++++=+++++++++^ -- 20-- ^+++++++++^ -- 34-- ^+++++++++
        // |    |++++===+++++++/5\\5 P 0//0\+++++++/4\\4 P 4//4\+++++++
        // | 57 |+++=====+++++/ A \\ B // B \+++++/ B \\ B // A \+++++
        // |    |++=======+++/4 P 4\\4//4 P 1\+++/2 P 1\\1//1 P 5\+++
        // |    |+=========+ -- 54-- v -- 10-- + -- 29-- v -- 35-- +
        // |    |=+++++++++=+++++++++^ -- 32-- =+++++++++=+++++++++=
        // |    |==+++++++===+++++++/4\\4 P 1/===+++++++===+++++++===
        // | 58 |===+++++=====+++++/ B \\ A /=====+++++=====+++++=====
        // |    |====+++=======+++/3 P 3\\3/=======+++=======+++=======
        // |    |=====+=========+ -- 48-- v=========+=========+=========

        // Assume it fits...
        boolean itFits = true;
        String whyItFails = "";

        // Evaluate that the position is actually on the board
        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {

            itFits = false;
            whyItFails = "Off the board!";

        } else {

            boolean weCanLookLeft = (col > 0);
            boolean weCanLookFarLeft = (col > 1);
    
            int cornerToMatch = t.getLeftCorner();

            // These matches are checked because they don't depend on orientation.
            //   The code is the same regardless if the piece being checked is UP or DOWN.

            // left
            if (weCanLookLeft) {
                Tile tileToTheLeft = pieceAtLocation(row, col - 1);
                if ((tileToTheLeft != null)
                    && (tileToTheLeft.getMiddleCorner() != cornerToMatch)) {
                    Log.info(Player.showTwoTilesLeftAndRight(tileToTheLeft, t));
                    itFits = false;
                    whyItFails = "left";
                }
            }

            // far-left
            if (itFits
                && weCanLookFarLeft) {
                Tile tileFarLeft = pieceAtLocation(row, col - 2);
                if ((tileFarLeft != null)
                    && (tileFarLeft.getRightCorner() != cornerToMatch)) {
                    itFits = false;
                    whyItFails = "far-left";
                }
            }

            if (itFits) {

                if (t.getOrientation() == Orientation.UP) {

                    boolean weCanLookDown = (row < numberOfRows - 1);

                    if (weCanLookDown) {

                        // down && left
                        if (weCanLookLeft) {
                            Tile tileDownLeft = pieceAtLocation(row + 1, col - 1);
                            if ((tileDownLeft != null)
                                && (tileDownLeft.getMiddleCorner() != cornerToMatch)) {
                                itFits = false;
                                whyItFails = "down & left";
                            }
                        }
                        // down && far-left
                        if (weCanLookFarLeft) {
                            Tile tileDownFarLeft = pieceAtLocation(row + 1, col - 2);
                            if ((tileDownFarLeft != null)
                                && (tileDownFarLeft.getRightCorner() != cornerToMatch)) {
                                itFits = false;
                                whyItFails = "down & far-left";
                            }
                        }
                    }

                } else {

                    boolean weCanLookUp = (row > 0);

                    // up && left
                    if (weCanLookUp
                        && weCanLookLeft
                        && (pieceAtLocation(row - 1, col - 1) != null)
                        && (pieceAtLocation(row - 1, col - 1).getMiddleCorner() != cornerToMatch)) {
                        itFits = false;
                        whyItFails = "up & left";
                    }

                    // up && far-left
                    if (weCanLookUp
                        && weCanLookFarLeft
                        && (pieceAtLocation(row - 1, col - 2) != null)
                        && (pieceAtLocation(row - 1, col - 2).getRightCorner() != cornerToMatch)) {
                        itFits = false;
                        whyItFails = "up & far-left";
                    }
                }
            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_CORNER_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails left corner test - %s", t, row, col,
                    whyItFails));
        }
        return itFits;
    }

    /**
     * Determines if tile 't' can fit in location (row,col) based on the middle
     * corner of the tile fitting with all other pieces on the board adjacent to the
     * new tile if placed.
     * 
     * @param t   - new tile to be placed
     * @param row - row for it to be placed in
     * @param col - col for it to be placed in
     * @return true if the tile's middle corner fits the location
     */
    protected boolean middleCornerFits(Tile t, int row, int col) {
        boolean itFits = true;

        String whyItFails = "";

        boolean weCanLookLeft = (col > 0);
        boolean weCanLookRight = (col < numberOfColumns - 1);
        boolean weCanLookDown = (row < numberOfRows - 1);
        boolean weCanLookUp = (row > 0);

        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {
            itFits = false;
            whyItFails = "Off the board!";
        } else {

            if (t.getOrientation() == Orientation.DOWN) {
                if (weCanLookDown) {
                    if (weCanLookRight) {
                        Tile tileDownAndRight = pieceAtLocation(row + 1, col + 1);
                        if ((tileDownAndRight != null) 
                            && (tileDownAndRight.getLeftCorner() != t.getMiddleCorner())) {
                            Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row + 1, col + 1)));
                            itFits = false;
                            whyItFails = "down & right";
                        }
                    }

                    Tile tileDown = pieceAtLocation(row + 1, col);
                    if ((tileDown != null) 
                        && (tileDown.getMiddleCorner() != t.getMiddleCorner())) {
                        itFits = false;
                        whyItFails = "down";
                    }

                    if (weCanLookLeft) {
                        Tile tileDownAndLeft = pieceAtLocation(row + 1, col - 1);
                        if ((tileDownAndLeft != null) 
                            && (tileDownAndLeft.getRightCorner() != t.getMiddleCorner())) {
                            itFits = false;
                            whyItFails = "down & left";
                        }
                    }
                }

            } else {

                if ((weCanLookUp) 
                    && (weCanLookLeft) 
                    && (pieceAtLocation(row - 1, col - 1) != null)
                    && (pieceAtLocation(row - 1, col - 1).getRightCorner() != t.getMiddleCorner())) {
                    itFits = false;
                    whyItFails = "up & left";
                }

                if ((weCanLookUp) 
                    && (pieceAtLocation(row - 1, col) != null)
                    && (pieceAtLocation(row - 1, col).getMiddleCorner() != t.getMiddleCorner())) {
                    itFits = false;
                    whyItFails = "up";
                }

                if ((weCanLookUp) 
                    && (weCanLookRight) 
                    && (pieceAtLocation(row - 1, col + 1) != null)
                    && (pieceAtLocation(row - 1, col + 1).getLeftCorner() != t.getMiddleCorner())) {
                    itFits = false;
                    whyItFails = "up & right";
                }

            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_CORNER_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails middle corner test - %s", t, row, col,
                    whyItFails));
        }
        return itFits;
    }

    /**
     * Determines if tile 't' can fit in location (row,col) based on the right
     * corner of the tile fitting with all other pieces on the board adjacent to the
     * new tile if placed.
     * 
     * @param t   - new tile to be placed
     * @param row - row for it to be placed in
     * @param col - col for it to be placed in
     * @return true if the tile's right corner fits the location
     */
    protected boolean rightCornerFits(Tile t, int row, int col) {

        // This is a sample board, and we need to determine
        // if a right corner can fit or not. In this case, we are
        // looking at a placement (row, column) @ (59,56). Piece (54),
        // '4-4-5' was placed when it shouldn't have.
        // The right corner needs to match with (row,col) @ 
        //   (60,56) - Look Right
        //   (61,56) - Look Far Right
        //   (61,57) - Look Far Right (and down)
        //   (59,57) - Look Down
        //   (60,57) - Look Down (and right)
        // We can assume that the face test will handle any other match issues.
        // ----------------------------------------------
        //          55   56   57   58   59   60   61
        // ----------------------------------------------
        // |    |++++^ --  1-- ^ -- 19-- ^+++++++++=
        // |    |+++/0\\0 P 0//0\\0 P 4//4\+++++++===
        // | 56 |++/ B \\ A // A \\ B // A \+++++=====
        // |    |+/5 P 0\\0//0 P 4\\4//4 P 5\+++=======
        // |    | --  6-- v --  5-- v -- 54-- +=========
        // |    | -- 20-- ^+++++++++^ -- 34-- ^+++++++++
        // |    |=\5 P 0//0\+++++++/4\\4 P 4//4\+++++++
        // | 57 |==\ B // B \+++++/ B \\ B // A \+++++
        // |    |===\4//4 P 1\+++/2 P 1\\1//1 P 5\+++
        // |    |====v -- 10-- + -- 29-- v -- 35-- +

        boolean itFits = true;

        String whyItFails = "";

        // Check out the length of the looks we need
        boolean weCanLookRight = (col < numberOfColumns - 1);
        boolean weCanLookFarRight = (col < numberOfColumns - 2);
        boolean weCanLookDown = (row < numberOfRows - 1);
        boolean weCanLookUp = (row > 0);

        // Ensure that we are well within the bounds of the board
        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {
            itFits = false;
            whyItFails = "Off the board!";
        } else {

            int cornerToMatch = t.getRightCorner();

            // right
            if (weCanLookRight) {
                Tile tileToTheRight = pieceAtLocation(row, col + 1);
                if ((tileToTheRight != null) 
                    && (tileToTheRight.getMiddleCorner() != cornerToMatch)) {
                    Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row, col + 1)));
                    itFits = false;
                    whyItFails = "right";
                }
            }

            // far-right
            if (itFits 
                && weCanLookFarRight) {
                Tile tileToTheFarRight = pieceAtLocation(row, col + 2);
                if ((tileToTheFarRight != null) && (tileToTheFarRight.getLeftCorner() != cornerToMatch)) {
                    Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row, col + 2)));
                    itFits = false;
                    whyItFails = "far-right";
                }
            }

            if (itFits) {
                // If we are oriented UP, we need to look down...
                if (t.getOrientation() == Orientation.UP) {

                    // down && far-right
                    if (weCanLookDown
                        && weCanLookFarRight) {
                        Tile tileDownAndFarRight = pieceAtLocation(row + 1, col + 2);
                        if ((tileDownAndFarRight != null) && (tileDownAndFarRight.getLeftCorner() != cornerToMatch)) {
                            Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row + 1, col + 2)));
                            itFits = false;
                            whyItFails = "down & far-right";
                        }
                    }

                    // down && right
                    if (itFits 
                        && weCanLookDown 
                        && weCanLookRight) {
                        Tile tileDownAndRight = pieceAtLocation(row + 1, col + 1);
                        if ((tileDownAndRight != null) 
                            && (tileDownAndRight.getMiddleCorner() != cornerToMatch)) {
                            Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row + 1, col + 1)));
                            itFits = false;
                            whyItFails = "down & right";
                        }
                    }

                } else {

                    // up && right
                    if (weCanLookUp 
                        && weCanLookRight) {
                        Tile tileUpAndRight = pieceAtLocation(row - 1, col + 1);
                        if ((tileUpAndRight != null) 
                            && (tileUpAndRight.getMiddleCorner() != cornerToMatch)) {
                            Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row - 1, col + 1)));
                            itFits = false;
                            whyItFails = "up & right";
                        }
                    }

                    // up && far right
                    if (itFits 
                        && weCanLookUp 
                        && weCanLookFarRight) {
                        Tile tileUpAndFarRight = pieceAtLocation(row - 1, col + 2);
                        if ((tileUpAndFarRight != null) 
                            && (tileUpAndFarRight.getLeftCorner() != cornerToMatch)) {
                            Log.info(Player.showTwoTilesLeftAndRight(t, pieceAtLocation(row - 1, col + 2)));
                            itFits = false;
                            whyItFails = "up & far-right";
                        }
                    }
                }
            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_CORNER_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails right corner test - %s", t, row, col,
                    whyItFails));
        }
        return itFits;
    }

    /**
     * Determines if the tile being placed at row and col actually fits there based
     * on the left face of the tile.
     * 
     * @param t   - tile to be placed
     * @param row - row in which to place it
     * @param col - column in which to place it
     * @return true if tile's left face matches or is adjacent to an empty spot,
     *         false otherwise
     */
    protected Boolean leftFaceFits(Tile t, int row, int col) {

        // This test determines if a tile's (2) '0-0-1' left face fits on the
        // board when the tile (2) is placed at (row,col) @ (56,57). The left
        // face of our tile (2) is 0-1, and tile (7) at position (56,56) has a
        // right face 1-0. These faces match because faces are always seen
        // with the reference point of the center of the tile.
        //
        // --------------------
        //           56   57
        // --------------------
        // |    |= -- 7 -- ^
        // |    |==\1 P 1//1\
        // | 56 |===\ A // B \
        // |    |====\0//0 T 0\
        // |    |=====v -- 2--
        // Assume it fits...
        boolean itFits = true;
        String whyItFails = "";

        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {
            itFits = false;
            whyItFails = "Off the board!";
        } else {

            // On left-edge is automatic fit.  Are we inward of the edge?
            if (col > 0) {

                // Look left...
                Tile tileToOurLeft = pieceAtLocation(row, col - 1);

                // Is the left space empty?
                Boolean occupied = (tileToOurLeft != null);
                if (occupied) {

                    // Left tile's orientation should be opposite of this one
                    // Left tile's right face should match our tile's left face
                    Boolean orientationSame = (tileToOurLeft.getOrientation() == t.getOrientation());
                    Boolean facesMatch = tileToOurLeft.getRightFace().match(t.getLeftFace());

                    itFits = !orientationSame && facesMatch;
                    whyItFails = "Orientation or Face Mismatch";
                }
            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_FACE_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails left face test - %s", t, row, col,
                    whyItFails));
        }

        return itFits;
    }

    /**
     * Determines if the tile being placed at row and col actually fits there based
     * on the right face of the tile.
     * 
     * @param t   - tile to be placed
     * @param row - row in which to place it
     * @param col - column in which to place it
     * @return true if tile's right face matches or is adjacent to an empty spot,
     *         false otherwise
     */
    protected Boolean rightFaceFits(Tile t, int row, int col) {
        // This test determines if a tile's (7) '0-1-1' right face fits on the
        // board when the tile (2) is placed at (row,col) @ (56,57). The right
        // face of our tile (7) is 0-1, and tile (2) at position (56,57) has a
        // right face 1-0. These faces match because faces are always seen
        // with the reference point of the center of the tile.
        //
        // --------------------
        //           56   57
        // --------------------
        // |    |= -- 7 -- ^
        // |    |==\1 P 1//1\
        // | 56 |===\ A // B \
        // |    |====\0//0 T 0\
        // |    |=====v -- 2--

        // Assume it fits...
        boolean itFits = true;
        String whyItFails = "";

        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {
            itFits = false;
            whyItFails = "Off the board!";
        } else {
            // Are we inward of the edge?
            if (col < numberOfColumns - 1) {

                // Look right...
                Tile tileToOurRight = pieceAtLocation(row, col + 1);

                // Is the right space empty?
                // Right tile's orientation should be opposite of this one
                // Right tile's left face should match our tile's right face
                if ((tileToOurRight != null) 
                    && ((tileToOurRight.getOrientation() == t.getOrientation())
                    || (!tileToOurRight.getLeftFace().match(t.getRightFace())))) {
                    itFits = false;
                    whyItFails = "Orientation or Face Mismatch";
                }
            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_FACE_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails right face test - %s", t, row, col,
                    whyItFails));
        }

        return itFits;
    }

    /**
     * Determines if the tile being placed at row and col actually fits there based
     * on the middle face of the tile.
     * 
     * @param t   - tile to be placed
     * @param row - row in which to place it
     * @param col - column in which to place it
     * @return true if tile's middle face matches or is adjacent to an empty spot,
     *         false otherwise
     */
    protected Boolean middleFaceFits(Tile t, int row, int col) {

        // Assume it fits...
        boolean itFits = true;
        String whyItFails = "";

        if ((col < 0) 
            || (col >= numberOfColumns) 
            || (row < 0) 
            || (row >= numberOfRows)) {
            itFits = false;
            whyItFails = "Off the board!";
        } else {

            // look up?
            if (t.getOrientation() == Orientation.DOWN) {

                // If we are inward of the top border...
                if (row > 0) {

                    // Look up...
                    Tile tileAbove = pieceAtLocation(row - 1, col);

                    // Is the space above empty?
                    // Above tile's orientation should be opposite of this one
                    // Above tile's middle face should match our tile's middle face
                    if ((tileAbove != null)
                        && ((tileAbove.getOrientation() == t.getOrientation())
                        || (!tileAbove.getMiddleFace().match(t.getMiddleFace())))) {
                        itFits = false;
                        whyItFails = "Above middle face";
                    }
                }

            } else {

                // If we are inward of the bottom border...
                if (row < numberOfRows - 1) {

                    // Look down...
                    Tile tileBelow = pieceAtLocation(row + 1, col);

                    // Is the space below empty?
                    // Below tile's orientation should be opposite of this one
                    // Below tile's middle face should match our tile's middle face
                    if ((tileBelow != null)
                        && ((tileBelow.getOrientation() == t.getOrientation())
                        || (!tileBelow.getMiddleFace().match(t.getMiddleFace())))) {
                        itFits = false;
                        whyItFails = "Below middle face";
                    }
                }
            }
        }

        if (!itFits) {
            Event.logEvent(EventType.FAIL_FACE_TEST, t, t.getPlayer(), getRound());
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) fails middle face test - %s", t, row, col,
                    whyItFails));
        }

        return itFits;
    }

    /**
     * Let's calculate the possible score if this tile is placed where we want it to
     * be placed.
     * 
     * @param choice - Choice the choice to calculate the score with
     * @return int - score if tile is placed
     */
    @SuppressFBWarnings("UC_USELESS_OBJECT")
    protected int calculateScore(Choice choice) {

        int score = 0;

        Tile t = choice.getTile();
        int row = choice.getRow();
        int col = choice.getCol();

        // Validate board requirements first.
        if ((t != null) 
            && (row >= 0)
            && (col >= 0)
            && (row < getNumberOfRows())
            && (col < getNumberOfCols())) {

            // ***************************************************************
            // Note: This is a real board, and should be considered
            // valid. It is only for demonstration purposes but
            // resulted from real game play.
            // *******************************************************
            // For this scoring round, the following will be used:
            // Piece 42 (2-3-4) completes a hexagon.
            // Piece 51 (3-4-5) completes a bridge.
            // *******************************************************
            // -------------------------------------------------------------
            //           53   54   55   56   57   58   59   60   61   62
            // -------------------------------------------------------------
            // |    |+++++=+++++++++=+++++++++^+++++++++=+++++++++=+++++++++
            // |    |++++===+++++++===+++++++/2\+++++++===+++++++===+++++++
            // | 52 |+++=====+++++=====+++++/ A \+++++=====+++++=====+++++
            // |    |++=======+++=======+++/3 P 3\+++=======+++=======+++
            // |    |+=========+=========+ -- 41-- +=========+=========+
            // |    |=+++++++++=+++++++++^ -- 48-- ^+++++++++^ -- 56-- =
            // |    |==+++++++===+++++++/3\\3 P 3//3\+++++++/5\\5 P 5/===
            // | 53 |===+++++=====+++++/ B \\ A // B \+++++/ A \\ A /=====
            // |    |====+++=======+++/2 P 4\\4//4 P 4\+++/4 P 5\\5/=======
            // |    |=====+=========+ -- 42-- v -- 50-- + -- 55-- v=========
            // |    |+++++^ -- 3-- ^ -- 39-- ^ -- 53-- ^ -- 35-- ^ -- 21--
            // |    |++++/0\\0 P 2//2\\2 P 4//4\\4 P 4//4\\4 P 5//5\\5 P 0/
            // | 54 |+++/ A \\ A // A \\ B // A \\ B // A \\ B // B \\ B /
            // |    |++/5 P 0\\0//0 P 2\\2//2 P 4\\4//4 P 1\\1//1 P 5\\5/
            // |    |+ -- 6-- v -- 12-- v -- 44-- v -- 34-- v -- 36-- v
            // |    |= -- 20-- = -- 15-- ^ -- 45-- ^+++++++++= -- 11-- =
            // |    |==\5 P 0/===\0 P 2//2\\2 P 4//4\+++++++===\1 P 5/===
            // | 55 |===\ B /=====\ B // A \\ A // B \+++++=====\ A /=====
            // |    |====\4/=======\5//5 P 5\\5//5 P 4\+++=======\0/=======
            // |    |=====v=========v -- 46-- v -- 54-- +=========v=========
            // |    |+++++= -- 51-- ^ -- 52-- ^+++++++++=+++++++++=+++++++++
            // |    |++++===\4 P 5//5\\5 P 5//5\+++++++===+++++++===+++++++
            // | 56 |+++=====\ A // B \\ B // A \+++++=====+++++=====+++++
            // |    |++=======\3//3 P 3\\3//3 P 0\+++=======+++=======+++
            // |    |+=========v -- 49-- v -- 18-- +=========+=========+
            //
            // Does this piece complete a bridge?
            // row = 56
            // col = 54
            // Orientation.DOWN
            // ( ( row > 0 ) && ( col > 0 ) && ( col < num_cols ) &&
            // ( ( [row-1,col-1] != null ) || ( [row-1,col+1] != null ) || ( [row ) ) ||
            // ( ( row < num_rows-1 ) && ( col < num_cols-2 ) &&
            // ( ( [row, col+2] != null ) || ( [row+1,col+1] != null ) ) ) ||
            // ( ( row < num_rows-1 ) && ( col < num_cols-2 ) &&
            // ( ( [row, col-2] != null ) || ( [row+1,col-1] != null ) ) )
            //
            // row = 57
            // col = 58
            // Orientation.UP
            // ( ( row < num_rows-1 ) && ( col > 0 ) && ( col < num_cols ) &&
            // ( ( [row+1,col-1] != null ) || ( [row+1,col+1] != null ) ) ) ||
            // ( ( row > 0 ) && ( col > 1 ) &&
            // ( ( [row, col-2] != null ) || ( [row-1,col-1] != null ) ) ) ||
            // ( ( row > 0 ) && ( col < num_cols - 2 ) &&
            // ( ( [row, col+2] != null
            //

            boolean ulHexagon = false;
            boolean urHexagon = false;
            boolean umHexagon = false;
            boolean dlHexagon = false;
            boolean drHexagon = false;
            boolean dmHexagon = false;

            boolean leftFaceEmpty = (col > 0) && (pieceAtLocation(row, col - 1) == null);
            boolean rightFaceEmpty = (col < numberOfColumns - 1) && (pieceAtLocation(row, col + 1) == null);
            boolean middleFaceEmpty;

            boolean createsBridge = false;

            if (t.getOrientation() == Orientation.UP) {

                middleFaceEmpty = (row < numberOfRows - 1) && (pieceAtLocation(row + 1, col) == null);

                boolean anchorAbove = 
                    (((col > 0)
                      && (row > 0)
                      && (pieceAtLocation(row - 1, col - 1) != null))
                     || ((row > 0)
                         && (pieceAtLocation(row - 1, col) != null))
                     || ((col < numberOfColumns - 1)
                         && (row > 0)
                         && (pieceAtLocation(row - 1, col + 1) != null)));
                boolean anchorLeft = 
                    (((col > 1)
                      && (pieceAtLocation(row, col - 2) != null))
                     || ((col > 1)
                         && (row < numberOfRows - 1)
                         && (pieceAtLocation(row + 1, col - 2) != null))
                     || ((col > 0)
                         && (row < numberOfRows - 1)
                         && (pieceAtLocation(row + 1, col - 1) != null)));
                boolean anchorRight = 
                    (((col < numberOfColumns - 2)
                      && (pieceAtLocation(row, col + 2) != null))
                     || ((row < numberOfRows - 1) 
                         && (col < numberOfColumns - 2)
                         && (pieceAtLocation(row + 1, col + 2) != null))
                     || ((row < numberOfRows - 1)
                         && (col < numberOfColumns - 1)
                         && (pieceAtLocation(row + 1, col + 1) != null)));

                if ((leftFaceEmpty && anchorAbove && anchorLeft) 
                    || (rightFaceEmpty && anchorAbove && anchorRight)
                    || (middleFaceEmpty && anchorLeft && anchorRight)) {
                    createsBridge = true;
                }

                final int[] hexLrd = { 0, 0, 1, 1, 1 };
                final int[] hexLcd = { -1, -2, -2, -1, 0 };
                final int[] hexRrd = { 1, 1, 1, 0, 0 };
                final int[] hexRcd = { 0, 1, 2, 2, 1 };
                final int[] hexArd = { 0, -1, -1, -1, 0 };
                final int[] hexAcd = { -1, -1, 0, 1, 1 };

                if (row < numberOfRows - 1) {
                    if (col > 1) {
                        ulHexagon = true;
                        // hexagon left
                        for (int i = 0; i < 5; i++) {
                            ulHexagon &= (pieceAtLocation(row + hexLrd[i], col + hexLcd[i]) != null);
                        }
                        if (ulHexagon) {
                            Log.debug("  Completed hexagon with Up-Left orientation!");
                        }
                    }
                    if (col < numberOfColumns - 2) {
                        urHexagon = true;
                        // hexagon right
                        for (int i = 0; i < 5; i++) {
                            urHexagon &= (pieceAtLocation(row + hexRrd[i], col + hexRcd[i]) != null);
                        }
                        if (urHexagon) {
                            Log.debug("  Completed hexagon with Up-Right orientation!");
                        }
                    }
                }

                if (row > 1) {
                    if (col > 1 && col < numberOfColumns - 1) {
                        umHexagon = true;
                        // hexagon above
                        for (int i = 0; i < 5; i++) {
                            umHexagon &= (pieceAtLocation(row + hexArd[i], col + hexAcd[i]) != null);
                        }
                    }
                    if (umHexagon) {
                        Log.debug("  Completed hexagon with Up-Middle orientation!");
                    }
                }

            } else {

                middleFaceEmpty = (row > 0) && (pieceAtLocation(row - 1, col) == null);

                boolean anchorBelow = 
                    (((col > 0)
                      && (row < numberOfRows - 1)
                      && (pieceAtLocation(row + 1, col - 1) != null))
                     || ((row < numberOfRows - 1)
                         && (pieceAtLocation(row + 1, col) != null))
                     || ((col < numberOfColumns - 1)
                         && (row < numberOfRows - 1)
                         && (pieceAtLocation(row + 1, col + 1) != null)));
                boolean anchorLeft = 
                    (((col > 1)
                      && (pieceAtLocation(row, col - 2) != null))
                     || ((col > 1)
                         && (row > 0)
                         && (pieceAtLocation(row - 1, col - 2) != null))
                     || ((col > 0) 
                         && (row > 0)
                         && (pieceAtLocation(row - 1, col - 1) != null)));
                boolean anchorRight =
                    (((col < numberOfColumns - 2)
                      && (pieceAtLocation(row, col + 2) != null))
                     || ((col < numberOfColumns - 2)
                         && (row > 0) 
                         && (pieceAtLocation(row - 1, col + 2) != null))
                     || ((col < numberOfColumns - 1)
                         && (row > 0)
                         && (pieceAtLocation(row - 1, col + 1) != null)));

                if ((leftFaceEmpty && anchorBelow && anchorLeft) 
                    || (rightFaceEmpty && anchorBelow && anchorRight)
                    || (middleFaceEmpty && anchorLeft && anchorRight)) {
                    createsBridge = true;
                }

                final int[] hexLrd = { 0, 0, -1, -1, -1 };
                final int[] hexLcd = { -1, -2, -2, -1, 0 };
                final int[] hexRrd = { -1, -1, -1, 0, 0 };
                final int[] hexRcd = { 0, 1, 2, 2, 1 };
                final int[] hexArd = { 0, +1, +1, +1, 0 };
                final int[] hexAcd = { -1, -1, 0, 1, 1 };

                if (row > 0) {
                    if (col > 1) {
                        dlHexagon = true;
                        // hexagon left
                        for (int i = 0; i < 5; i++) {
                            dlHexagon &= (pieceAtLocation(row + hexLrd[i], col + hexLcd[i]) != null);
                        }
                        if (dlHexagon) {
                            Log.debug("  Completed hexagon with Down-Left orientation!");
                        }
                    }
                    if (col < numberOfColumns - 2) {
                        drHexagon = true;
                        // hexagon right
                        for (int i = 0; i < 5; i++) {
                            drHexagon &= (pieceAtLocation(row + hexRrd[i], col + hexRcd[i]) != null);
                        }
                        if (drHexagon) {
                            Log.debug("  Completed hexagon with Down-Right orientation!");
                        }
                    }
                }

                if ((row < numberOfRows - 1)
                    && (col > 1 && col < numberOfColumns - 1)) {
                    dmHexagon = true;
                    // hexagon below
                    for (int i = 0; i < 5; i++) {
                        dmHexagon &= (pieceAtLocation(row + hexArd[i], col + hexAcd[i]) != null);
                    }
                    if (dmHexagon) {
                        Log.debug("  Completed hexagon with Down-Middle orientation!");
                    }
                }
            }

            boolean createsHexagon = false;

            // If any one of six hexagons was created with tile placement, add the bonus!
            createsHexagon = ulHexagon || umHexagon || urHexagon || dlHexagon || dmHexagon || drHexagon;

            // Hexagon bonus
            if (createsHexagon) {
                if (!choice.isTestForFitOnly()) {
                    Event.logEvent(EventType.CREATE_A_HEXAGON, t, t.getPlayer(), getRound());
                    Log.info(String.format("  Tile %s creates a hexagon @ (%d,%d)!  Bonus of %d points!", t, row, col,
                            HEXAGON_BONUS));
                }
                score = HEXAGON_BONUS;
            }

            // Bridge bonus
            if (createsBridge) {
                if (!choice.isTestForFitOnly()) {
                    Event.logEvent(EventType.CREATE_A_BRIDGE, t, t.getPlayer(), getRound());
                    Log.info(String.format("  Tile %s creates a bridge @ (%d,%d)!  Bonus of %d points!", t, row, col,
                            BRIDGE_BONUS));
                }
                score = BRIDGE_BONUS;
            }

            // The first tile played
            // Starting player can earn 10 points if tile is a triplet
            if ((count() == 0)
                && (t.isTriplet())) {

                score += 10;

                // If three 0's start, there is a 30 point bonus
                if (t.getValue() == 0) {
                    score += 30;
                }
            }

            // Value of tile face
            score += t.getValue();
        }

        return score;
    }

    /**
     * Places the provided tile on the board at the location specified, if and only
     * if, it will fit.
     * 
     * @param choice - the choice we've made to place a tile
     * @return true if placed, false otherwise.
     */
    public boolean placeTile(Choice choice) {

        int row = choice.getRow();
        int col = choice.getCol();

        if (row < 0 || col < 0 || row >= numberOfRows || col >= numberOfColumns) {
            Log.info(String.format("  Tile '%s' placement @ (%d,%d) failed bounds check!", choice.tile, row, col));
            return false;
        }

        Tile t = choice.getTile();

        // Orientation needs to be set based on the tile position.
        // Do it now so the checks and balances can work.
        t.setOrientation(getOrientationForPositionOnBoard(row, col));

        // Does the piece fit into that location on the board?
        if (pieceFits(choice)) {

            // Play the tile and setup the piece information.
            playedTiles[row][col] = t;

            // Set the row and column and player
            t.setRow(row);
            t.setCol(col);

            // Placed on the board
            t.setPlaced(true);

            // Removed from the player's tray
            t.setInTray(false);
            return true;
        }
        return false;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    /**
     * Draws the horizontal scale for the board (across the top).
     * 
     * @return (String) a String representation of the column scale
     */
    private String drawColumnScale() {

        StringBuilder strScale = new StringBuilder(120);
        strScale.append(colorPurple + "------");
        // Let's paint the rows and column numbers for reference
        for (int col = leftBorder; col <= rightBorder; col++) {
            strScale.append(colorPurple + "---------");
        }
        strScale.append("\n        ");
        for (int col = leftBorder; col <= rightBorder; col++) {
            strScale.append(colorPurple + String.format(" %3d ", col));
        }
        strScale.append(colorPurple + "\n------");
        for (int col = leftBorder; col <= rightBorder; col++) {
            strScale.append(colorPurple + "---------");
        }
        strScale.append("\n" + colorReset);

        return strScale.toString();
    }

    /**
     * Determines if the value provided is an even number or not.
     * 
     * @param val (int) the value to determine oddness
     * @return true if even; false otherwise
     */
    private Boolean even(int val) {
        return ((val % 2) == 0);
    }

    /**
     * Draws a single slice (row) of the vertical scale for the board (left side)
     * Both the row and column are needed to determine which direction the spacers
     * go for each sub-row.
     * 
     * @param strScale (String[]) array of Strings representing a single row
     * @param row      - The value of the row this scale is for.
     * @param col      - The value of the column this scale is for.
     */
    private void drawRowScale(String[] strScale, int row, int col) {
        if (even(row + col)) {
            strScale[0] += colorPurple + "|    |" + colorCyan + "=";
            strScale[1] += colorPurple + "|    |" + colorCyan + "==";
            strScale[2] += String.format(colorPurple + "|%3d |" + colorCyan + "===", row);
            strScale[3] += colorPurple + "|    |" + colorCyan + "====";
            strScale[4] += colorPurple + "|    |" + colorCyan + "=====";
        } else {
            strScale[0] += colorPurple + "|    |" + colorCyan + "+++++";
            strScale[1] += colorPurple + "|    |" + colorCyan + "++++";
            strScale[2] += String.format(colorPurple + "|%3d |" + colorCyan + "+++", row);
            strScale[3] += colorPurple + "|    |" + colorCyan + "++";
            strScale[4] += colorPurple + "|    |" + colorCyan + "+";
        }
    }

    /**
     * Constructs the String representation of just the board.
     * 
     * @param full - true to show the whole board, false to show only the smallest
     *             size necessary.
     * @return (String) a String representation of just the board and placed tiles.
     */
    protected String display(boolean full) {

        StringBuilder strBoard = new StringBuilder(700);

        findBoardMinMax(full);

        // Display a row at a time, so
        for (int row = topBorder; row <= bottomBorder; row++) {

            // Let's paint the rows and column numbers for reference
            if (row == topBorder) {
                strBoard.append(drawColumnScale());
            }

            String[] strRow = new String[5];
            for (int i = 0; i < 5; i++) {
                strRow[i] = "";
            }

            for (int col = leftBorder; col <= rightBorder; col++) {

                // If we are starting a new row
                if (col == leftBorder) {
                    drawRowScale(strRow, row, col);
                }

                // If there is no tile here, let's fill it with empty space
                if (playedTiles[row][col] == null) {
                    if (even(row + col)) {
                        strRow[0] += colorCyan + "+++++++++";
                        strRow[1] += colorCyan + "+++++++";
                        strRow[2] += colorCyan + "+++++";
                        strRow[3] += colorCyan + "+++";
                        strRow[4] += colorCyan + "+";
                    } else {
                        strRow[0] += colorCyan + "=";
                        strRow[1] += colorCyan + "===";
                        strRow[2] += colorCyan + "=====";
                        strRow[3] += colorCyan + "=======";
                        strRow[4] += colorCyan + "=========";
                    }
                } else {
                    playedTiles[row][col].draw(false, strRow);
                }
            }

            if (even(row + rightBorder)) {
                strRow[0] += colorCyan + "=";
                strRow[1] += colorCyan + "==";
                strRow[2] += colorCyan + "===";
                strRow[3] += colorCyan + "====";
                strRow[4] += colorCyan + "=====";
            } else {
                strRow[0] += colorCyan + "+++++";
                strRow[1] += colorCyan + "++++";
                strRow[2] += colorCyan + "+++";
                strRow[3] += colorCyan + "++";
                strRow[4] += colorCyan + "+";
            }
            for (int k = 0; k < 5; k++) {
                strBoard.append(strRow[k]);
                strBoard.append("\n" + colorReset);
            }
        }
        return strBoard.toString();
    }

    /**
     * Displays a version of the complete board and its values.
     * 
     * @return (String) a String representation of the current state of gameplay on
     *         the board.
     */
    public String toString() {
        findBoardMinMax(false);
        String board = String.format("Board:%n  Played Piece Count:%d%n  Boundaries:(%d,%d,%d,%d)%n", count(),
                topBorder, bottomBorder, leftBorder, rightBorder);
        return (board + display(false));
    }
}