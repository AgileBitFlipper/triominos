/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This is the class designed to brute force analyze the raw data for each game.
 * Futures: This action should be replaced by the use of the ELK stack. Logstash
 * would read the JSON file data and send it to Elasticsearch. Then Kibana would
 * digest that data and present it in a format that the user desires.
 */
public class AnalyzeResults {

    static final long serialVersionUID = 54L;
    private static AnalyzeResults instance;
    private static Object mutex = new Object();

    int numberOfEventFiles;
 
    HashMap<Tile, Integer> tilesPlayed = new HashMap<Tile, Integer>();
    HashMap<Tile, Integer> startingTilesPlayed = new HashMap<Tile, Integer>();

    List<Integer> listTilesPlayedInRound = new ArrayList<Integer>();
 
    /**
     * Singleton pattern to get instance.
     * 
     * @return AnalyzeResults instance.
     */
    public static AnalyzeResults getInstance() {
        AnalyzeResults result = instance;
        if (result == null) {
            synchronized (mutex) {
                instance = result = new AnalyzeResults();
            }
        }
        return result;
    }

    /**
     * Provide a way to analyze the results.
     */
    public AnalyzeResults() {
        // We use this class to analyze only, so it doesn't need anything here.
    }

    /**
     * Let's analyze any results that are present.
     */
    public void analyzeResults() {

        // Let's go analyze the data..
        analyze();

        // Only show data if we have it.
        if (0 < numberOfEventFiles) {

            Log.info(String.format("%nAnalysis of Events in %d Event files complete:%n",
                numberOfEventFiles));

            Log.info(gamesWonByAPlayer());
            Log.info(roundsWonByAPlayer());
            Log.info(tilesPlayed());
            Log.info(startingTilePlayed());
            Log.info(numberOfGames());
            Log.info(numberOfRounds());
            Log.info(averageNumberOfTilesInARound());
            Log.info(averageNumberOfTilesInAGame());
            Log.info(numberOfTilesDrawn());
            Log.info(numberOfBridgesCompleted());
            Log.info(numberOfHexagonsCompleted());
            Log.info(numberOfPlacementTestsFailed());
        } else {
            Log.info("No games are available for analysis.");
        }
    }

    private String numberOfPlacementTestsFailed() {
        Integer failFace = eventStatsMap.get(EventType.FAIL_FACE_TEST);
        Integer failCorner = eventStatsMap.get(EventType.FAIL_CORNER_TEST);
        return String.format("Number of tile placement attempt failures: %d", 
            (failFace == null ? 0 : failFace) 
            + (failCorner == null ? 0 : failCorner));
    }

    private String numberOfHexagonsCompleted() {
        return String.format("Number of Hexagons completed: %d", eventStatsMap.get(EventType.CREATE_A_HEXAGON));
    }

    private String numberOfBridgesCompleted() {
        return String.format("Number of Bridges completed: %d", eventStatsMap.get(EventType.CREATE_A_BRIDGE));
    }

    private String numberOfTilesDrawn() {
        return String.format("Number of tiles drawn: %d", eventStatsMap.get(EventType.DRAW_A_TILE));
    }

    String numberOfGames() {
        return String.format("Number of games played: %d", eventStatsMap.get(EventType.START_A_GAME));
    }

    private String numberOfRounds() {
        return String.format("Number of rounds played: %d", eventStatsMap.get(EventType.START_A_ROUND));
    }

    private String averageNumberOfTilesInARound() {
        int sum = 0;
        Iterator<Integer> iterator = listTilesPlayedInRound.iterator();
        while (iterator.hasNext()) {
            sum += iterator.next().intValue();
        }
        int denominator = listTilesPlayedInRound.size();
        int average = (denominator == 0) ? 0 : sum / denominator;
        return String.format("Average number of Tiles played in a Round: %d", average);
    }

    private String averageNumberOfTilesInAGame() {
        int sum = 0;
        for (String game : gameStatMap.keySet()) {
            sum += gameStatMap.get(game).get(EventType.PLACE_A_TILE);
        }
        int denominator = gameStatMap.keySet().size();
        int average = (denominator == 0) ? 0 : sum / denominator;
        return String.format("Average number of Tiles played in a Game: %d", average);
    }

    // ***********************************************
    // Event File Hierarchy 
    // ***********************************************
    // Event File
    // │   ├── Filename, events120852176
    // │   └── Datetime, Sun Jun 13 17:08:10 EDT 2021
    // └──Game
    //    ├── Start a Game
    //    ├── Setup Players
    //    ├── Generate Tiles
    //    │    ├── Start a Round
    //    │    │   ├── Shuffle Tiles
    //    │    │   ├── Setup Player Tray
    //    │    │   │   └── Draw a Tile
    //    │    │   └── Player Starts A Round
    //    │    │       ├── Triple Play Bonus
    //    │    │       ├── Triple Zero Bonus
    //    │    │       ├── Fail Corner Test
    //    │    │       ├── Fail Face Test
    //    │    │       ├── Place a Tile
    //    │    │       │   ├── Create a Hexagon
    //    │    │       │   ├── Create a Bridge
    //    │    │       │   ├── Win a Round By Empty Tray
    //    │    │       │   └── Win a Game
    //    │    │       └── Draw a Tile
    //    │    └── End a Round
    //    └── End a Game

    Map<EventType, Integer> eventStatsMap = new HashMap<>();
    Map<Player, Map<EventType, Integer>> playerMap = new HashMap<>();
    Map<String, Map<EventType, Integer>> gameStatMap = new HashMap<>();
    Map<String, Map<EventType, Integer>> roundStatMap = new HashMap<>();
    Map<Player, Map<String, EventType>> playerWinByMap = new HashMap<>();

    List<Integer> games = new ArrayList<>();
    List<Integer> rounds = new ArrayList<>();
        
    // Game
    //      File
    //          Name
    //          Date Time
    //          File Size
    //      Players
    //      Rounds
    //          Round
    //              Name
    //              Time Elapsed
    //              Staring Player
    //              Starting Tile
    //              Tiles Played
    //              Winning Player
    //              Winning Method
    //      Time Elapsed
    //      Tiles Played
    //      Winning Player
    //      Score ?
    // Map<String, Map<String, Map<String, Integer>>> gameRoundEvent = new HashMap<>();

    /**
     * Provide a method to analyze the results.
     * How many times has each Player won a game?
     * How many times has each Player won a round?
     * How many times has a Tile been played?
     * How many times has a Tile been used to start a Round?
     * What is the average number of tiles played in a Round?
     * What is the highest number of tiles played in a Round?
     */
    public void analyze() {

        // Process each event file
        List<String> eventFiles = EventManager.getInstance().getAllEventDataFiles();
        numberOfEventFiles = eventFiles.size();
        for (String eventFile : eventFiles) {

            // Collect the stats for just THIS game's event file
            Map<EventType, Integer> gameStats = gameStatMap.containsKey(eventFile)
                ? gameStatMap.get(eventFile) : 
                new HashMap<EventType, Integer>();
            gameStatMap.put(eventFile, gameStats);
            
            // Process the events in each event file
            List<Event> events = EventManager.getInstance().getAllEventsForDataFile(eventFile);
            events.stream().forEach(e -> {

                // Find the event in all game stats and update
                int count = eventStatsMap.containsKey(e.type) 
                    ? eventStatsMap.get(e.type) : 
                    0; 
                eventStatsMap.put(e.type, count + 1);

                // Find the event in the game stats and update.
                count = gameStats.containsKey(e.type)
                    ? gameStats.get(e.type) :
                    0;
                gameStats.put(e.type, count + 1);

                // Update the game-round statistics
                if (games.size() != 0 && rounds.size() != 0) {
                    String key = String.format("%3s-%s", games.size(), rounds.size());
                    Map<EventType, Integer> roundStats =
                            roundStatMap.containsKey(key) ? roundStatMap.get(key)
                                    : new HashMap<EventType, Integer>();
                    int roundStatCount = roundStats.containsKey(e.type) ? roundStats.get(e.type) : 0;
                    roundStats.put(e.type, roundStatCount + 1);
                    roundStatMap.put(key, roundStats);
                }

                switch (e.type) {
                    // Game-only statistics
                    case START_A_GAME:
                        games.add(Integer.valueOf(games.size() + 1));
                        rounds = new ArrayList<>();
                        break;
                    case START_A_ROUND:
                        rounds.add(Integer.valueOf(rounds.size() + 1));
                        break;
                    case END_A_GAME:
                    case END_A_ROUND:
                    case GENERATE_TILES:
                    case SHUFFLE_TILES:
                    case SETUP_PLAYER_TRAY:
                    case SETUP_PLAYERS:
                        break;

                    // Player statistics
                    case WIN_A_ROUND_BY_EMPTY_TRAY:
                    case WIN_A_ROUND_BY_FEWEST_TILES:
                    case WIN_A_GAME_BY_POINTS: 
                    case PLAYER_STARTS_A_ROUND:
                    case TRIPLE_ZERO_BONUS:
                    case TRIPLE_PLAY_BONUS:
                    case HIGHEST_TILE_START:
                    case FAIL_CORNER_TEST:
                    case FAIL_FACE_TEST:
                    case DRAW_A_TILE:
                    case PLACE_A_TILE:
                    case CREATE_A_BRIDGE:
                    case CREATE_A_HEXAGON:
                        // Deal with How a player wins...
                        if ((e.type == EventType.WIN_A_ROUND_BY_EMPTY_TRAY)
                            || (e.type == EventType.WIN_A_ROUND_BY_FEWEST_TILES) 
                            || (e.type == EventType.WIN_A_GAME_BY_POINTS)) {

                            Map<String, EventType> winByStats =
                                    playerWinByMap.containsKey(e.player) ? playerWinByMap.get(e.player)
                                            : new HashMap<String, EventType>();
                            String key = String.format("%3s-%s", games.size(), rounds.size());
                            winByStats.put(key, e.type);
                            if (!playerWinByMap.containsKey(e.player)) {
                                playerWinByMap.put(e.player, winByStats);
                            }
                        }

                        // Tile stats
                        if ((e.type == EventType.PLAYER_STARTS_A_ROUND)
                            || (e.type == EventType.PLACE_A_TILE)) {

                            if (e.type == EventType.PLAYER_STARTS_A_ROUND) {
                                // If we start a round
                                int startingTileCount =
                                        startingTilesPlayed.containsKey(e.player.getStartingTile())
                                                ? startingTilesPlayed
                                                        .get(e.player.getStartingTile())
                                                : 0;
                                if (e.player.getStartingTile() != null) {
                                    startingTilesPlayed.put(e.player.getStartingTile(),
                                            startingTileCount + 1);
                                } else {
                                    Log.error("Tile was 'null' at " + e + " event.");
                                }
                            }

                            // Tile stats
                            int tileCount =
                                    tilesPlayed.containsKey(e.tile) ? tilesPlayed.get(e.tile) : 0;
                            if (e.tile != null) {
                                tilesPlayed.put(e.tile, tileCount + 1);
                            } else {
                                Log.error("Tile was 'null' at " + e + " event.");
                            }
                        }

                        // Stats by player
                        Map<EventType, Integer> stats =
                                playerMap.containsKey(e.player) ? stats = playerMap.get(e.player)
                                        : new HashMap<EventType, Integer>();
                        
                        // Update the statistics for the player
                        int statCount = stats.containsKey(e.type) ? stats.get(e.type) : 0;
                        stats.put(e.type, statCount + 1);
                        playerMap.put(e.player, stats);
                        break;
                    default:
                        Log.info(" Unhandled event: " + e.type);
                        break;
                }
            });
        }

        Log.info(" Completed ingesting game data.");

        // Only show statistics if there are some to show.
        if (0 < numberOfEventFiles) {

            Log.info("\nGame event statistics are as follows:");
            eventStatsMap.keySet().stream().sorted().forEach(e -> { 
                Log.info(String.format(" Event:%-30s Count:%d", e, eventStatsMap.get(e))); 
            });

            Log.info("\nPlayer statistics are as follows:");
            playerMap.keySet().stream().sorted(Comparator.comparing(Player::getName)).forEach(p -> {
                Log.info(String.format("%n Player:%s", p.getName()));
                playerMap.get(p).keySet().stream().forEach(e -> { 
                    Log.info(String.format("  Event:%-30s Count:%d", e, playerMap.get(p).get(e))); 
                });
            });

            Log.info("\nGame-Round statistics are as follows:");
            roundStatMap.keySet().stream().sorted().forEach(k -> {
                Log.info(String.format("%n Game-Round:%s", k));
                roundStatMap.get(k).keySet().stream().forEach(e -> { 
                    Log.info(String.format("  Event:%-30s Count:%d", e, roundStatMap.get(k).get(e)));
                    if (e == EventType.PLACE_A_TILE) {
                        listTilesPlayedInRound.add(roundStatMap.get(k).get(e));
                    }
                });
            });
        }
    }

    // a comparator using generic type
    static class ValueComparator<K, V extends Comparable<V>> implements Comparator<K>, Serializable {

        static final long serialVersionUID = 51L;

        HashMap<K, V> map = new HashMap<K, V>();

        /**
         * How to compare two elements.
         */
        public ValueComparator(HashMap<K, V> map) {
            this.map.putAll(map);
        }

        /**
         * Override the comparator and provide a way to 
         *      compare two elements.
         */
        @Override
        public int compare(K s1, K s2) {
            V value1 = map.get(s1);
            V value2 = map.get(s2);
            // The order of the elements, ascending or descending, is determined by
            // the sign given to the compare result. If '-', the largest value is
            // first. If '+', the smallest value is first. If '0', you can decide
            // how to order the elements.
            int order = value1.compareTo(value2) * -1;
            if (order == 0) {
                order = -1;
            }
            return order;
        }
    }

    /**
     * Determines the number of games won by a Player.
     * 
     * @return String number of games won by each player.
     */
    public String gamesWonByAPlayer() {

        StringBuilder strGamesWonByPlayer =
            new StringBuilder(100).append("Games won by players:\n");

        List<EventType> winTypes = new ArrayList<>(
                Arrays.asList(
                    EventType.WIN_A_GAME_BY_POINTS, 
                    EventType.WIN_A_ROUND_BY_EMPTY_TRAY,
                    EventType.WIN_A_ROUND_BY_FEWEST_TILES));

        playerMap.keySet().stream().sorted(Comparator.comparing(Player::getName)).forEach(p -> {
            strGamesWonByPlayer.append(String.format("  Player '%s':%n", p.getName()));
            winTypes.stream().forEach(w -> {
                strGamesWonByPlayer.append(
                        String.format("    Has %-30s %d times.%n", w, 
                        playerMap.get(p).containsKey(w) ? playerMap.get(p).get(w) : 0));
            });
        });

        return strGamesWonByPlayer.toString();
    }

    /**
     * Determines the number of rounds won by a Player.
     * 
     * @return String the list of rounds won by each player.
     */
    public String roundsWonByAPlayer() {

        StringBuilder strRoundsWonByPlayer = 
            new StringBuilder(100).append("Rounds won by players:\n");

        playerWinByMap.keySet().stream().sorted(Comparator.comparing(Player::getName))
                .forEach(p -> {
                    strRoundsWonByPlayer.append(String.format("  Player '%s':%n", p.getName()));
                    playerWinByMap.get(p).keySet().stream().sorted().forEach(w -> {
                        strRoundsWonByPlayer.append(String.format("    Game-Round:%-7s by %s.%n",
                                w, playerWinByMap.get(p).containsKey(w) ? playerWinByMap.get(p).get(w) : 0));
                    });
                });

        return strRoundsWonByPlayer.toString();
    }

    /**
     * Method called when tiles are played.
     * 
     * @return String how many times each tile was played.
     */
    public String tilesPlayed() {
        StringBuilder strTilesPlayed = new StringBuilder(100).append("Tiles played:\n");

        tilesPlayed.keySet().stream().sorted(Comparator.comparing(Tile::getId)).forEach(t -> {
            strTilesPlayed.append(String.format(
                "  Tile '%s' has been played %s times.%n",
                    t, tilesPlayed.get(t)));
        });

        return strTilesPlayed.toString();
    }

    /**
     * Method called when the starting tile is played.
     * 
     * @return String the tiles played at start of each game.
     */
    public String startingTilePlayed() {
        StringBuilder strTilesPlayed = new StringBuilder(100).append("Tiles played at start:\n");

        startingTilesPlayed.keySet().stream().sorted(Comparator.comparing(Tile::getId)).forEach(t -> {
            strTilesPlayed.append(String.format(
                "  Tile '%s' has been played as the starting tile %s times.%n",
                    t, startingTilesPlayed.get(t)));
        });

        return strTilesPlayed.toString();
    }
}
