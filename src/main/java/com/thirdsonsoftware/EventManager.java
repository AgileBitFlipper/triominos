/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 5:07 PM
 */

package com.thirdsonsoftware;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventManager implements Serializable {

    static final long serialVersionUID = 44L;

    static final String EVENT_OBJECT_PATH_FORMAT = "logs/events%s.ser";
    static final String EVENT_OBJECT_PATH = "logs";
    static final String EVENT_OBJECT_FILE_PREFIX = "event";
    static final String EVENT_OBJECT_FILE_EXTENSION = ".ser";
    static final String EVENT_JSON_FILE_EXTENSION = ".json";

    ArrayList<Event> events;

    private static EventManager instance;
    private static Object mutex = new Object();

    //private constructor to avoid client applications to use constructor
    EventManager() {
        events = new ArrayList<Event>();
    }

    /**
     * Singleton pattern to get instance.
     * 
     * @return EventManager instance.
     */
    public static EventManager getInstance() {
        EventManager result = instance;
        if (result == null) {
            synchronized (mutex) {
                instance = result = new EventManager();
            }
        }
        return result;
    }

    /**
     * Return the current list of events.
     * 
     * @return ArrayList&lt;Event&gt; list of events
     */
    public ArrayList<Event> getEvents() {
        return getInstance().events;
    }

    /**
     * Log a specific event.
     * 
     * @param e - the Event to log
     */
    public void logEvent(Event e) {
        events.add(e);
    }

    /**
     * Removes all events from the current event list.
     */
    public void clearEvents() {
        getInstance().getEvents().clear();
    }

    /**
     * Return a unique event data file name.
     * 
     * @return String the unique event data file name.
     */
    public String getUniqueEventDataFilename() {
        Date dt = new Date();
        String filename = String.format(EVENT_OBJECT_PATH_FORMAT, dt.hashCode());
        return filename;
    }

    /**
     * Take the current list of events and log them out to an Event file.
     * 
     * @return String the logEvents file name
     */
    public String logEvents() {

        // Let's define the path where the events will be stored.
        File file = new File(EVENT_OBJECT_PATH);

        // If the file exists, make sure it's a directory.
        // If not, let's create it.
        if (file.exists() ? file.isDirectory() : file.mkdirs()) {
            Log.debug(" Log path available for log files.");
        } else {
            Log.error("Problem creating the log path:" + EVENT_OBJECT_PATH);
        }

        // Obtain a unique event data file name based on our time code
        String filename = getUniqueEventDataFilename();

        try (FileOutputStream fos = new FileOutputStream(filename);
             ObjectOutputStream oos = new ObjectOutputStream(fos); ) {
            for (Event evt : getInstance().events) {
                oos.writeObject(evt);
            }
        } catch (FileNotFoundException e) {
            Log.error("  Error event serialization file not found:\n" + e.getMessage());
        } catch (IOException e) {
            Log.error("  Error initializing event stream:\n" + e.getMessage());
        }
        return filename;
    }

    /**
     * Collect all of the event data files and return them in a List.
     * 
     * @return - a list of all event data files
     */
    public List<String> getAllEventDataFiles() {
        List<String> eventFiles = new ArrayList<String>();
        File index = new File(EVENT_OBJECT_PATH);
        String[] entries = index.list();
        if (entries != null) {
            for (String s : entries) {
                File currentFile = new File(index.getPath(), s);
                String filename = currentFile.getName();
                // todo: can we be smarter about this with a file system call that takes a mask?
                if (filename.startsWith(EVENT_OBJECT_FILE_PREFIX) 
                    && filename.endsWith((EVENT_OBJECT_FILE_EXTENSION))) {
                    eventFiles.add(currentFile.getAbsolutePath());
                }
            }
        }
        return eventFiles;
    }

    /**
     * Given a data filename, collect all the Event records and return them in a List.
     * 
     * @param dataFile - the full path of the Event data file
     * @return - List of Event objects in the dataFile provided
     */
    public List<Event> getAllEventsForDataFile(String dataFile) {

        // The event list to return
        ArrayList<Event> evtList = new ArrayList<Event>();

        try (
            FileInputStream fis = new FileInputStream(dataFile);
            ObjectInputStream ois = new ObjectInputStream(fis);) {
            try {
                Event evt;
                while ((evt = (Event) ois.readObject()) != null) {
                    evtList.add(evt);
                }
            } catch (EOFException eof) {
                Log.debug(String.format("  Reading of event log %s is complete.", dataFile));
            }
        } catch (FileNotFoundException e) {
            Log.error(String.format("  Error event serialization file, %s, not found", dataFile));
        } catch (IOException e) {
            Log.error("  Error initializing event stream:\n" + e.getMessage());
        } catch (ClassNotFoundException cnfe) {
            Log.error("  Class not found in event stream:\n" + cnfe.getMessage());
        }
        return evtList;
    }

    /**
     * Spin through the data files and dump the data.
     */
    public void dumpAllEventData() {
        List<String> eventFiles = getAllEventDataFiles();
        for (String s : eventFiles) {
            File currentFile = new File(s);
            if (currentFile.getName().startsWith(EVENT_OBJECT_FILE_PREFIX)) {
                dumpEventData(currentFile.getAbsolutePath());
                dumpEventDataToFile(currentFile.getAbsolutePath());
            }
        }
    }

    /**
     * Given the event datafile name, dump all the records contained therein.
     * 
     * @param filename - the name of the data file to dump
     */
    public void dumpEventData(String filename) {
        List<Event> allEventsInDataFile = getAllEventsForDataFile(filename);
        Log.info(String.format("   Events (%d) recorded in %s:", allEventsInDataFile.size(), filename));
        for (Event e : allEventsInDataFile) {
            Log.debug(String.format("    %s", e));
        }
    }

    /**
     * Dump the event data to a file for later processing.
     * 
     * @param filename name of file to dump the data to.
     */
    public void dumpEventDataToFile(String filename) {
        if ((filename != null) && !filename.isEmpty()) {
            String eventDataFile = filename.replace(".ser", ".txt");
            Log.debug(" Writing event data file: " + eventDataFile);
            try (
                FileOutputStream fileStream = new FileOutputStream(eventDataFile);
                OutputStreamWriter writer = new OutputStreamWriter(fileStream, "UTF-8");
                BufferedWriter bw = new BufferedWriter(writer); ) {

                List<Event> allEventsInDataFile = getAllEventsForDataFile(filename);
                Log.debug(String.format("   Events (%d) recorded:", allEventsInDataFile.size()));
                for (Event e : allEventsInDataFile) {
                    bw.write(String.format("    %s%n", e));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.error(String.format(" Unable to dump event data file to: '%s'", filename));
        }
    }

    /**
     * This method is designed to scan the logs directory, looking for all
     * event-*.ser files and making sure they have a proper event-*.json file. These
     * json files are used for analysis and contain almost an exact replicate of the
     * ser files.
     */
    public void convertToJson() {

        // Where do we find the log files we are to convert?
        List<String> entries = getAllEventDataFiles();

        for (String eventFile : entries) {

            String jsonFilename = eventFile.replace(EVENT_OBJECT_FILE_EXTENSION, EVENT_JSON_FILE_EXTENSION);

            // Let's setup the File object to reference the JSON file
            File jsonFile = new File(jsonFilename);

            if (!jsonFile.exists()) {

                List<Event> allEventsInDataFile = getAllEventsForDataFile(eventFile);

                writeJsonEventFile(allEventsInDataFile, jsonFile.getAbsolutePath());

            } else {
                Log.debug(String.format("  Event file %s has already been translated to JSON.", eventFile));
            }
        }
    }

    /**
     * Takes the list of Events and writes out each of them to the JSON file provided.
     * 
     * @param eventList   - the list of Events
     * @param jsonFile - the JSON file to create and add all events to
     */
    public void writeJsonEventFile(List<Event> eventList, String jsonFile) {

        // We need to define a serializer to help convert an Event to a JSON string
        SimpleModule eventModule = new SimpleModule("CustomEventSerializer", new Version(1, 0, 0, null, null, null));
        SimpleModule playerModule = new SimpleModule("CustomPlayerSerializer", new Version(1, 0, 0, null, null, null));
        SimpleModule tileModule = new SimpleModule("CustomTileSerializer", new Version(1, 0, 0, null, null, null));

        // Add the serializers to the module
        eventModule.addSerializer(Event.class, new CustomEventSerializer());
        playerModule.addSerializer(Player.class, new CustomPlayerSerializer());
        tileModule.addSerializer(Tile.class, new CustomTileSerializer());

        // Setup the JSON object mapper first
        ObjectMapper objectMapper = new ObjectMapper();

        // Update the object mapper
        objectMapper.registerModule(eventModule);
        objectMapper.registerModule(playerModule);
        objectMapper.registerModule(tileModule);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        try {
            // Now, spin up the JSON factory and write the entries out.
            JsonFactory factory = new JsonFactory();
            JsonGenerator gen = factory.createGenerator(new File(jsonFile), JsonEncoding.UTF8);
            gen.useDefaultPrettyPrinter();

            try {

                // Take the entire list of events and write it out as a single string
                String obj = objectMapper.writeValueAsString(eventList);
                gen.writeRaw(obj);

            } catch (Exception e) {
                Log.error("  Exception during object write: " + e.getMessage());
            } finally {
                gen.close();
            }
        } catch (IOException e) {
            Log.error("  Error initializing event stream:\n" + e.toString());
        }
    }
}
