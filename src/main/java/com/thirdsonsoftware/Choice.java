/*
 * Copyright (c) 2017 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 11/30/17 5:08 PM.
 *
 * This file is part of Project triominos.
 *
 * triominos can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 11/30/17 4:59 PM
 */

package com.thirdsonsoftware;

import java.util.Objects;

/**
 * The Choice class helps us decide what the board will look like
 *   and the potential score if a Tile is placed.  This also allows
 *   us to keep track of the myriad possible plays we can take
 *   per turn.  That way, we can choose the one with the greatest
 *   value as determined by its placement on the board.
 */
public class Choice {

    // Which tile?
    Tile tile;

    boolean testForFitOnly;

    // If this choice is taken, what would the score be?
    private int score;

    // Where?
    private int row;
    private int col;

    // How?
    private Orientation orientation;

    private int rotation;

    /**
     * Constructs a single choice based on a tile, position, orientation,
     *   rotation and score.
     * Note:  Even though a Tile object can hold row, col, orientation and
     *   rotation, it can't hold more than one set of these values.  That's
     *   why a choice was created.  The choice holds the tile reference and
     *   how that tile should be placed on the board for this particular
     *   instance.
     * 
     * @param tile - the tile to place
     * @param row - the row to place it at
     * @param col - the column to place it at
     * @param orientation - the orientation to use
     * @param rotation - the rotation to use
     */
    public Choice(Tile tile, int row, int col, Orientation orientation, int rotation) {
        this.tile = tile;
        this.row = row;
        this.col = col;
        this.orientation = orientation;
        this.rotation = rotation;
        this.score = 0;
    }

    /**
     * Get the Choice's tile reference.
     * 
     * @return (Tile) the tile for this choice
     */
    public Tile getTile() {
        return tile;
    }

    /**
     * Set the tile reference.
     * 
     * @param tile - the Tile to set in the choice
     */
    public void setTile(Tile tile) {
        this.tile = tile;
    }

    /**
     * Getter for the testForFitOnly flag.
     * 
     * @return boolean true if testing for fit, false otherwise.
     */
    public boolean isTestForFitOnly() {
        return testForFitOnly;
    }

    /**
     * Setter for the testForFitOnly flag.  If set, this means that
     *   this test is only to see if it fits, not that it is actually
     *   placing a Tile.
     * 
     * @param testForFitOnly - should we test for fit only
     */
    public void setTestForFitOnly(boolean testForFitOnly) {
        this.testForFitOnly = testForFitOnly;
    }

    /**
     * Get the row of the Choice.
     * 
     * @return int the row index for this choice.
     */
    public int getRow() {
        return this.row;
    }

    /**
     * Set the row of the Choice.
     * 
     * @param row - the Row to set the Choice to
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Get the column of the Choice.
     * 
     * @return int the column index for this choice.
     */
    public int getCol() {
        return this.col;
    }

    /**
     * Set the column of the Choice.
     * 
     * @param col - the Column to set the Choice to
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Get the orientation of the Choice.
     * 
     * @return Orientation the orientation for this choice
     */
    public Orientation getOrientation() {
        return this.orientation;
    }

    /**
     * Set the orientation of the Choice.
     * 
     * @param orientation - the orientation to set the Choice to
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * Get the rotation of the Choice.
     * 
     * @return int the degrees the choice is rotated by.
     */
    public int getRotation() {
        return this.rotation;
    }

    /**
     * Set the rotation of the Choice.
     * 
     * @param rotation - the Rotation to set the Choice to
     */
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    /**
     * Get the score for this Choice.
     * 
     * @return int the score for this choice.
     */
    public int getScore() { 
        return this.score; 
    }

    /**
     * Set the score for this Choice.
     * 
     * @param s - the score to set in this choice
     */
    public void setScore(int s) { 
        this.score = s; 
    }

    /**
     * We need this for HashMap to work when using a Choice as a key.
     * 
     * @param obj - Object to compare this against
     * @return true if names are the same, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Choice other = (Choice) obj;
        if (!tile.equals(other.tile)
            || (col != other.col) 
            || (row != other.row) 
            || (score != other.score) 
            || (orientation != other.orientation) 
            || (rotation != other.rotation)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tile, testForFitOnly, score, row, col, orientation, rotation);
    }
}
